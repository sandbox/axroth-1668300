#!/bin/bash

# prost base functions
# v0.1 ezi

# some default settings
PROST_DEFAULT_LIVEHOSTDOMAIN="drupalconcept.net";

PROST_DEPENDENCIES="dirname|grep";

PROST_SCRIPTSTAGES="";

# add a dependency to the dependency list
#
# 1: dependency
function prost_adddependency() {
  if ! echo "$PROST_DEPENDENCIES" | grep "^$1$\|^$1|\||$1$\||$1|" >/dev/null; then
    prefix="";
    if [ "$PROST_DEPENDENCIES" != "" ]; then
      prefix="|";
    fi;

    PROST_DEPENDENCIES="$PROST_DEPENDENCIES$prefix$1";
  fi;
}

prost_adddependency "dirname";
# include the extended functions
PROST_SCRIPT_PATH="$(dirname "$0")";

# include functions
. "$PROST_SCRIPT_PATH/include/_prost-script.sh";
. "$PROST_SCRIPT_PATH/include/_prost-db.sh";
. "$PROST_SCRIPT_PATH/include/_prost-filesystem.sh";
. "$PROST_SCRIPT_PATH/include/_prost-git.sh";
. "$PROST_SCRIPT_PATH/include/_prost-drupal.sh";

# general messages

prost_adddependency "cut";
# print a message (may be formatted later?)
#
# 1: message
# 2: message type
function prost_printmsg() {
  msgtype="info";
  if [ "$2" != "" ]; then
    msgtype="$2";
  fi;

  # TODO: don't do coloring on non coloring shells
  if [ "$msgtype" == "error" ]; then
    prost_script_setcolor "light red";
    echo "> ERROR: $1" 1>&2;
    prost_script_resetcolor;
  elif [ "$msgtype" == "warning" ]; then
    prost_script_setcolor "yellow";
    echo "> WARNING: $1" 1>&2;
    prost_script_resetcolor;
  elif echo "$msgtype" | grep "^color " >/dev/null; then
    color="$(echo "$msgtype" | cut -d" " -f2-)";
    prost_script_setcolor "$color";
    echo "$1";
    prost_script_resetcolor;
  else
    echo "> $1";
  fi;
}

# print a separator (like a newline or a line)
function prost_separator() {
  echo;
  echo "==========";
  echo;
}

# print a sub separator (like a newline or a line)
function prost_subseparator() {
  echo "----------";
}


# base functions

prost_adddependency "cat";
prost_adddependency "basename";
# call all init functions
function prost_init() {
  # get the called scripts properties
  prost_scriptproperties;

  # get the command line arguments
  prost_arginit "$@";

  if [ "$PROST_INIT_NOBRANCHCHECK" -eq 0 ]; then
    # check branch
    prost_checkbranch;

    if [ "$?" -gt 0 ]; then
      prost_printmsg "error in branch section, aborting" "error";
      exit 1;
    fi;
  fi;

  if [ "$PROST_INIT_NOUPDATE" -eq 0 ]; then
    PROST_RETURN_HASUPDATE=0;

    # check for an update and update
    prost_checkandupdate;

    if [ "$?" -gt 0 ]; then
      prost_script_yesno "error in update section, continue anyways? ";

      # 0 is false in this case, exit script
      if [ "$?" -eq 0 ]; then
        exit 1;
      fi;
    else
      # check if there was an update
      if [ "$PROST_RETURN_HASUPDATE" -eq 1 ]; then
        if [ "$PROST_INIT_NOROLLOUT" -eq 0 ]; then
          # ask if the user wants to do a rollout an all installation directories
          prost_script_yesno "script update found, do you want to update all current projects? ";

          if [ "$?" -gt 0 ]; then
            # do a rollout
            "$PROST_SCRIPT_PATH/prost-rollout.sh" -noupdate -nobranchcheck;
          fi;
        fi;

        # todo: ask if we want to rollout drush commands, and if we want to renew sehll-alias
        if [ "$PROST_INIT_CMD" -eq 1 ]; then
          prost_fs_cmd;
        else
         # ask if the user wants to do a rollout an all installation directories
          prost_script_yesno "Do you want to rollout prost-drush commands? ";

          if [ "$?" -gt 0 ]; then
            prost_fs_cmd;
          fi;
        fi;

        if [ "$PROST_INIT_NORESTART" -eq 0 ]; then
          # scripts updated, call ourselves and exit
          prost_printmsg "restarting script";
          prost_separator;

          # call and return our child's exit status
          "$0" "$@";
          exit $?;
        fi;
      fi;
    fi;
  fi;
}

prost_adddependency "basename";
prost_adddependency "dirname";
prost_adddependency "sed";
# get the called scripts properties
function prost_scriptproperties() {
  # path including filename the script has been called with
  PROST_SCRIPT_CALL="";
  # the script filename
  PROST_SCRIPT_NAME="";
  # the path part of CALL excluding filename
  PROST_SCRIPT_PATH="";

  PROST_SCRIPT_CALL="$0";
  PROST_SCRIPT_NAME="$(basename "$0" | sed -e "s/\.sh$//gi")";
  PROST_SCRIPT_PATH="$(dirname "$0")";
}

# check if we are on the right branch
function prost_checkbranch() {
  prost_printmsg "checking git branch";

  cd "$PROST_SCRIPT_PATH";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $PROST_SCRIPT_PATH" "error";
    return 1;
  fi;

  # check git branch
  prost_git_currentbranch;

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not determine current git branch, aborting" "error";
    exit 1;
  fi;

  currentbranch="$PROST_RETURN_GITBRANCH";

  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not determine current git branch" "error";
    return 1;
  fi;

  if [ "$currentbranch" == "" ]; then
    prost_printmsg "could not determine current git branch" "error";
    return 1;
  fi;

  if [ "$currentbranch" != "master" ]; then
    prost_printmsg "not on master branch" "error";
    return 1;
  fi;

  return 0;
}

prost_adddependency "cat";
prost_adddependency "grep";
# check if there is an update and update
function prost_checkandupdate() {
  prost_printmsg "checking local version";
  prost_git_getlastcommitid "$PROST_SCRIPT_PATH";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not check local version" "error";
    return 1;
  fi;

  localversion="$PROST_RETURN_COMMITID";

  prost_printmsg "checking remote version";
  prost_git_getlastremotecommitid "$PROST_SCRIPT_PATH" "origin" "master";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not check remote version" "error";
    return 1;
  fi;

  remoteversion="$PROST_RETURN_COMMITID";

  if [ "$localversion" != "$remoteversion" ]; then
    prost_printmsg "there is a newer version available, updating";

    cd "$directory";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not change into directory $PROST_SCRIPT_PATH" "error";
      return 1;
    fi;

    prost_git_fetch "origin" "master" &>"$PROST_SCRIPT_PATH/tmp/scriptfetch";

    if [ "$?" -gt 0 ]; then
      cat "$PROST_SCRIPT_PATH/tmp/scriptfetch";

      prost_printmsg "could not fetch project" "error";
      return 1;
    fi;

    cat "$PROST_SCRIPT_PATH/tmp/scriptfetch";

    prost_git_pull "origin" "master" &>"$PROST_SCRIPT_PATH/tmp/scriptpull";

    if [ "$?" -gt 0 ]; then
      cat "$PROST_SCRIPT_PATH/tmp/scriptpull";

      prost_printmsg "could not update project" "error";
      return 1;
    fi;

    cat "$PROST_SCRIPT_PATH/tmp/scriptpull";

    cd "$OLDPWD";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not change back into previous directory" "error";
      return 1;
    fi;

    PROST_RETURN_HASUPDATE=0;

    if grep "^Updating " "$PROST_SCRIPT_PATH/tmp/scriptpull" >/dev/null; then
      PROST_RETURN_HASUPDATE=1;
    fi;
  fi;
}

# print the usage/help messages for the prost scripts
function prost_usage() {
  if [ "$PROST_SCRIPT_NAME" == "prost-install" ]; then
    echo "This script installs a project from a git repository (see '{url}') into a local directory (see '-projectname')";
    echo;
    echo "usage: $PROST_SCRIPT_NAME ($0)";
    echo "  [-projectname {name | defaults to last part of the url, see '{url}'}]";
    echo "  [-myuser {user for local MySQL connection | defaults to 'root'}]";
    echo "  [-mypwd {password for local MySQL connection | defaults to ''}]";
    echo "  [-createuser {mysql user for drupal}]";
    echo "  [-createpwd {mysql user password for drupal}]";
    echo "  [-adminuser {admin user for drupal | defaults to 'admin'}]";
    echo "  [-adminpwd {admin user password for drupal | defaults to 'dev'}]";
    echo "  [-baseurl {base url including host like 'localhost/project'}]";
    echo "  [-gitliveurl {live server git url | specify here or on deploy, i.e. https://site[0-9]+:{password}@power[0-9]+.drupalconcept.net/git/site[0-9]+}]";
    echo "  [-version {drupal version to use, defaults to 6}]";
    echo "  [-sudo (use sudo to change file permissions)]";
    echo "  [-nodeployment (skip deployment settings)]";
    echo "  [-stage {stage to start from, see -stage list for a list of stages}]";
    echo "  [-xstage {stage to exclude, can be used multiple times}]";
    echo "  [-stop (stops execution after the first stage)]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
    echo "  {url to clone the project from like git://github.com/somenonsense.git}";

    echo;
    prost_stages;
  elif [ "$PROST_SCRIPT_NAME" == "prost-newproject" ]; then
    echo "This script installs an empty Drupal 6/7 project into a local directory (see '-projectname')";
    echo;
    echo "usage: $PROST_SCRIPT_NAME ($0)";
    echo "  [-projectname {projectname of your drupal project}]";
    echo "  [-makefile {optional: path to drush make file, generate with http://drushmake.me/}]";
    echo "  [-myuser {user for local MySQL connection | defaults to 'root' or local drupal variable if present}]";
    echo "  [-mypwd {password for local MySQL connection | defaults to '' or local drupal variable if present}]";
    echo "  [-version {drupal version to use, defaults to 6}]";
    echo "  [-sudo (use sudo to change file permissions)]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
  elif [ "$PROST_SCRIPT_NAME" == "prost-push" ]; then
    echo "This script pushes the content of the current project to it's development repository, creating a dump first if set to";
    echo;
    echo "usage: $PROST_SCRIPT_NAME ($0)";
    echo "  [-dumpdb (create a database dump and commit it before pushing)]";
    echo "  [-myuser {user for local MySQL connection | defaults to 'root' or local drupal variable if present}]";
    echo "  [-mypwd {password for local MySQL connection | defaults to '' or local drupal variable if present}]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
  elif [ "$PROST_SCRIPT_NAME" == "prost-pull" ]; then
    echo "This script pulls the content of the current project from it's development repository, importing a dump afterwards if set to";
    echo;
    echo "usage: $0";
    echo "  [-importdb (list dump files for importing after pulling)]";
    echo "  [-myuser {user for local MySQL connection | defaults to 'root'}]";
    echo "  [-mypwd {password for local MySQL connection | defaults to ''}]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
  elif [ "$PROST_SCRIPT_NAME" == "prost-importdb" ]; then
    echo "This script imports a database dump";
    echo;
    echo "usage: $0";
    echo "  [-myuser {user for local MySQL connection | defaults to 'root'}]";
    echo "  [-mypwd {password for local MySQL connection | defaults to ''}]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
  elif [ "$PROST_SCRIPT_NAME" == "prost-cmd" ]; then
    echo "This script adds drush commands delivered by prost to users home-directory ";
    echo;
    echo "usage: $0 ";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
  elif [ "$PROST_SCRIPT_NAME" == "prost-fixpermissions" ]; then
    echo "This script fixes some directory/file permissions. May needs a working sudo.";
    echo;
    echo "usage: $PROST_SCRIPT_NAME ($0)";
    echo "  [-sudo (use sudo to change file permissions)]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
  elif [ "$PROST_SCRIPT_NAME" == "prost-rollout" ]; then
    echo "This script applies changes from prost-install to every registered installation";
    echo;
    echo "usage: $PROST_SCRIPT_NAME ($0)";
    echo "  [-projectname {name | name of the project to use for rollout, otherwise do all projects}]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
  elif [ "$PROST_SCRIPT_NAME" == "prost-update" ]; then
    echo "This script updates the prost deployment scripts and the containing project";
    echo;
    echo "usage: $PROST_SCRIPT_NAME ($0)";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
  elif [ "$PROST_SCRIPT_NAME" == "install" ]; then
    echo "This script installs the prost deployment infrastructure and the containing project into a given directory.";
    echo "For example ~/opt/prost-deployment";
    echo;
    echo "usage: $PROST_SCRIPT_NAME ($0)";
    echo "  [-dry (only check if the dependencies are met)]";
    echo "  [-noupdate (don't try to perform an update via internet)]";
    echo "  [-h|--help (prints this help text)]";
    echo "  {directory to install the scripts into}";
  else
    prost_printmsg "no help available, sorry" "error";
  fi;
}

prost_adddependency "sed";
# list the available stages of a script
function prost_stages() {
  if [ "$PROST_SCRIPTSTAGES" != "" ]; then
    echo "  available stages in order: $PROST_SCRIPTSTAGES" | sed -e "s/|/ /g";
  fi;
}

prost_adddependency "grep";
# get the command line arguments
function prost_arginit() {
  # flag whether to check the scripts branch
  PROST_INIT_NOBRANCHCHECK=0;
  # flag whether to check and update
  PROST_INIT_NOUPDATE=0;
  # flag whether to check dependencies
  PROST_INIT_NODEPENDENCIES=0;
  # flag whether to do a rollout after an update
  PROST_INIT_NOROLLOUT=0;
  # flag whether the script has to be restarted on update
  PROST_INIT_NORESTART=0;
  # flag whether to rollout drush comands
  PROST_INIT_CMD=0;

  if [ "$PROST_SCRIPT_NAME" == "prost-install" ]; then
    # init the arguments for a project init
    PROST_INIT_DIRECTORY="";
    PROST_INIT_URL="";
    PROST_INIT_GITLIVEURL="";
    PROST_INIT_MYUSER="$PROST_DEFAULT_MYUSER";
    PROST_INIT_MYPWD="$PROST_DEFAULT_MYPWD";
    PROST_INIT_CREATEUSER="";
    PROST_INIT_CREATEPWD="";
    PROST_INIT_DRUPALADMINUSER="$PROST_DEFAULT_DRUPALADMINUSER";
    PROST_INIT_DRUPALADMINPWD="$PROST_DEFAULT_DRUPALADMINPWD";
    PROST_INIT_BASEURL="";
    PROST_INIT_VERSION="6";
    PROST_INIT_SUDO=0;
    PROST_INIT_NODEPLOYMENT=0;
    PROST_INIT_STAGE="";
    PROST_INIT_XSTAGE="";
    PROST_INIT_STOP=0;
  elif [ "$PROST_SCRIPT_NAME" == "prost-newproject" ]; then
    PROST_INIT_VERSION="6";
    PROST_INIT_DIRECTORY="d6";
    PROST_INIT_MAKEFILE="d6.make";
    PROST_INIT_SUDO=0;
    PROST_INIT_MYUSER="$PROST_DEFAULT_MYUSER";
    PROST_INIT_MYPWD="$PROST_DEFAULT_MYPWD";
  elif [ "$PROST_SCRIPT_NAME" == "prost-push" ]; then
    PROST_INIT_DUMPDB=0;
    PROST_INIT_MYUSER="$PROST_DEFAULT_MYUSER";
    PROST_INIT_MYPWD="$PROST_DEFAULT_MYPWD";
  elif [ "$PROST_SCRIPT_NAME" == "prost-pull" ]; then
    PROST_INIT_IMPORTDB=0;
    PROST_INIT_MYUSER="$PROST_DEFAULT_MYUSER";
    PROST_INIT_MYPWD="$PROST_DEFAULT_MYPWD";
  elif [ "$PROST_SCRIPT_NAME" == "prost-importdb" ]; then
    PROST_INIT_MYUSER="$PROST_DEFAULT_MYUSER";
    PROST_INIT_MYPWD="$PROST_DEFAULT_MYPWD";
  elif [ "$PROST_SCRIPT_NAME" == "prost-fixpermissions" ]; then
    PROST_INIT_SUDO=0;
  elif [ "$PROST_SCRIPT_NAME" == "prost-rollout" ]; then
    PROST_INIT_DIRECTORY="";
    # this script is the rollout script itself, it will be restarted after an update and do the rollout itself
    PROST_INIT_NOROLLOUT=1;
  elif [ "$PROST_SCRIPT_NAME" == "prost-update" ]; then
    # this script is the update script. it does nothing itself so we don't need to restart it which would cause another update check
    PROST_INIT_NORESTART=1;
  elif [ "$PROST_SCRIPT_NAME" == "install" ]; then
    PROST_INIT_DRYRUN=0;
    PROST_INIT_DIRECTORY="";
    PROST_INIT_SUDO=0;
  fi;

  while [ "$1" != "" ]; do
    if [ "$1" == "-nobranchcheck" ]; then
      PROST_INIT_NOBRANCHCHECK=1;
      shift;
      continue;
    elif [ "$1" == "-noupdate" ]; then
      PROST_INIT_NOUPDATE=1;
      shift;
      continue;
    elif [ "$1" == "-nodependencies" ]; then
      PROST_INIT_NODEPENDENCIES=1;
      shift;
      continue;
    elif [ "$1" == "-cmd" ]; then
      PROST_INIT_PROST_INIT_CMD=1;
      shift;
      continue;
    elif [ "$1" == "-h" ]; then
      # guess what, help
      prost_usage;
      exit 0;
    elif [ "$1" == "--help" ]; then
      #help again
      prost_usage;
      exit 0;
    fi;

    if [ "$PROST_SCRIPT_NAME" == "prost-install" ]; then
      if [ "$1" == "-projectname" ]; then
        # directory to clone the project into
        shift;
        PROST_INIT_DIRECTORY="$1";
      elif [ "$1" == "-myuser" ]; then
        # mysql user to connect to the database
        shift;
        PROST_INIT_MYUSER="$1";
      elif [ "$1" == "-mypwd" ]; then
        # mysql user password to connect to the database
        shift;
        PROST_INIT_MYPWD="$1";
      elif [ "$1" == "-adminuser" ]; then
        # drupal admin user
        shift;
        PROST_INIT_DRUPALADMINUSER="$1";
      elif [ "$1" == "-adminpwd" ]; then
        # drupal admin user password
        shift;
        PROST_INIT_DRUPALADMINPWD="$1";
      elif [ "$1" == "-createuser" ]; then
        # mysql user for drupal to create
        shift;
        PROST_INIT_CREATEUSER="$1";
      elif [ "$1" == "-createpwd" ]; then
        # mysql user password for drupal to create
        shift;
        PROST_INIT_CREATEPWD="$1";
      elif [ "$1" == "-baseurl" ]; then
        # mysql user password for drupal to create
        shift;
        PROST_INIT_BASEURL="$1";
      elif [ "$1" == "-gitliveurl" ]; then
        # git live remote origin url
        shift;
        PROST_INIT_GITLIVEURL="$1";
      elif [ "$1" == "-version" ]; then
        shift;

        if [ "$1" == "7" ]; then
          PROST_INIT_VERSION="$1";
        fi;
      elif [ "$1" == "-sudo" ]; then
        PROST_INIT_SUDO=1;
      elif [ "$1" == "-nodeployment" ]; then
        PROST_INIT_NODEPLOYMENT=1;
      elif [ "$1" == "-stage" ]; then
        # stage to start installation from
        shift;

        if [ "$1" == "list" ]; then
          prost_stages;
          exit 0;
        elif [ "$1" == "help" ]; then
          prost_stages;
          exit 0;
        elif [ "$1" == "" ]; then
          prost_stages;
          exit 0;
        fi;

        PROST_INIT_STAGE="$1";
      elif [ "$1" == "-xstage" ]; then
        # stage to exclude
        shift;

        if [ "$1" == "list" ]; then
          prost_stages;
          exit 0;
        elif [ "$1" == "help" ]; then
          prost_stages;
          exit 0;
        elif [ "$1" == "" ]; then
          prost_stages;
          exit 0;
        fi;

        if [ "$PROST_INIT_XSTAGE" != "" ]; then
          PROST_INIT_XSTAGE="$PROST_INIT_XSTAGE|";
        fi;
        PROST_INIT_XSTAGE="$PROST_INIT_XSTAGE$1";
      elif [ "$1" == "-stop" ]; then
        # flag if script has to stop after first executed stage
        PROST_INIT_STOP=1;
      elif echo "$1" | grep "^-" >/dev/null; then
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      else
        # url from which to fetch the project from
        # currently only git is available
        PROST_INIT_URL="$1";
      fi;
    elif [ "$PROST_SCRIPT_NAME" == "prost-newproject" ]; then
      if [ "$1" == "-version" ]; then
        shift;

        if [ "$1" == "7" ]; then
          PROST_INIT_VERSION="$1";
        fi;
      elif [ "$1" == "-sudo" ]; then
        PROST_INIT_SUDO=1;
      elif [ "$1" == "-projectname" ]; then
        # directory to clone the project into
        shift;
        PROST_INIT_DIRECTORY="$1";
      elif [ "$1" == "-makefile" ]; then
        # apache directory for vhosts
        shift;
        PROST_INIT_MAKEFILE="$1";

        if ! echo "$PROST_INIT_MAKEFILE" | grep -i "\.make$" >/dev/null; then
          PROST_INIT_MAKEFILE="$PROST_INIT_MAKEFILE.make";
        fi;

        if [ "$PROST_INIT_DIRECTORY" == "d6" ]; then
          if echo "$PROST_INIT_MAKEFILE" | grep -i "^d7\." >/dev/null; then
            PROST_INIT_DIRECTORY="d7";
          fi;
        fi;
      elif [ "$1" == "-myuser" ]; then
        # mysql user to connect to the database
        shift;
        PROST_INIT_MYUSER="$1";
      elif [ "$1" == "-mypwd" ]; then
        # mysql user password to connect to the database
        shift;
        PROST_INIT_MYPWD="$1";
      else
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      fi;
    elif [ "$PROST_SCRIPT_NAME" == "prost-push" ]; then
      if [ "$1" == "-dumpdb" ]; then
        # dump the database
        PROST_INIT_DUMPDB=1;
      elif [ "$1" == "-myuser" ]; then
        # mysql user to connect to the database
        shift;
        PROST_INIT_MYUSER="$1";
      elif [ "$1" == "-mypwd" ]; then
        # mysql user password to connect to the database
        shift;
        PROST_INIT_MYPWD="$1";
      else
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      fi;
    elif [ "$PROST_SCRIPT_NAME" == "prost-pull" ]; then
      if [ "$1" == "-importdb" ]; then
        # dump the database
        PROST_INIT_IMPORTDB=1;
      elif [ "$1" == "-myuser" ]; then
        # mysql user to connect to the database
        shift;
        PROST_INIT_MYUSER="$1";
      elif [ "$1" == "-mypwd" ]; then
        # mysql user password to connect to the database
        shift;
        PROST_INIT_MYPWD="$1";
      else
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      fi;
    elif [ "$PROST_SCRIPT_NAME" == "prost-importdb" ]; then
      if [ "$1" == "-myuser" ]; then
        # mysql user to connect to the database
        shift;
        PROST_INIT_MYUSER="$1";
      elif [ "$1" == "-mypwd" ]; then
        # mysql user password to connect to the database
        shift;
        PROST_INIT_MYPWD="$1";
      else
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      fi;
    elif [ "$PROST_SCRIPT_NAME" == "prost-fixpermissions" ]; then
      if [ "$1" == "-sudo" ]; then
        PROST_INIT_SUDO=1;
      else
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      fi;
    elif [ "$PROST_SCRIPT_NAME" == "prost-rollout" ]; then
      if [ "$1" == "-projectname" ]; then
        # directory to use for rollout
        shift;
        PROST_INIT_DIRECTORY="$1";
      else
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      fi;
    elif [ "$PROST_SCRIPT_NAME" == "install" ]; then
      if [ "$1" == "-dry" ]; then
        PROST_INIT_DRYRUN=1;
      elif echo "$1" | grep "^-" >/dev/null; then
        prost_printmsg "unknown parameter $1" "error";
        prost_usage;
        exit 1;
      else
        # installation directory
        PROST_INIT_DIRECTORY="$1";
      fi;
    fi;

    shift;
  done;

  # do some checks if mandatory parameters have been set
  if [ "$PROST_SCRIPT_NAME" == "prost-install" ]; then
    # the initial copy script needs at least an url
    if [ "$PROST_INIT_URL" == "" ]; then
      # check if clone should be skipped anyways
      # TODO: move this to a function
      if ! echo "$PROST_INIT_XSTAGE" | grep "^clone|\||clone$\||clone|\|^clone$" >/dev/null; then
        prost_usage;
        exit 1;
      fi;
    fi;

    # guess the directory to copy into if not set as parameter
    if [ "$PROST_INIT_DIRECTORY" == "" ]; then
      if [ "$PROST_INIT_URL" != "" ]; then
        prost_git_parseurl "$PROST_INIT_URL";
        PROST_INIT_DIRECTORY="$PROST_RETURN_PROJECTURL";
      fi;
    fi;

    # if the directory is still not set, this won't work
    if [ "$PROST_INIT_DIRECTORY" == "" ]; then
      prost_printmsg "could not determine project name" "error";
      exit 1;
    fi;
  elif [ "$PROST_SCRIPT_NAME" == "install" ]; then
    # we need an installation directory if this is no dry run
    if [ "$PROST_INIT_DRYRUN" -eq 0 ]; then
      if [ "$PROST_INIT_DIRECTORY" == "" ]; then
        prost_usage;
        exit 1;
      fi;
    fi;
  fi;
}
