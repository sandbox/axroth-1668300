#!/bin/bash

# prost shell functions
# V0.1 ezi

PROST_SCRIPT_BLACK="0;30";
PROST_SCRIPT_DARK_GRAY="1;30";
PROST_SCRIPT_BLUE="0;34";
PROST_SCRIPT_LIGHT_BLUE="1;34";
PROST_SCRIPT_GREEN="0;32";
PROST_SCRIPT_LIGHT_GREEN="1;32";
PROST_SCRIPT_CYAN="0;36";
PROST_SCRIPT_LIGHT_CYAN="1;36";
PROST_SCRIPT_RED="0;31";
PROST_SCRIPT_LIGHT_RED="1;31";
PROST_SCRIPT_PURPLE="0;35";
PROST_SCRIPT_LIGHT_PURPLE="1;35";
PROST_SCRIPT_BROWN="0;33";
PROST_SCRIPT_YELLOW="1;33";

# set the current color
function prost_script_setcolor() {
  color="PROST_SCRIPT_$(echo "$1" | tr "[:lower:]" "[:upper:]" | tr -s " " "_")";
  echo -ne '\e['"${!color}"'m';
}

# reset the current color
function prost_script_resetcolor() {
  echo -ne '\e[0m';
}

# ask a yes or no question
#
# 1: message
# 2: parameter
#
# parameter:
#   -default  default choice on return
function prost_script_input() {
  message="$1"; shift;
  defaultset=0;
  default="";

  while [ "$1" != "" ]; do
    if [ "$1" == "-default" ]; then
      shift;
      defaultset=1;
      default="$1";
    fi;

    shift;
  done;

  # yes is 1, no is 0
  response="";

  while true; do
    prost_script_setcolor "light green";
    echo -n "$message";
    if [ "$default" != "" ]; then
      echo -n "($default) ";
    fi;
    prost_script_resetcolor;

    read input;

    # if default is set and user uses default by pressing enter, use default
    if [ "$defaultset" -eq 1 ]; then
      if [ "$input" == "" ]; then
        # set the answer
        response="$default";
        break;
      fi;
    fi;

    # these are valid answers
    if [ "$input" != "" ]; then
      response="$input";
      break;
    fi;
  done;

  PROST_RETURN_INPUT="$response";

  return 0;
}

prost_adddependency "tr";
# ask a yes or no question
#
# 1: message
# 2: parameter
#
# parameter:
#   -default  default choice on return
function prost_script_yesno() {
  message="$1"; shift;
  default="";

  while [ "$1" != "" ]; do
    if [ "$1" == "-default" ]; then
      shift;
      default="$1";
    fi;

    shift;
  done;

  # yes is 1, no is 0
  response=0;

  while true; do
    prost_script_setcolor "light green";
    echo -n "$message[yes|no] ";
    if [ "$default" != "" ]; then
      echo -n "($default) ";
    fi;
    prost_script_resetcolor;

    read input;

    # turn all into lower case for comparison
    input="$(echo "$input" | tr "[:upper:]" "[:lower:]")";

    # if default is set and user uses default by pressing enter, use default
    if [ "$default" != "" ]; then
      if [ "$input" == "" ]; then
        # set the answer
        input="$default";
      fi;
    fi;

    # these are valid answers
    if [ "$input" == "yes" ]; then
      response=1;
      break;
    elif [ "$input" == "y" ]; then
      response=1;
      break;
    elif [ "$input" == "no" ]; then
      response=0;
      break;
    elif [ "$input" == "n" ]; then
      response=0;
      break;
    fi;
  done;

  return $response;
}

prost_adddependency "grep";
# ask a numeric question
#
# 1: message
# 2: min (optional)
# 3: max (optional)
function prost_script_numeric() {
  message="$1"; shift;

  min="";
  max="";

  if [ "$1" != "" ]; then
    if echo "$1" | grep "^[0-9][0-9]*$" >/dev/null; then
      min=$1;
    else
      prost_printmsg "min value needs to be numeric" "error";
      return 1;
    fi;

    shift;
  fi;

  if [ "$1" != "" ]; then
    if echo "$1" | grep "^[0-9][0-9]*$" >/dev/null; then
      max=$1;
    else
      prost_printmsg "max value needs to be numeric" "error";
      return 1;
    fi;

    shift;
  fi;

  response=0;

  while true; do
    prost_script_setcolor "light green";
    echo -n "$message";
    prost_script_resetcolor;

    read input;

    # check if answer is a number
    if echo "$input" | grep "^[0-9][0-9]*$" >/dev/null; then
      # check if answer is within limits if set
      match=1;

      if [ "$min" != "" ]; then
        if [ $input -lt $min ]; then
          match=0;
        fi;
      fi;

      if [ "$max" != "" ]; then
        if [ $input -gt $max ]; then
          match=0;
        fi;
      fi;

      if [ $match -eq 1 ]; then
        response=$input;
        break;
      fi;
    fi;
  done;

  return $response;
}

prost_adddependency "cut";
prost_adddependency "grep";
# ask a numeric question
#
# 1: message
# 2: choices separated by pipe
# 3: parameter
#
# parameters:
#   -default default choice on return
function prost_script_multichoice() {
  message="$1"; shift;
  default="";

  if [ "$1" == "" ]; then
    prost_printmsg "no choices given" "error";
    return 0;
  fi;

  choices="$1"; shift;

  while [ "$1" != "" ]; do
    if [ "$1" == "-default" ]; then
      shift;
      default="$1";
    fi;

    shift;
  done;

  response="";

  while true; do
    prost_script_setcolor "light green";
    echo -n "$message[$choices] ";
    if [ "$default" != "" ]; then
      echo -n "($default) ";
    fi;
    prost_script_resetcolor;

    read input;

    match=0;

    tmpchoices="$choices";

    # if default is set and user uses default by pressing enter, don't do the checks
    if [ "$default" != "" ]; then
      if [ "$input" == "" ]; then
        # skip the loop
        tmpchoices="";

        # set the answer
        match=1;
        input="$default";
      fi;
    fi;

    while [ "$tmpchoices" != "" ]; do
      choice="$(echo "$tmpchoices" | cut -d"|" -f1)";

      # remove choice or make empty if this was the last
      if echo "$tmpchoices" | grep "|" >/dev/null; then
        tmpchoices="$(echo "$tmpchoices" | cut -d"|" -f2-)";
      else
        tmpchoices="";
      fi;

      if [ "$input" == "$choice" ]; then
        match=1;
      fi;
    done;

    if [ $match -eq 1 ]; then
      response="$input";
      break;
    fi;
  done;

  PROST_RETURN_RESPONSE="$response";

  return 0;
}

prost_adddependency "cut";
prost_adddependency "grep";
# run dependency checks
#
# 1: parameter
#
# parameters:
#   -quiet
function prost_script_checkdependencies() {
  quiet=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-quiet" ]; then
      quiet=1;
    fi;

    shift;
  done;

  tmpdependencies="$PROST_DEPENDENCIES";
  while [ "$tmpdependencies" != "" ]; do
    dependency="$(echo "$tmpdependencies" | cut -d"|" -f1)";
    alternatives="";

    # remove dependency or make empty if this was the last
    if echo "$tmpdependencies" | grep "|" >/dev/null; then
      tmpdependencies="$(echo "$tmpdependencies" | cut -d"|" -f2-)";
    else
      tmpdependencies="";
    fi;

    if [ "$quiet" -eq 0 ]; then
      prost_printmsg "checking dependency $dependency";
    fi;

    if echo "$dependency" | grep ";" >/dev/null; then
      alternatives="$(echo "$dependency" | cut -d";" -f2-)";
      dependency="$(echo "$dependency" | cut -d";" -f1)";
    fi;

    which "$dependency" >/dev/null;

    if [ "$?" -gt 0 ]; then
      if [ "$alternatives" != "" ]; then
        prost_printmsg "dependency $dependency not found, checking alternatives" "warning";

        foundalternative=0;

        tmpalternatives="$alternatives";
        while [ "$tmpalternatives" != "" ]; do
          alternative="$(echo "$tmpalternatives" | cut -d";" -f1)";

          # remove alternative or make empty if this was the last
          if echo "$tmpalternatives" | grep ";" >/dev/null; then
            tmpalternatives="$(echo "$tmpalternatives" | cut -d";" -f2-)";
          else
            tmpalternatives="";
          fi;

          if [ "$quiet" -eq 0 ]; then
            prost_printmsg "checking alternative $alternative";
          fi;

          if test -e "$alternative"; then
            foundalternative=1;
          else
            prost_printmsg "alternative $alternative not found" "warning";
          fi;
        done;

        if [ $foundalternative -ne 1 ]; then
          prost_printmsg "found none of the alternatives" "error";
          return 1;
        fi;
      else
        prost_printmsg "dependency $dependency not found" "error";
        return 1;
      fi;
    fi;
  done;

  return 0;
}

# add a stage to the stage list
#
# 1: stage
function prost_addstage() {
  if ! echo "$PROST_SCRIPTSTAGES" | grep "^$1$\|^$1|\||$1$\||$1|" >/dev/null; then
    prefix="";
    if [ "$PROST_SCRIPTSTAGES" != "" ]; then
      prefix="|";
    fi;

    PROST_SCRIPTSTAGES="$PROST_SCRIPTSTAGES$prefix$1";
  fi;
}
