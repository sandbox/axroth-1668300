#!/bin/bash

# prost filesystem functions
# v0.1 ezi

prost_adddependency "mkdir";
# create a directory
# can accept parameters in front of the directory they apply to
# example: prost_fs_mkdirs -check landingpage -check drupal -ignore dumps
# this would check if landingpage and drupal exist and return an error
# if dumps already exists it would not cause an error
#
# 1-n: directory or parameter
#
# parameters:
#   -check    check if the directory exists prior to creating it
#   -ignore   if directory exists already don't create it, just ignore
#   -fatal    if directory exists, return an error (instead of just printing a warning)
#   -mode     use a specific directory mode
#   -moderec  use a specific directory mode recursively
function prost_fs_mkdirs() {
  check=0;
  ignore=0;
  fatal=0;
  mode="";
  moderec="";

  returnvalue=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-check" ]; then
      check=1;
    elif [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-fatal" ]; then
      fatal=1;
    elif [ "$1" == "-mode" ]; then
      shift;
      mode="$1";
    elif [ "$1" == "-moderec" ]; then
      shift;
      moderec="$1";
    else
      # this is a directory, create it according to the parameters

      dochmod=0;

      if [ "$check" -eq 1 ]; then
        if test -d "$1"; then
          # directory already exists
          if [ "$fatal" -eq 1 ]; then
            # we shall fail
            prost_printmsg "directory $1 already exists" "error";
            returnvalue=1;
          elif [ "$ignore" -eq 0 ]; then
            # we shall not ignore this
            prost_printmsg "directory $1 already exists" "warning";
            dochmod=1;
          else
            dochmod=1;
          fi;
        else
          mkdir -p "$1";

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          else
            dochmod=1;
          fi;
        fi;
      else
        mkdir -p "$1";

        if [ "$?" -gt 0 ]; then
          returnvalue=1;
        else
          dochmod=1;
        fi;
      fi;

      if [ "$dochmod" -eq 1 ]; then
        if [ $PROST_INIT_SUDO -eq 0 ]; then
          prost_fs_chmod -mode "$mode" -moderec "$moderec" "$1";
        else
          prost_fs_chmod -mode "$mode" -moderec "$moderec" -sudo "$1";
        fi;

        if [ "$?" -gt 0 ]; then
          prost_printmsg "could not set directory permissions" "warning";
        fi;
      fi;

      # reset the parameters for the next directory
      check=0;
      ignore=0;
      fatal=0;
      mode="";
      moderec="";
    fi;

    shift;
  done;

  return $returnvalue;
}

prost_adddependency "chmod";
# change permissions
# can accept parameters in front of the entry they apply to
#
# 1-n: directory/file or parameter
#
# parameters:
#   -check    check if the entry exists prior to changing its permissions
#   -ignore   if entry does not exist, just ignore
#   -fatal    if entry does not exist, return an error (instead of just printing a warning)
#   -mode     use a specific mode
#   -moderec  use a specific mode recursively
#   -sudo     change the file permissions using sudo
function prost_fs_chmod() {
  check=0;
  ignore=0;
  fatal=0;
  mode="";
  moderec="";
  dirmode="";
  dirmoderec="";
  filemode="";
  filemoderec="";
  sudo=0;

  returnvalue=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-check" ]; then
      check=1;
    elif [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-fatal" ]; then
      fatal=1;
    elif [ "$1" == "-mode" ]; then
      shift;
      mode="$1";
    elif [ "$1" == "-moderec" ]; then
      shift;
      moderec="$1";
    elif [ "$1" == "-dirmode" ]; then
      shift;
      dirmode="$1";
    elif [ "$1" == "-dirmoderec" ]; then
      shift;
      dirmoderec="$1";
    elif [ "$1" == "-filemode" ]; then
      shift;
      filemode="$1";
    elif [ "$1" == "-filemoderec" ]; then
      shift;
      filemoderec="$1";
    elif [ "$1" == "-sudo" ]; then
      sudo=1;
    else
      # this is an entry, apply modes

      dochmod=0;

      if [ "$check" -eq 1 ]; then
        if ! test -e "$1"; then
          # entry does not exist
          if [ "$fatal" -eq 1 ]; then
            # we shall fail
            prost_printmsg "entry $1 does not exist" "error";
            returnvalue=1;
          elif [ "$ignore" -eq 0 ]; then
            # we shall not ignore this
            prost_printmsg "entry $1 does not exist" "warning";
          else
            dochmod=1;
          fi;
        else
          dochmod=1;
        fi;
      else
        dochmod=1;
      fi;

      if [ "$dochmod" -eq 1 ]; then
        if [ "$moderec" != "" ]; then
          if [ $sudo -eq 0 ]; then
            chmod -R "$moderec" "$1";
          else
            sudo chmod -R "$moderec" "$1";
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$dirmoderec" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type d -exec chmod "$dirmoderec" {} \;;
          else
            find "$1" -type d -exec sudo chmod "$dirmoderec" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$filemoderec" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type f -exec chmod "$filemoderec" {} \;;
          else
            find "$1" -type f -exec sudo chmod "$filemoderec" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$dirmoderec" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type d -exec chmod "$dirmoderec" {} \;;
          else
            find "$1" -type d -exec sudo chmod "$dirmoderec" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$filemoderec" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type f -exec chmod "$filemoderec" {} \;;
          else
            find "$1" -type f -exec sudo chmod "$filemoderec" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$mode" != "" ]; then
          if [ $sudo -eq 0 ]; then
            chmod "$mode" "$1";
          else
            sudo chmod "$mode" "$1";
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$dirmode" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type d -exec chmod "$dirmode" {} \;;
          else
            find "$1" -type d -exec sudo chmod "$dirmode" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$filemode" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type f -exec chmod "$filemode" {} \;;
          else
            find "$1" -type f -exec sudo chmod "$filemode" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$dirmode" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type d -exec chmod "$dirmode" {} \;;
          else
            find "$1" -type d -exec sudo chmod "$dirmode" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$filemode" != "" ]; then
          if [ $sudo -eq 0 ]; then
            find "$1" -type f -exec chmod "$filemode" {} \;;
          else
            find "$1" -type f -exec sudo chmod "$filemode" {} \;;
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;
      fi;

      # reset the parameters for the next directory
      check=0;
      ignore=0;
      fatal=0;
      mode="";
      moderec="";
      dirmode="";
      dirmoderec="";
      filemode="";
      filemoderec="";
    fi;

    shift;
  done;

  return $returnvalue;
}

prost_adddependency "chown";
# change permissions
# can accept parameters in front of the entry they apply to
#
# 1-n: directory/file or parameter
#
# parameters:
#   -check     check if the entry exists prior to changing its permissions
#   -ignore    if entry does not exist, just ignore
#   -fatal     if entry does not exist, return an error (instead of just printing a warning)
#   -group     use a specific group
#   -grouprec  use a specific group recursively
#   -sudo      change the file permissions using sudo
function prost_fs_chown() {
  check=0;
  ignore=0;
  fatal=0;
  user="";
  userrec="";
  group="";
  grouprec="";
  sudo=0;

  returnvalue=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-check" ]; then
      check=1;
    elif [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-fatal" ]; then
      fatal=1;
    elif [ "$1" == "-user" ]; then
      shift;
      user="$1";
    elif [ "$1" == "-userrec" ]; then
      shift;
      userrec="$1";
    elif [ "$1" == "-group" ]; then
      shift;
      group="$1";
    elif [ "$1" == "-grouprec" ]; then
      shift;
      grouprec="$1";
    elif [ "$1" == "-sudo" ]; then
      sudo=1;
    else
      # this is an entry, apply modes

      dochown=0;

      if [ "$check" -eq 1 ]; then
        if ! test -e "$1"; then
          # entry does not exist
          if [ "$fatal" -eq 1 ]; then
            # we shall fail
            prost_printmsg "entry $1 does not exist" "error";
            returnvalue=1;
          elif [ "$ignore" -eq 0 ]; then
            # we shall not ignore this
            prost_printmsg "entry $1 does not exist" "warning";
          else
            dochown=1;
          fi;
        else
          dochown=1;
        fi;
      else
        dochown=1;
      fi;

      thisuser="$(id -nu)";
      thisgroup="$(id -ng)";

      tmpuser="$user";
      tmpuserrec="$userrec";

      tmpgroup="$group";
      tmpgrouprec="$grouprec";

      if [ "$tmpuser:$tmpgroup" != ":" ]; then
        if [ "$tmpuser" == "" ]; then
          tmpuser="$thisuser";
        fi;
        if [ "$tmpgroup" == "" ]; then
          tmpgroup="$thisgroup";
        fi;
      fi;

      single="$tmpuser:$tmpgroup";

      if [ "$tmpuserrec:$tmpgrouprec" != ":" ]; then
        if [ "$tmpuserrec" == "" ]; then
          tmpuserrec="$thisuser";
        fi;
        if [ "$tmpgrouprec" == "" ]; then
          tmpgrouprec="$thisgroup";
        fi;
      fi;

      multi="$tmpuserrec:$tmpgrouprec";

      if [ "$dochown" -eq 1 ]; then
        if [ "$multi" != "" ]; then
          if [ $sudo -eq 0 ]; then
            chown -R "$multi" "$1";
          else
            sudo chown -R "$multi" "$1";
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;

        if [ "$single" != "" ]; then
          if [ "$single" != "$multi" ]; then
            if [ $sudo -eq 0 ]; then
              chown "$single" "$1";
            else
              sudo chown "$single" "$1";
            fi;

            if [ "$?" -gt 0 ]; then
              returnvalue=1;
            fi;
          fi;
        fi;
      fi;

      # reset the parameters for the next directory
      check=0;
      ignore=0;
      fatal=0;
      user="";
      userrec="";
      group="";
      grouprec="";
    fi;

    shift;
  done;

  return $returnvalue;
}

prost_adddependency "grep";
prost_adddependency "cp";
# copy a template file into a directory
#
# 1: template file
# 2: target directory
# 3: target file (optional)
# 3-n: parameters
#
# parameters:
#   -skip    if file exists, skip it
#   -ignore  if directory does not exist, don't return as error; if the file already exists, overwrite
function prost_fs_copytemplate() {
  template="$1"; shift;
  targetdir="$1"; shift;
  targetfile="$template";

  ignore=0;
  skip=0;

  # get the target file name if set
  if ! echo "$1" | grep "^-" >/dev/null; then
    targetfile="$1"; shift;
  fi;

  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-skip" ]; then
      skip=1;
    fi;

    shift;
  done;

  # check if the template to be copied exists
  if ! test -f "$PROST_SCRIPT_PATH/templates/$template"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "template $template does not exist" "error";
      return 1;
    else
      prost_printmsg "template $template does not exist" "warning";
      return 0;
    fi;
  fi;

  # check if the target directory exists
  if ! test -d "$targetdir"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $targetdir does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $targetdir does not exist" "warning";
      return 0;
    fi;
  fi;

  # check if the template already exists in the target directory
  if test -f "$targetdir/$targetfile"; then
    if [ "$skip" -eq 1 ]; then
      # we should skip this file if it already exists
      prost_printmsg "template $template already exists in target directory $targetdir, skipping";
      return 0;
    elif [ "$ignore" -eq 1 ]; then
      # we should not ignore this, print a warning
      prost_printmsg "template $template already exists in target directory $targetdir, overwriting" "warning";
    else
      # this is fatal, return an error message and status
      prost_printmsg "template $template already exists in target directory $targetdir" "error";
      return 1;
    fi;
  fi;

  # copy the template; the trailing slash makes sure this is actually a directory
  cp "$PROST_SCRIPT_PATH/templates/$template" "$targetdir/$targetfile";

  # return the status of cp
  return $?;
}

prost_adddependency "patch";
# apply a patch to the drupal directory from the templates directory
#
# 1: directory to apply patch on
# 2: patch file
# 3-n: parameters
#
# parameters:
#   -prefixskip  skip n parts of the path
#   -ignore      if patch does not exist or is not applicable, don't return as error
function prost_fs_applypatch() {
  directory="$1"; shift;
  patchfile="$1"; shift;

  prefixskip=0;
  ignore=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-prefixskip" ]; then
      shift;
      prefixskip="$1";
    elif [ "$1" == "-ignore" ]; then
      ignore=1;
    fi;

    shift;
  done;

  if ! test -f "$PROST_SCRIPT_PATH/templates/$patchfile"; then
    if [ $ignore -eq 0 ]; then
      prost_printmsg "could not find patchfile $patchile" "error";
      return 1;
    else
      prost_printmsg "could not find patchfile $patchile" "warning";
      return 0;
    fi;
  fi;

  # test patch first
  patch --dry-run -Np$prefixskip -d "$directory" -i "$PROST_SCRIPT_PATH/templates/$patchfile";

  if [ "$?" -gt 0 ]; then
    if [ $ignore -eq 0 ]; then
      prost_printmsg "could not apply patch $patchfile" "error";
      return 1;
    else
      prost_printmsg "could not apply patch $patchfile" "warning";
      return 0;
    fi;
  fi;

  # apply the patch
  patch -Np$prefixskip -d "$directory" -i "$PROST_SCRIPT_PATH/templates/$patchfile";  

  if [ "$?" -gt 0 ]; then
    if [ $ignore -eq 0 ]; then
      prost_printmsg "could not apply patch $patchfile" "error";
      return 1;
    else
      prost_printmsg "could not apply patch $patchfile" "warning";
      return 0;
    fi;
  fi;

  return 0;
}

prost_adddependency "pwd";
prost_adddependency "grep";
prost_adddependency "basename";
prost_adddependency "sed";
# try getting the active path's project name by checking for git or nbproject
function prost_fs_getcurrentproject() {
  PROST_RETURN_PROJECTPATH="";
  PROST_RETURN_PROJECTNAME="";

  path="$(pwd)";

  returnvalue=1;

  # go through all parts of the path in the order of
  # dir1/dir2/dir3
  # dir1/dir2
  # dir1
  while echo "$path" | grep "\/" >/dev/null; do
    # if we found the project, get the paths name and break out of the loop
    if test -d "$path/.git"; then
      PROST_RETURN_PROJECTPATH="$path";
      PROST_RETURN_PROJECTNAME="$(basename "$path")";
      returnvalue=0;
      break;
    elif test -d "$path/nbproject"; then
      PROST_RETURN_PROJECTPATH="$path";
      PROST_RETURN_PROJECTNAME="$(basename "$path")";
      returnvalue=0;
      break;
    fi;

    path="$(echo "$path" | sed -e "s/^\(.*\)\/.*$/\1/g")";
  done;

  return $returnvalue;
}

prost_adddependency "grep";
prost_adddependency "sed";
# replace strings in a file
#
# 1: file
# 2-n: parameters (optional)
# 2-n %2=0: search
# 2-n %2=1: replace
#
# parameters:
#   -ignore   ignore if file does not exist
#   -caseins  use a case insensitive replace
function prost_fs_replace() {
  filename="$1"; shift;

  # get the optional parameters
  ignore=0;
  caseins=0;

  while echo "$1" | grep "^-" >/dev/null; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-caseins" ]; then
      caseins=1;
    fi;

    shift;
  done;

  # check if file exists and is a file
  if ! test -f "$filename"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "file $filename not found" "error";
      return 1;
    else
      prost_printmsg "file $filename not found";
      return 0;
    fi;
  fi;

  returnvalue=0;

  # cycle through the parameters
  while [ "$1" != "" ]; do
    search="$1"; shift;
    replace="$1"; shift;

    sedsuffix="g";
    if [ "$caseins" -eq 1 ]; then
      sedsuffix="i$sedsuffix";
    fi;

    sed -e "s/$search/$replace/$sedsuffix" "$filename" -i;

    # check if sed was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "replace in file $filename was not successfull" "warning";
      returnvalue=1;
    fi;
  done;

  return $returnvalue;
}

prost_adddependency "ln";
# create a symlink
# 1: target (where the linkfile should point to)
# 2: linkfile (the actual linkfile)
# 3: parameter
#
# parameters:
#   -ignore  ignore (overwrite) already existing file
function prost_fs_symlink() {
  ignore=0;

  target="$1"; shift;
  linkfile="$1"; shift;

  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    fi;

    shift;
  done;

  exists=0;
  if test -L "$linkfile"; then
    exists=1;
  elif test -e "$linkfile"; then
    exists=1;
  fi;

  # check if linkfile already exists
  if [ $exists -eq 1 ]; then
    # check if we should overwrite this
    if [ "$ignore" -eq 1 ]; then
      if test -L "$linkfile"; then
        # this is a symbolic link, overwrite
        prost_printmsg "target already exists, overwriting" "warning";
        rm "$linkfile";

        if [ "$?" -gt 0 ]; then
          prost_printmsg "could not remove old symlink" "error";
          return 1;
        fi;
      elif test -d "$linkfile"; then
        # this is a directory, removing it may be dangerous
        prost_printmsg "target is an already existing directory" "error";
        return 1;
      else
        # this is something else, overwrite
        prost_printmsg "target already exists, overwriting" "warning";
      fi;
    else
      # we should not ignore this, return an error
      prost_printmsg "target already exists" "error";
      return 1;
    fi;
  fi;

  ln -s "$target" "$linkfile";
  return $?;
}

prost_adddependency "grep";
prost_adddependency "head";
prost_adddependency "cut";
prost_adddependency "tail";
prost_adddependency "cp";
# remove a block from a file containing a given pattern
# block has to be bordered by empty lines
#
# 1: file     file to modify
# 2: pattern  pattern to search for
# 3: parameter
#
# parameters:
#   - ignore  ignore if file does not exist
function prost_fs_removeblockbypattern() {
  filename="$1"; shift;
  pattern="$1"; shift;

  ignore=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    fi;

    shift;
  done;

  # check if file actually exists
  if ! test -f "$filename"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "the rules file does not exist" "error";
      return 1;
    else
      prost_printmsg "the rules file does not exist" "warning";
      return 0;
    fi;
  fi;

  # get the first pattern line number
  linenumber="$(grep -nP "$pattern" "$filename" | head -n1 | cut -d: -f1 | grep "^[0-9][0-9]*$")";

  while [ "$linenumber" != "" ]; do
    prost_printmsg "removing block line $linenumber";

    # now get the last empty line before this line
    lastemptyline="$(head -n "$linenumber" "$filename" | grep -nP "^[ \t]*$" | tail -n1 | cut -d: -f1 | grep "^[0-9][0-9]*$")";
    lastfilledline="";

    if [ "$lastemptyline" != "" ]; then
      if [ "$lastemptyline" -gt 1 ]; then
        # this will be the line used for cutting
        let lastfilledline=$lastemptyline-1;

        #prost_printmsg "keeping old content up to line $lastfilledline";
      fi;
    fi;

    # get the next empty line after this line
    nextemptyline="$(tail -n +"$linenumber" "$filename" | grep -nP "^[ \t]*$" | head -n1 | cut -d: -f1 | grep "^[0-9][0-9]*$")";
    nextfilledline="";

    if [ "$nextemptyline" != "" ]; then
      # this is a relative number, starting at 1 so remove 1 and add the current line number
      let nextemptyline=$nextemptyline-1+$linenumber;

      # this will be the line used for cutting
      let nextfilledline=$nextemptyline+1;

      #prost_printmsg "keeping old content from line $nextfilledline to the end";
    fi;

    # make sure the temp file is empty
    echo -n >"$PROST_SCRIPT_PATH/tmp/removeblock";

    # if there is content before this block, copy
    if [ "$lastfilledline" != "" ]; then
      head -n "$lastfilledline" "$filename" >>"$PROST_SCRIPT_PATH/tmp/removeblock";
    fi;

    # if there is content before and after this block, leave an empty line between the blocks
    if [ "$lastfilledline" != "" ]; then
      if [ "$nextfilledline" != "" ]; then
        echo >>"$PROST_SCRIPT_PATH/tmp/removeblock";
      fi;
    fi;

    if [ "$nextfilledline" != "" ]; then
      tail -n +"$nextfilledline" "$filename" >>"$PROST_SCRIPT_PATH/tmp/removeblock";
    fi;

    # copy the temp file back to the file to modify
    cp "$PROST_SCRIPT_PATH/tmp/removeblock" "$filename";

    # check this
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not copy the temporary cutting file back to the original file" "error";
      return 1;
    fi;

    # get the new first pattern line number
    linenumber="$(grep -nP "$pattern" "$filename" | head -n1 | cut -d: -f1 | grep "^[0-9][0-9]*$")";
  done;

  return 0;
}

prost_adddependency "cat";
prost_adddependency "cp";
# prepend a template to a file
# 1: template
# 2: filename  file to alter
# 3: parameter
#
# parameter:
#   -ignore  ignore if template or file don't exist
function prost_fs_prependtemplate() {
  template="$1"; shift;
  targetfile="$1"; shift;

  ignore=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    fi;

    shift;
  done;

  # check if the template to be copied exists
  if ! test -f "$PROST_SCRIPT_PATH/templates/$template"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "template $template does not exist" "error";
      return 1;
    else
      prost_printmsg "template $template does not exist" "warning";
      return 0;
    fi;
  fi;

  # check if the target file exists
  if ! test -f "$targetfile"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "file $targetfile does not exist" "error";
      return 1;
    else
      prost_printmsg "file $targetfile does not exist" "warning";
      return 0;
    fi;
  fi;

  # "cat" the file into the temporary directory and copy back
  (cat "$PROST_SCRIPT_PATH/templates/$template"; cat "$targetfile") >"$PROST_SCRIPT_PATH/tmp/templateprepend";
  cp "$PROST_SCRIPT_PATH/tmp/templateprepend" "$targetfile";

  # return the status of cp
  return $?;
}

prost_adddependency "grep";
prost_adddependency "cut";
# copy directories recursively or files into a target directory
#
# 1-n: directory or file to copy
# n+1: target directory
function prost_fs_copy() {
  filenames="";
  oldfilenames="";
  targetdir="";

  while [ "$1" != "" ]; do
    prefix="";
    if [ "$filenames" != "" ]; then
      prefix="|";
    fi;

    targetdir="$1";

    oldfilenames="$filenames";
    filenames="$filenames$prefix$1";

    shift;
  done;

  filenames="$oldfilenames";

  # check if we have something to work with

  # source files?
  if [ "$filenames" == "" ]; then
    prost_printmsg "no files/directories specified to copy";
    return 1;
  fi;

  # target directory?
  if [ "$targetdir" == "" ]; then
    prost_printmsg "no target directory specified";
    return 1;
  fi;

  if ! test -d "$targetdir"; then
    prost_printmsg "target directory not found";
    return 1;
  fi;

  tmpfilenames="$filenames";
  while [ "$tmpfilenames" != "" ]; do
    filename="$(echo "$tmpfilenames" | cut -d"|" -f1)";

    prost_printmsg "copying $filename to $targetdir";

    # remove choice or make empty if this was the last
    if echo "$tmpfilenames" | grep "|" >/dev/null; then
      tmpfilenames="$(echo "$tmpfilenames" | cut -d"|" -f2-)";
    else
      tmpfilenames="";
    fi;

    if ! test -e "$filename"; then
      prost_printmsg "could not find $filename" "error";
      return 1;
    fi;

    cp -r "$filename" "$targetdir/";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not copy $filename" "error";
      return 1;
    fi;
  done;

  return 0;
}

prost_adddependency "grep";
prost_adddependency "cut";
# move directories or files into a target directory
#
# 1-n: directory or file to copy
# n+1: target directory
# n+2: parameter
#
# parameters:
#   -skip  file/directory to skip
#   -ignore  ignore if a directory/file is not present
function prost_fs_move() {
  filenames="";
  skipfilenames="";
  oldfilenames="";
  targetdir="";
  ignore=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-skip" ]; then
      shift;
      prefix="";
      if [ "$skipfilenames" != "" ]; then
        prefix="|";
      fi;

      skipfilenames="$skipfilenames$prefix$1";
    elif [ "$1" == "-ignore" ]; then
      ignore=1;
    else
      prefix="";
      if [ "$filenames" != "" ]; then
        prefix="|";
      fi;

      targetdir="$1";

      oldfilenames="$filenames";
      filenames="$filenames$prefix$1";
    fi;

    shift;
  done;

  filenames="$oldfilenames";

  # check if we have something to work with

  # source files?
  if [ "$filenames" == "" ]; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "no files/directories specified to copy" "error";
      return 1;
    else
      prost_printmsg "no files/directories specified to copy" "warning";
      return 0;
    fi;
  fi;

  # target directory?
  if [ "$targetdir" == "" ]; then
    prost_printmsg "no target directory specified";
    return 1;
  fi;

  if ! test -d "$targetdir"; then
    prost_printmsg "target directory not found";
    return 1;
  fi;

  tmpfilenames="$filenames";
  while [ "$tmpfilenames" != "" ]; do
    filename="$(echo "$tmpfilenames" | cut -d"|" -f1)";

    # remove choice or make empty if this was the last
    if echo "$tmpfilenames" | grep "|" >/dev/null; then
      tmpfilenames="$(echo "$tmpfilenames" | cut -d"|" -f2-)";
    else
      tmpfilenames="";
    fi;

    skip=0;

    if ! test -e "$filename"; then
      if [ "$ignore" -eq 0 ]; then
        prost_printmsg "could not find $filename" "error";
        return 1;
      else
        prost_printmsg "could not find $filename" "error";
        skip=1;
      fi;
    fi;

    if [ "$skip" -eq 0 ]; then
      if ! echo "$skipfilenames" | grep "^$filename$\|^$filename|\||$filename$\||$filename|" >/dev/null; then
        prost_printmsg "moving $filename to $targetdir";
        git mv "$filename" "$targetdir/";

        if [ "$?" -gt 0 ]; then
          prost_printmsg "could not move $filename" "error";
          return 1;
        fi;
      fi;
    fi;
  done;

  return 0;
}

prost_adddependency "grep";
prost_adddependency "pwd";
# log an installation
#
# 1: directory
# 2: source repository url
function prost_fs_loginstallation() {
  directory="$1"; shift;
  sourceurl="$1";

  if ! echo "$directory" | grep "^\/" >/dev/null; then
    directory="$(pwd)/$directory";
  fi;

  currentdate="$(date "+%s")";

  echo "$directory|$currentdate|$sourceurl" >>"$PROST_SCRIPT_PATH/log/installlog";

  prost_fs_cleanupinstallationlog;

  return 0;
}

prost_adddependency "tac";
prost_adddependency "cut";
prost_adddependency "grep";
# clean up the installation log and remove orphan entries
function prost_fs_cleanupinstallationlog() {
  if ! test -f "$PROST_SCRIPT_PATH/log/installlog"; then
    prost_printmsg "could not load installation log" "warning";
    return 0;
  fi;

  echo -n >"$PROST_SCRIPT_PATH/tmp/installlog";

  # walking reverse through the logfile
  # that makes sure only the latest entry is being used
  tac "$PROST_SCRIPT_PATH/log/installlog" |
    grep "^\/" |

    while read entry; do
      directory="$(echo "$entry" | cut -d"|" -f1)";

      # only add the entry if the directory still exists
      if test -d "$directory"; then
        url="$(echo "$entry" | cut -d"|" -f3)";

        quoteddirectory="$(echo "$directory" | sed -e "s/\//\\\\\//g")";

        if ! grep "^$quoteddirectory|" "$PROST_SCRIPT_PATH/tmp/installlog" >/dev/null; then
          # directory not yet set, add
          echo "$entry" >>"$PROST_SCRIPT_PATH/tmp/installlog";
        fi;
      fi;
    done;

  # rewrite the installation log in the correct order
  tac "$PROST_SCRIPT_PATH/tmp/installlog" >"$PROST_SCRIPT_PATH/log/installlog";

  return 0;
}

prost_adddependency "cut";
prost_adddependency "pwd";
prost_adddependency "grep";
# do a rollout (rewrite files etc) based on the installation log
#
# 1: directory (optional)
function prost_fs_rollout() {
  dodirectory="$1";

  # do the rollout... if there is an installation log
  if test -f "$PROST_SCRIPT_PATH/log/installlog"; then
    # clean up first
    prost_fs_cleanupinstallationlog;

    exec 3<&0;

    exec 0<"$PROST_SCRIPT_PATH/log/installlog";

    while read entry; do
      directory="$(echo "$entry" | cut -d"|" -f1)";

      # check if we should do this directory
      if [ "$dodirectory" != "" ]; then
        if ! echo "$directory" | grep "\/$dodirectory$" >/dev/null; then
          continue;
        fi;
      fi;

      reldirectory="$(basename "$directory")";
      sourceurl="$(echo "$entry" | cut -d"|" -f3)";

      prost_printmsg "updating installation in $directory";

      returndirectory="$(pwd)";

      scriptdirectory="$PROST_SCRIPT_PATH";
      if echo "$scriptdirectory" | grep "^\.\/\|^\.$" >/dev/null; then
        scriptdirectory="$(echo "$scriptdirectory" | cut -b3-)";
      fi;
      if ! echo "$scriptdirectory" | grep "^\/" >/dev/null; then
        scriptdirectory="$returndirectory$scriptdirectory";
      fi;

      cd "$directory";

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not change into directory $directory" "error";
        return 1;
      fi;

      # get the remote sql connection
      prost_printmsg "getting the local sql connection info";
      prost_drupal_drushcmd "drupal" sql-connect;

      PROST_LOCAL_MYUSER="$PROST_DEFAULT_MYUSER";
      PROST_LOCAL_MYPWD="$PROST_DEFAULT_MYPWD";

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not get the connection info" "warning";
      else
        cmdline="$PROST_RETURN_OUTPUT";

        # extract the variables
        if echo "$cmdline" | grep "\--user=" >/dev/null; then
          PROST_LOCAL_MYUSER="$(echo "$cmdline" | sed -e "s/^.*--user=['\"]*//g" -e "s/['\"].*$//g" -e "s/[ \t].*$//g")";
        fi;
        if echo "$cmdline" | grep "\--password=" >/dev/null; then
          PROST_LOCAL_MYPWD="$(echo "$cmdline" | sed -e "s/^.*--password=['\"]*//g" -e "s/['\"].*$//g" -e "s/[ \t].*$//g")";
        fi;
      fi;

      cd "../";

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not change one directory up" "error";
        return 1;
      fi;

      prost_separator;

      exec 4<&0;
      exec 0<&3;
      "$scriptdirectory/prost-install.sh" -noupdate -nobranchcheck -projectname "$reldirectory" -myuser "$PROST_LOCAL_MYUSER" -mypwd "$PROST_LOCAL_MYPWD" "$sourceurl" -stage directories -stop;
      "$scriptdirectory/prost-install.sh" -noupdate -nobranchcheck -projectname "$reldirectory" -myuser "$PROST_LOCAL_MYUSER" -mypwd "$PROST_LOCAL_MYPWD" "$sourceurl" -stage templates -stop;
      exec 3<&0;
      exec 0<&4;

      if [ "$?" -gt 0 ]; then
        prost_printmsg "installation in directory $directory failed" "error";
      fi;

      prost_separator;

      cd "$returndirectory";

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not change back into previous directory" "error";
        return 1;
      fi;
    done;

    exec 0<&3;
  fi;
}

prost_adddependency "rmdir";
# remove a directory
# can accept parameters in front of the directory they apply to
# example: prost_fs_rmdirs -check landingpage -check drupal -ignore dumps
# this would check if landingpage and drupal exist and if not return an error
# if dumps does not exist it would not cause an error
#
# 1-n: directory or parameter
#
# parameters:
#   -check    check if the directory exists prior to removing it
#   -ignore   if directory does not exist, just ignore
#   -fatal    if directory does not exist, return an error (instead of just printing a warning)
#   -rec      use recursive deletion (with files)
function prost_fs_rmdirs() {
  check=0;
  ignore=0;
  fatal=0;
  rec=0;

  returnvalue=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-check" ]; then
      check=1;
    elif [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-fatal" ]; then
      fatal=1;
    elif [ "$1" == "-rec" ]; then
      rec=1;
    else
      # this is a directory, remove it according to the parameters

      if [ "$check" -eq 1 ]; then
        if ! test -d "$1"; then
          # directory does not exist
          if [ "$fatal" -eq 1 ]; then
            # we shall fail
            prost_printmsg "directory $1 does not exist" "error";
            returnvalue=1;
          elif [ "$ignore" -eq 0 ]; then
            # we shall not ignore this
            prost_printmsg "directory $1 does not exist" "warning";
          fi;
        else
          if [ "$rec" -eq 0 ]; then
            rmdir "$1";
          else
            rm -fr "$1";
          fi;

          if [ "$?" -gt 0 ]; then
            returnvalue=1;
          fi;
        fi;
      else
        rmdir "$1";

        if [ "$?" -gt 0 ]; then
          returnvalue=1;
        fi;
      fi;

      # reset the parameters for the next directory
      check=0;
      ignore=0;
      fatal=0;
      rec=0;
    fi;

    shift;
  done;

  return $returnvalue;
}

prost_adddependency "grep";
prost_adddependency "sed";
prost_adddependency "sort";
prost_adddependency "tail";
# get a new tag number from the dumps directory
# tag must be in the form of {prefix}[0-9]+\.[0-9]+
#
# 1: dumps directory
# 2: tag prefix
function prost_fs_getnewtagnumber() {
  directory="$1"; shift;
  tagprefix="$1";

  if ! test -d "$directory"; then
    prost_printmsg "directory $directory does not exist" "error";
    return 1;
  fi;

  currenttag="$(ls "$directory" | grep "\-$tagprefix[0-9.][0-9.]*\.[0-9][0-9]*\." | sed -e "s/^.*-$tagprefix//g" -e "s/\.[^0-9].*$//g" | sort -V | tail -n1)";

  if [ "$currenttag" == "" ]; then
    currenttag="0.0";
  fi;

  currentprefix="$(echo "$currenttag" | sed -e "s/^\([0-9.][0-9.]*\)\.[0-9][0-9]*$/\1/g")";
  currentsuffix="$(echo "$currenttag" | sed -e "s/^[0-9.][0-9.]*\.\([0-9][0-9]*\)$/\1/g")";

  let newsuffix=$currentsuffix+1;

  PROST_RETURN_NEWTAGNUM="$currentprefix.$newsuffix";
}

prost_adddependency "grep";
# copies all drush templates to users home directory
function prost_fs_cmd() {
  # copy drush command templates to ~/.drush/
  prost_printmsg "copying drush commands to users home directory";

  if [ "$HOME" == "" ]; then
    prost_printmsg "Home directory not found" "error";
    return 1;
  fi;

  if ! test -d "$HOME/.drush"; then
    mkdir "$HOME/.drush";
    prost_printmsg "Added $HOME/.drush directory as not existing yet";
  fi;

  ls "$PROST_SCRIPT_PATH/templates" | grep "\.drush\.inc$" | while read file; do
    cp "$PROST_SCRIPT_PATH/templates/$file" "$HOME/.drush/";
  done;

  prost_printmsg "Copied drush commands to $HOME/.drush";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "at least one rollout failed" "error";
  fi;
}
