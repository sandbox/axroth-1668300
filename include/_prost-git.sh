#!/bin/sh

# prost git functions
# v0.1 ezi

# some default settings
PROST_INIT_ORIGIN="devrepo";
PROST_INIT_LIVEORIGIN="live";

prost_adddependency "sed";
# parse an url
#
# 1: url
function prost_git_parseurl {
  # try to determine a project name from an url
  PROST_RETURN_PROJECTURL="$(echo "$1" | sed -e "s/^.*[:\/]//g" -e "s/\.git$//gi")";
}

prost_adddependency "git";
# clone a project
#
# 1: origin; name to set
# 2: url
# 3: directory to clone into
# 4-n: parameter
#
# parameters:
#   -ignore  ignore if directory already exists
function prost_git_clone() {
  origin="$1"; shift;
  url="$1"; shift;
  directory="$1"; shift;

  ignore=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    fi;

    shift;
  done;

  # check if the target directory already exists
  # if ignore is set print a warning and continue
  if test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory already exists" "error";
      return 1;
    else
      prost_printmsg "directory $directory already exists" "warning";
    fi;
  fi;

  git clone --origin "$origin" "$url" "$directory";
  return $?;
}

prost_adddependency "git";
# init a git flow directory
#
# 1: directory
# 2-n: parameter
#
# parameter:
#   -ignore  flag if a missing directory should return an error
function prost_git_init() {
  directory="$1"; shift;

  ignore=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    fi;

    shift;
  done;

  # check if directory exists
  # if ignore is set print a warning and return without error
  if ! test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $directory does not exist" "warning";
      return 0;
    fi;
  fi;

  # try switching into the directory
  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not switch into directory $directory" "error";
    return 1;
  fi;

  # -d accepts all defaults
  git init;

  returnvalue="$?";

  # return to the previous path
  if [ "$directory" != "" ]; then
    cd "$OLDPWD";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not change back into previous directory" "error";
      return 1;
    fi;
  fi;

  return $returnvalue;
}

prost_adddependency "git";
# change a branch and optionally create it
#
# 1/2/3: branch name or parameter
#
# parameters:
#   -create
#   -directory [dir] (optional)
function prost_git_checkout() {
  create=0;
  createonmissing=0;
  directory="";
  branch="";

  while [ "$1" != "" ]; do
    if [ "$1" == "-create" ]; then
      create=1;
    elif [ "$1" == "-createonmissing" ]; then
      createonmissing=1;
    elif [ "$1" == "-directory" ]; then
      shift;
      directory="$1";
    else
      branch="$1";
    fi;

    shift;
  done;

  # check if we have a branch name
  if [ "$branch" == "" ]; then
    prost_printmsg "branch not specified" "error";
    return 1;
  fi;

  if [ "$directory" != "" ]; then
    # try switching into the directory
    cd "$directory";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not switch into directory $directory";
      return 1;
    fi;
  fi;

  # create the branch if set
  if [ "$createonmissing" -eq 1 ]; then
    # first silently try to checkout branch
    git checkout "$branch" &>/dev/null;

    # if this fails, try to create it
    if [ "$?" -gt 0 ]; then
      prost_printmsg "creating missing branch $branch" "warning";
      git checkout -b "$branch";
    fi;
  elif [ "$create" -eq 1 ]; then
    git checkout -b "$branch";
  else
    git checkout "$branch";
  fi;

  returnvalue="$?";

  # return to the previous path
  if [ "$directory" != "" ]; then
    cd "$OLDPWD";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not change back into previous directory" "error";
      return 1;
    fi;
  fi;

  # return the status of git checkout
  return $returnvalue;
}

prost_adddependency "git";
# add a file to git
#
# 1-n: files or directories
function prost_git_add() {
  git add $@;
  return $?;
}

prost_adddependency "git";
# add a file to git
#
# 1: files
# 2: commit message
function prost_git_commit() {
  message="$1"; shift;
  git commit -m "$message" $@;
  return $?;
}

prost_adddependency "git";
prost_adddependency "grep";
prost_adddependency "cut";
# get the current branch name
function prost_git_currentbranch() {
  PROST_RETURN_GITBRANCH="$(git branch | grep "^\*" | cut -b3-)";
  return $?;
}

prost_adddependency "git";
# push to the origin and branch
#
# 1: origin name
# 2: branch name
function prost_git_push() {
  git push --tags "$1" "$2";
  return $?;
}

prost_adddependency "git";
# fetch from the origin and branch
#
# 1: origin name
# 2: branch name
function prost_git_fetch() {
  git fetch "$1" "$2";
  return $?;
}

prost_adddependency "git";
# pull from the origin and branch
#
# 1: origin name
# 2: branch name
function prost_git_pull() {
  git pull "$1" "$2";
  return $?;
}

prost_adddependency "git";
# get a variable from git config
#
# 1: directory
# 2: variable name
function prost_git_vget() {
  directory="$1"; shift;
  variable="$1";

  # check if directory does exist
  if ! test -d "$directory"; then
    prost_printmsg "directory $directory does not exist" "error";
    return 1;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  PROST_RETURN_VARIABLE="$(git config "$1")";
  returnvalue="$?";

  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  return $returnvalue;
}

prost_adddependency "git";
# add a remote origin
#
# 1: directory
# 2: name
# 3: url
function prost_git_addremote() {
  directory="$1"; shift;
  origin="$1"; shift;
  url="$1"; shift;

  # try switching into the directory
  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not switch into directory $directory" "error";
    return 1;
  fi;

  git remote add "$origin" "$url";
  returnvalue="$?";

  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  return $returnvalue;
}

prost_adddependency "git";
prost_adddependency "head";
# get the latest commit id of a directory
#
# 1: directory
function prost_git_getlastcommitid() {
  directory="$1";

  if ! test -d "$directory"; then
    prost_printmsg "directory $directory does not exist" "error";
    return 1;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  commitid="$(git rev-parse HEAD | head -n1)";

  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  PROST_RETURN_COMMITID="$commitid";

  return 0;
}

prost_adddependency "git";
prost_adddependency "head";
prost_adddependency "sed";
# get the latest remote commit id of a directory and branch
#
# 1: directory
# 2: remote repository
# 3: branch
function prost_git_getlastremotecommitid() {
  directory="$1"; shift;
  repository="$1"; shift;
  branch="$1";

  if ! test -d "$directory"; then
    prost_printmsg "directory $directory does not exist" "error";
    return 1;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  commitid="$(git ls-remote "$repository" -h "refs/heads/$branch" | head -n1 | sed -e "s/[ \t].*$//g")";

  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  PROST_RETURN_COMMITID="$commitid";

  return 0;
}

# append a git tag to all dump files at the end of the name but before the file extension
#
# 1: directory
# 2: tag number
# 3: parameter
#
# parameters:
#   -maxdepth  maximum depth to walk recursive through the subdirectories
function prost_git_appendtagtodumps() {
  directory="$1"; shift;
  tagnum="$1"; shift;

  maxdepth=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-maxdepth" ]; then
      shift;
      maxdepth="$1";
    fi;

    shift;
  done;

  if ! test -d "$directory"; then
    prost_printmsg "dumps directory $directory not found" "warning";
    return 0;
  fi;

  deptharg="";
  if [ "$maxdepth" -gt 0 ]; then
    deptharg="-maxdepth $maxdepth";
  fi;

  find "$directory" $deptharg -type f |
    grep "$PROST_DB_SQLFILES" |
    grep -v "\-tag-[0-9.][0-9.]*\.[0-9][0-9]*\.*" |

    while read file; do
      fileprefix="$(echo "$file" | sed -e "s/$PROST_DB_SQLFILES//g")";
      filesuffix="$(echo "$file" | sed -e "s/^.*\($PROST_DB_SQLFILES\)/\1/g")";
      newfile="$fileprefix-tag-$tagnum$filesuffix";

      let filecount=$filecount+1;

      mv -v "$file" "$newfile";
    done;

  # TODO: report errors...
  return 0;
}

# add a git tag
#
# 1: tag
function prost_git_addtag() {
  git tag "$1";
  return $?;
}
