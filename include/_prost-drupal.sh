#!/bin/bash

# prost drupal functions
# v0.1 ezi

# TODO: move the directory and drupal exists into a separate function

# some default variables
PROST_DEFAULT_DRUPALADMINUSER="admin";
PROST_DEFAULT_DRUPALADMINPWD="dev";

prost_adddependency "drush";
prost_adddependency "cut";
# get the drupal version of a directory
#
# 1: directory
# 2-n: parameter
#
# parameters:
#   -ignore  ignore if directory does not exist
#   -major   only get the major part
#   -minor   only get the minor part
function prost_drupal_drupalversion() {
  PROST_RETURN_DRUPALVERSION="";

  directory="$1"; shift;

  ignore=0;
  # 0 = complete version
  part=0;

  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-major" ]; then
      part=1;
    elif [ "$1" == "-minor" ]; then
      part=2;
    fi;

    shift;
  done;

  # check if the directory does exist
  if ! test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $directory does not exist" "warning";
      return 0;
    fi;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  fullversion="$(drush status drupal_version --pipe)";

  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  if [ "$part" -eq 1 ]; then
    PROST_RETURN_DRUPALVERSION="$(echo "$fullversion" | cut -d. -f1)";
  elif [ "$part" -eq 2 ]; then
    PROST_RETURN_DRUPALVERSION="$(echo "$fullversion" | cut -d. -f2-)";
  else
    PROST_RETURN_DRUPALVERSION="$fullversion";
  fi;
}

prost_adddependency "grep";
prost_adddependency "cut";
prost_adddependency "drush";
# set a drupal variable via drush
#
# 1: drupal directory
# 2: remote alias (optional)
# 2-n: parameter or variable and value
#
# parameters:
#   -ignore      ignore if directory does not exist
#   -version     full drupal version required
#   -majversion  major drupal version required
#   -minversion  minor drupal version required
function prost_drupal_vset() {
  directory="$1"; shift;

  remotealias="";

  if echo "$1" | grep "^@" >/dev/null; then
    remotealias="$1";
    shift;
  fi;

  ignore=0;

  # get flag if missing directory should be ignored
  if [ "$1" == "-ignore" ]; then
    ignore=1;
    shift;
  fi;

  curversion="";
  curmajversion="";
  curminversion="";

  # try getting the drupal version, ignore if we cannot get it
  prost_drupal_drupalversion "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not determine drupal version" "warning";
  fi;

  curversion="$PROST_RETURN_DRUPALVERSION";
  curmajversion="$(echo "$PROST_RETURN_DRUPALVERSION" | cut -d. -f1)";
  curminversion="$(echo "$PROST_RETURN_DRUPALVERSION" | cut -d. -f2-)";

  # check if the directory does exist
  if ! test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $directory does not exist" "warning";
      return 0;
    fi;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  version="";
  majversion="";
  minversion="";

  returnvalue=0;

  # cycle through the arguments
  while [ "$1" != "" ]; do
    if [ "$1" == "-version" ]; then
      shift; version="$1";
    elif [ "$1" == "-majversion" ]; then
      shift; majversion="$1";
    elif [ "$1" == "-minversion" ]; then
      shift; minversion="$1";
    else
      # if version is set, check for it
      # overwrite order is version > majversion > minversion

      # this determines if we have a version filtering
      filter=0;
      # this marks if the version matched the filter
      match=1;

      if [ "$version" != "" ]; then
        filter=1;

        if [ "$version" != "$curversion" ]; then
          match=0;
        fi;
      fi;
      if [ "$majversion" != "" ]; then
        filter=1;

        if [ "$majversion" != "$curmajversion" ]; then
          match=0;
        fi;
      fi;
      if [ "$minversion" != "" ]; then
        filter=1;

        if [ "$minversion" != "$curminversion" ]; then
          match=0;
        fi;
      fi;

      # flag if variable has to be set
      doit=0;

      if [ "$filter" -eq 0 ]; then
        # no filter, always set variable
        doit=1;
      else
        if [ "$match" -eq 1 ]; then
          # filter set, if filter matched set variable
          doit=1;
        fi;
      fi;

      if [ "$doit" -eq 1 ]; then
        # get variable and value
        variable="$1";
        value="$2";

        # now do the actual magic
        if [ "$remotealias" != "" ]; then
          drush "$remotealias" vset --yes "$variable" "$value";
        else
          drush vset --yes "$variable" "$value";
        fi;

        # check if setting variable was successfull
        if [ "$?" -gt 0 ]; then
          if [ "$ignore" -eq 0 ]; then
            prost_printmsg "failed setting variable $variable in directory $directory" "error";
            returnvalue=1;
          else
            prost_printmsg "failed setting variable $variable in directory $directory" "warning";
          fi;
        fi;
      fi;

      version="";
      majversion="";
      minversion="";

      shift;
    fi;

    shift;
  done;

  #return to the previous directory
  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  return $returnvalue;
}

prost_adddependency "cut";
prost_adddependency "drush";
prost_adddependency "sed";
# get a drupal variable via drush
#
# 1: drupal directory
# 2-n: parameter or variable
#
# parameters:
#   -ignore      ignore if directory does not exist
#   -version     full drupal version required
#   -majversion  major drupal version required
#   -minversion  minor drupal version required
function prost_drupal_vget() {
  directory="$1"; shift;

  ignore=0;

  # get flag if missing directory should be ignored
  if [ "$1" == "-ignore" ]; then
    ignore=1;
    shift;
  fi;

  curversion="";
  curmajversion="";
  curminversion="";

  # try getting the drupal version, ignore if we cannot get it
  prost_drupal_drupalversion "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not determine drupal version" "warning";
  fi;

  curversion="$PROST_RETURN_DRUPALVERSION";
  curmajversion="$(echo "$PROST_RETURN_DRUPALVERSION" | cut -d. -f1)";
  curminversion="$(echo "$PROST_RETURN_DRUPALVERSION" | cut -d. -f2-)";

  # check if the directory does exist
  if ! test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $directory does not exist" "warning";
      return 0;
    fi;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  version="";
  majversion="";
  minversion="";

  returnvalue=0;

  # cycle through the arguments
  while [ "$1" != "" ]; do
    if [ "$1" == "-version" ]; then
      shift; version="$1";
    elif [ "$1" == "-majversion" ]; then
      shift; majversion="$1";
    elif [ "$1" == "-minversion" ]; then
      shift; minversion="$1";
    else
      # if version is set, check for it
      # overwrite order is version > majversion > minversion

      # this determines if we have a version filtering
      filter=0;
      # this marks if the version matched the filter
      match=1;

      if [ "$version" != "" ]; then
        filter=1;

        if [ "$version" != "$curversion" ]; then
          match=0;
        fi;
      fi;
      if [ "$majversion" != "" ]; then
        filter=1;

        if [ "$majversion" != "$curmajversion" ]; then
          match=0;
        fi;
      fi;
      if [ "$minversion" != "" ]; then
        filter=1;

        if [ "$minversion" != "$curminversion" ]; then
          match=0;
        fi;
      fi;

      # flag if variable has to be set
      doit=0;

      if [ "$filter" -eq 0 ]; then
        # no filter, always set variable
        doit=1;
      else
        if [ "$match" -eq 1 ]; then
          # filter set, if filter matched set variable
          doit=1;
        fi;
      fi;

      if [ "$doit" -eq 1 ]; then
        # get variable and value
        variable="$1";

        # now do the actual magic
        value="$(drush vget "$variable" 2>/dev/null)";

        # check if getting variable was successfull
        if [ "$?" -gt 0 ]; then
          if [ "$ignore" -eq 0 ]; then
            prost_printmsg "failed getting variable $variable in directory $directory" "error";
            returnvalue=1;
          else
            prost_printmsg "failed getting variable $variable in directory $directory" "warning";
          fi;
        else
          # only set the return variable if successfull
          # remove everything except the value
          PROST_RETURN_VARIABLE="$(echo "$value" | sed -e "s/^[^\"]*\"//g" -e "s/\"$//g")";
        fi;
      fi;

      version="";
      majversion="";
      minversion="";
    fi;

    shift;
  done;

  #return to the previous directory
  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  return $returnvalue;
}

prost_adddependency "drush";
# set a drupal user password
#
# 1: directory
# 2: username
# 3: pwd
# 4-5: parameter
#
# parameters:
#   -ignore    ignore if directory does not exist
#   -fallback  get and print another admin user if setting password fails
#   -live      use the live server alias
function prost_drupal_setuserpwd() {
  directory="$1"; shift;
  username="$1"; shift;
  userpwd="$1"; shift;

  ignore=0;
  fallback=0;
  live=0;

  # get flag if missing directory should be ignored
  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-fallback" ]; then
      fallback=1;
    elif [ "$1" == "-live" ]; then
      live=1;
    fi;

    shift;
  done;

  # try getting the drupal version, fail if we cannot get it
  prost_drupal_drupalversion "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not determine drupal version for directory $directory" "error";
    return 1;
  fi;

  version="$PROST_RETURN_DRUPALVERSION";

  # check if version is empty
  if [ "$version" == "" ]; then
    prost_printmsg "could not determine drupal version for directory $directory" "error";
    return 1;
  fi;

  # check if the directory does exist
  if ! test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $directory does not exist" "warning";
      return 0;
    fi;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  if [ $live -eq 1 ]; then
    drush @live user-password "$username" --password="$userpwd";
  else
    drush user-password "$username" --password="$userpwd";
  fi;
  returnvalue="$?";

  #return to the previous directory
  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  if [ "$returnvalue" -gt 0 ]; then
    if [ "$fallback" -eq 1 ]; then
      # getting admin name
      prost_printmsg "falling back to admin user" "warning";

      prost_drupal_getadminname "$directory";

      # check if we could get the admin user
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not determine admin user" "error";
        return 1;
      fi;

      username="$PROST_RETURN_DRUPALADMIN";

      if [ "$username" == "" ]; then
        prost_printmsg "could not determine admin user" "error";
        return 1;
      fi;

      prost_printmsg "admin user is $username";

      if [ $live -eq 1 ]; then
        prost_drupal_setuserpwd "$directory" "$username" "$userpwd" -ignore -live;
      else
        prost_drupal_setuserpwd "$directory" "$username" "$userpwd" -ignore;
      fi;

      returnvalue="$?";
    else
      if [ "$ignore" -eq 0 ]; then
        prost_printmsg "could not set user password" "error";
        return 1;
      else
        prost_printmsg "could not set user password" "warning";
        return 0;
      fi;
    fi;
  fi;

  return $returnvalue;
}

prost_adddependency "grep";
prost_adddependency "sed";
prost_adddependency "drush";
# get the drupal admin user 1
#
# 1: directory
# 2: parameter
#
# parameters:
#   -ignore  ignore if directory does not exist
#   -live    use live server alias
function prost_drupal_getadminname() {
  directory="$1"; shift;

  ignore=0;
  live=0;

  # get flag if missing directory should be ignored
  while [ "$1" != "" ]; do
    if [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-live" ]; then
      live=1;
    fi;

    shift;
  done;

  # try getting the drupal version, fail if we cannot get it
  prost_drupal_drupalversion "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not determine drupal version for directory $directory" "error";
    return 1;
  fi;

  version="$PROST_RETURN_DRUPALVERSION";

  # check if version is empty
  if [ "$version" == "" ]; then
    prost_printmsg "could not determine drupal version for directory $directory" "error";
    return 1;
  fi;

  # check if the directory does exist
  if ! test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $directory does not exist" "warning";
      return 0;
    fi;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  if [ $live -eq 1 ]; then
    PROST_RETURN_DRUPALADMIN="$(drush uinf 1 | grep "User name" | sed -e "s/^.*: *//g")";
  else
    PROST_RETURN_DRUPALADMIN="$(drush @live uinf 1 | grep "User name" | sed -e "s/^.*: *//g")";
  fi;
  returnvalue="$?";

  #return to the previous directory
  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  return $returnvalue;
}

prost_adddependency "grep";
prost_adddependency "drush";
prost_adddependency "cat";
prost_adddependency "head";
# run a drush command
#
# 1: directory
# 2: parameter
# 3: command
#
# parameters:
#   -ignore   ignore if directory does not exist
function prost_drupal_drushcmd() {
  directory="$1"; shift;

  ignore=0;

  # get flag if missing directory should be ignored
  while [ "$1" != "" ]; do
    if ! echo "$1" | grep "^-" >/dev/null; then
      # the command begins, break out of the loop
      break;
    fi;

    if [ "$1" == "-ignore" ]; then
      ignore=1;
    fi;

    shift;
  done;

  # try getting the drupal version, fail if we cannot get it
  prost_drupal_drupalversion "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not determine drupal version for directory $directory" "error";
    return 1;
  fi;

  version="$PROST_RETURN_DRUPALVERSION";

  # check if version is empty
  if [ "$version" == "" ]; then
    prost_printmsg "could not determine drupal version for directory $directory" "error";
    return 1;
  fi;

  # check if the directory does exist
  if ! test -d "$directory"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "directory $directory does not exist" "error";
      return 1;
    else
      prost_printmsg "directory $directory does not exist" "warning";
      return 0;
    fi;
  fi;

  cd "$directory";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $directory" "error";
    return 1;
  fi;

  drush --yes $@ &>"$PROST_SCRIPT_PATH/tmp/drushoutput";
  returnvalue="$?";
  cat "$PROST_SCRIPT_PATH/tmp/drushoutput";

  PROST_RETURN_OUTPUT="$(head -n1 "$PROST_SCRIPT_PATH/tmp/drushoutput")";

  #return to the previous directory
  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  return $returnvalue;
}

prost_adddependency "sed";
prost_adddependency "grep";
prost_adddependency "whoami";
prost_adddependency "ssh";
prost_adddependency "mysql";
# create drupal user 0
#
# 1: database
# 2: remote url (optional) in form of [user@]host
#    any directory appended by : will be discardes
# 3: parameter
#
# parameters:
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
function prost_drupal_createuser0() {
  # database user to use for connecting
  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  commands="DELETE FROM users WHERE uid=0 OR name=''; INSERT INTO users (uid,name) VALUES (0,''); UPDATE users SET uid=0 WHERE name=''";

  database="$1"; shift;

  remote="";

  if [ "$1" != "" ]; then
    if ! echo "$1" | grep "^-" >/dev/null; then
      remote="$(echo "$1" | sed -e "s/:.*$//g")";
      shift;
    fi;
  fi;

  # get the parameters
  while [ "$1" != "" ]; do
    if [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    fi;

    shift;
  done;

  if [ "$remote" != "" ]; then
    # this is a remote operation
    user="";
    host="";

    if echo "$remote" | grep "@" >/dev/null; then
      user="$(echo "$remote" | cut -d"@" -f1)";
      remote="$(echo "$remote" | cut -d"@" -f2-)";
    fi;

    if echo "$user" | grep ":" >/dev/null; then
      user="$(echo "$user" | cut -d":" -f1)";
    fi;
    if echo "$remote" | grep ":" >/dev/null; then
      remote="$(echo "$remote" | cut -d":" -f1)";
    fi;

    host="$remote";

    # did not specify user, get the current one
    if [ "$user" == "" ]; then
      user="$(whoami)";
    fi;

    if [ "$mypwd" != "" ]; then
      ssh "$user@$host" "mysql -u\"$myuser\" -p\"$mypwd\" \"$database\" --execute=\"$commands\"";
    else
      ssh "$user@$host" "mysql -u\"$myuser\" \"$database\" --execute=\"$commands\"";
    fi;
  else
    if [ "$mypwd" != "" ]; then
      mysql -u"$myuser" -p"$mypwd" "$database" --execute="$commands";
    else
      mysql -u"$myuser" "$database" --execute="$commands";
    fi;
  fi;

  return $?;
}

prost_adddependency "sed";
# update a drush alias file; append "/drupal" to the root directory
#
# 1: alias file
function prost_drupal_appenddrupalroot() {
  filename="$1";

  if ! grep -P "['\"]root['\"][ \t]*=>[ \t]*['\"][^'\"]*\/drupal['\"]" "$filename" >/dev/null; then
    sed -e "s/\(['\"]root['\"][ \t]*=>[ \t]*['\"][^'\"]*\)\(['\"]\)/\1\/drupal\2/g" "$filename" -i;
  fi;

  return $?;
}

# install a fresh drupal using a drush make file
#
# 1: parameter
#
# parameters:
#   -projectname [directory/database name]
#   -makefile [drush makefile to use]
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
function install_drupal {
  projectname="";
  makefile="";
  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  while [ "$1" != "" ]; do
    if [ "$1" == "-projectname" ]; then
      shift;
      projectname="$1";
    elif [ "$1" == "-makefile" ]; then
      shift;
      makefile="$1";
    elif [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    fi;

    shift;
  done;

  version=6;

  basemakefile="$(basename "$makefile")";
  if echo "$basemakefile" | grep "^d7\." >/dev/null; then
    version=7;
  fi;

  if [ "$PROST_INIT_VERSION" != "" ]; then
    version="$PROST_INIT_VERSION";
  fi;

  if [ "$projectname" == "" ]; then
    prost_printmsg "no project name given" "error";
    return 1;
  elif [ "$makefile" == "" ]; then
    prost_printmsg "no makefile given" "error";
    return 1;
  fi;

  # make this an absolute path if it is not already
  if ! echo "$makefile" | grep "\/" >/dev/null; then
    makefile="$PROST_SCRIPT_PATH/templates/$makefile";
  fi;

  # get drupal with some modules
  prost_printmsg "running drush makefile";
  drush make "$makefile" "$projectname";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "failed running drush makefile" "error";
    return 1;
  fi;

  cd "$projectname";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change into directory $projectname" "error";
    return 1;
  fi;

  # do site install
  if [ "$version" == "7" ]; then
    prost_printmsg "running site-install for profile standard";
    drush site-install standard --db-url="mysql://$myuser:$mypwd@localhost/$projectname" --account-pass="$PROST_DEFAULT_DRUPALADMINPWD";
  else
    prost_printmsg "running site-install for profile default";
    drush site-install default --db-url="mysql://$myuser:$mypwd@localhost/$projectname" --account-pass="$PROST_DEFAULT_DRUPALADMINPWD";
  fi;

  cd "$OLDPWD";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not change back into previous directory" "error";
    return 1;
  fi;

  return 0;
}
