#!/bin/bash

# prost database functions
# v0.1 ezi

# some default settings
PROST_DEFAULT_MYUSER="root";
PROST_DEFAULT_MYPWD="";
PROST_DEFAULT_DUMPDIR="dumps";

#some other settings
PROST_DB_SQLFILES="\.sql$\|\.sql\.gz$\|\.sql\.bz2$\|\.dump$\|\.dump\.gz$\|\.dump\.bz2$\|\.mysql$\|\.mysql\.gz$\|\.mysql\.bz2$";

prost_adddependency "mysql";
prost_adddependency "grep";
# create a database
#
# 1: database name
# 2: create mysql user: username
# 3: create mysql user: password
# 4-n: parameters
#
# parameters:
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
function prost_db_create() {
  # database user to use for connecting
  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  # database name to create
  database="$1";
  shift;

  # get user to create only if both are set
  # set an empty password by passing an empty parameter ("")
  createuser="";
  createpwd="";

  # check if the arguments are not empty and not parameters
  # password is optional
  if [ "$1" != "" ]; then
    if ! echo "$1" | grep "^-" >/dev/null; then
      if ! echo "$2" | grep "^-" >/dev/null; then
        createuser="$1"; shift;
        createpwd="$1"; shift;
      fi;
    fi;
  fi;

  # get the parameters
  while [ "$1" != "" ]; do
    if [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    fi;

    shift;
  done;

  # check if we have a database name
  if [ "$database" == "" ]; then
    prost_printmsg "database name not specified" "error";
    return 1;
  fi;

  # create the database
  if [ "$mypwd" != "" ]; then
    mysql -u"$myuser" -p"$mypwd" --execute="CREATE DATABASE \`$database\`";
  else
    mysql -u"$myuser" --execute="CREATE DATABASE \`$database\`";
  fi;

  returnvalue="$?";

  # check if database creation was successfull
  if [ "$returnvalue" -gt 0 ]; then
    return $returnvalue;
  fi;

  # create the database user
  if [ "$createuser" != "" ]; then
    if [ "$mypwd" != "" ]; then
      mysql -u"$myuser" -p"$mypwd" --execute="GRANT ALL PRIVILEGES ON \`$database\`.* IDENTIFIED BY '$createpwd'";
    else
      mysql -u"$myuser" --execute="GRANT ALL PRIVILEGES ON \`$database\`.* IDENTIFIED BY '$createpwd'";
    fi;
  fi;

  returnvalue="$?";

  return $returnvalue;
}

prost_adddependency "grep";
prost_adddependency "head";
prost_adddependency "tail";
prost_adddependency "ls";
prost_adddependency "cat";
prost_adddependency "wc";
prost_adddependency "zcat";
prost_adddependency "bzcat";
prost_adddependency "mysql";
prost_adddependency "ssh";
# import a database dump
#
# 1: database name
# 2: file or directory to import
# 3: remote url (optional) in form of [user@]host
#    any directory appended by : will be discardes
# 4-n: parameters
#
# parameters:
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
#   -first     - first file on directory import
#   -last      - if import is a directory, use the last import according to filename
#   -select    - list possible files and let the user select
#   -ignore    - ignore if import does not exist
#   -dumpfirst - dump database before importing
#   -skip      - skip file when listing
function prost_db_import() {
  # get database name and import file/directory
  database="$1"; shift;
  import="$1"; shift;

  # directory choice: 0=none, 1=first, 2=last, 3=select
  dirchoice=0;

  # flag if missing file/directory has to be ignored
  ignore=0;

  # flag if dump has to be done before import
  dumpfirst=0;

  # file to skip
  skipfile="";

  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  remote="";

  if [ "$1" != "" ]; then
    if ! echo "$1" | grep "^-" >/dev/null; then
      remote="$(echo "$1" | sed -e "s/:.*$//g")";
      shift;
    fi;
  fi;

  while [ "$1" != "" ]; do
    if [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    elif [ "$1" == "-first" ]; then
      dirchoice=1;
    elif [ "$1" == "-last" ]; then
      dirchoice=2;
    elif [ "$1" == "-select" ]; then
      dirchoice=3;
    elif [ "$1" == "-ignore" ]; then
      ignore=1;
    elif [ "$1" == "-dumpfirst" ]; then
      dumpfirst=1;
    elif [ "$1" == "-skip" ]; then
      shift;
      skipfile="$1";
    fi;

    shift;
  done;

  # check if we have all we need
  if [ "$database" == "" ]; then
    prost_printmsg "database name not specified" "error";
    return 1;
  fi;

  if [ "$import" == "" ]; then
    prost_printmsg "import not specified" "error";
    return 1;
  fi;

  # the file that would actually be imported
  importfile="";

  # check what type of import this is
  if test -f "$import"; then
    # this is a single file, import directly
    importfile="$import";
  elif test -d "$import"; then
    # this is a directory, import according to parameters
    if [ "$dirchoice" -eq 1 ]; then
      importfile="$import/$(ls "$import/" | grep -i "$PROST_DB_SQLFILES" | head -n1)";
    elif [ "$dirchoice" -eq 2 ]; then
      importfile="$import/$(ls "$import/" | grep -i "$PROST_DB_SQLFILES" | tail -n1)";
    elif [ "$dirchoice" -eq 3 ]; then
      # flag if user has chosen to import or discard
      # 0 means not chosen yet, 1 means user has chosen
      setfile=0;

      importfile="";

      # loop until the user has chosen to import a file or selected "none"
      while [ $setfile -eq 0 ]; do
        # let the user chose the file
        echo "     0  none";
        prost_db_listdumpfiles "$import" -skip "$skipfile";

        # get the file counter
        prost_db_countdumpfiles "$import" -skip "$skipfile";
        max=$?;

        prost_script_numeric "please select the number for which file to import: " 0 "$max";
        selected=$?;

        # if the answer is not 0 (none), ask if user wants to import
        if [ $selected -gt 0 ]; then
          setfileaction=0;

          while [ $setfileaction -eq 0 ]; do
            prost_script_multichoice "please select if to import, diff or select another file or to cancel the import: " "import|diff|select|cancel";

            if [ "$PROST_RETURN_RESPONSE" == "import" ]; then
              importfile="$import/$(prost_db_getdumpfileselection "$import" $selected -skip "$skipfile")";
              setfileaction=1;
              setfile=1;
            elif [ "$PROST_RETURN_RESPONSE" == "diff" ]; then
              importfile="$import/$(prost_db_getdumpfileselection "$import" $selected -skip "$skipfile")";
              prost_db_diff "$database" "$importfile" "variable" -myuser "$myuser" -mypwd "$mypwd";
            elif [ "$PROST_RETURN_RESPONSE" == "select" ]; then
              importfile="";
              setfileaction=1;
            elif [ "$PROST_RETURN_RESPONSE" == "cancel" ]; then
              importfile="";
              setfileaction=1;
              setfile=1;
            else
              importfile="";
            fi;
          done;
        else
          setfile=1;
        fi;
      done;

      # user selected to import nothing
      if [ "$importfile" == "" ]; then
        return 0;
      fi;
    else
      prost_printmsg "when importing from directory you have to specify which one to use" "error";
      return 1;
    fi;
  else
    if [ "$ignore" -eq 0 ]; then
      # this is something else we don't support yet
      prost_printmsg "import $import not found or supported" "error";
      return 1;
    else
      prost_printmsg "nothing to import";
      return 0;
    fi;
  fi;

  # check if importfile is set and exists
  if [ "$importfile" == "" ]; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "importfile could not be determined" "error";
      return 1;
    else
      return 0;
    fi;
  fi;

  # check if importfile exists
  if ! test -f "$importfile"; then
    if [ "$ignore" -eq 0 ]; then
      prost_printmsg "importfile $importfile is not a file" "error";
      return 1;
    else
      prost_printmsg "nothing to import";
      return 0;
    fi;
  fi;

  # now import the file

  # check if this is a compressed file
  compressed=0;
  catcmd="cat";

  if echo "$importfile" | grep -i "\.tar\.gz$" >/dev/null; then
    prost_printmsg "file type tar.gz not supported" "error";
    return 1;
  elif echo "$importfile" | grep -i "\.tar\.bz$" >/dev/null; then
    prost_printmsg "file type tar.bz not supported" "error";
    return 1;
  elif echo "$importfile" | grep -i "\.tar\.bz2$" >/dev/null; then
    prost_printmsg "file type tar.bz2 not supported" "error";
    return 1;
  elif echo "$importfile" | grep -i "\.gz$" >/dev/null; then
    compressed=1;
    catcmd="zcat";
  elif echo "$importfile" | grep -i "\.gz$" >/dev/null; then
    compressed=1;
    catcmd="zcat";
  elif echo "$importfile" | grep -i "\.bz$" >/dev/null; then
    compressed=1;
    catcmd="bzcat";
  elif echo "$importfile" | grep -i "\.bz2$" >/dev/null; then
    compressed=1;
    catcmd="bzcat";
  fi;

  if [ $dumpfirst -eq 1 ]; then
    # do the dump the manual way
    prost_printmsg "dumping database tables via mysqldump";
    prost_db_dumptables "$database" -myuser "$myuser" -mypwd "$mypwd" -nodatafile "$PROST_SCRIPT_PATH/templates/dump_nodata";
  fi;

  prost_printmsg "importing file $importfile";

  if [ "$remote" != "" ]; then
    # this is a remote operation
    user="";
    host="";

    if echo "$remote" | grep "@" >/dev/null; then
      user="$(echo "$remote" | cut -d"@" -f1)";
      remote="$(echo "$remote" | cut -d"@" -f2-)";
    fi;

    if echo "$user" | grep ":" >/dev/null; then
      user="$(echo "$user" | cut -d":" -f1)";
    fi;
    if echo "$remote" | grep ":" >/dev/null; then
      remote="$(echo "$remote" | cut -d":" -f1)";
    fi;

    host="$remote";

    # did not specify user, get the current one
    if [ "$user" == "" ]; then
      user="$(whoami)";
    fi;

    if [ "$mypwd" != "" ]; then
      "$catcmd" "$importfile" | ssh "$user@$host" "mysql -u\"$myuser\" -p\"$mypwd\" \"$database\"";
    else
      "$catcmd" "$importfile" | ssh "$user@$host" "mysql -u\"$myuser\" \"$database\"";
    fi;
  else
    if [ "$mypwd" != "" ]; then
      "$catcmd" "$importfile" | mysql -u"$myuser" -p"$mypwd" "$database";
    else
      "$catcmd" "$importfile" | mysql -u"$myuser" "$database";
    fi;
  fi;

  return $?;
}

prost_adddependency "tail";
prost_adddependency "grep";
prost_adddependency "cat";
prost_adddependency "sed";
# list dump files sorted by date in the filename
#
# 1: directory
function prost_db_listdumpfiles() {
  directory="$1"; shift;

  # file to skip
  skipfile="";

  while [ "$1" != "" ]; do
    if [ "$1" == "-skip" ]; then
      shift;
      skipfile="$1";
    fi;

    shift;
  done;

  if ! test -d "$directory"; then
    prost_printmsg "directory $directory does not exist" "error";
    return 1;
  fi;

  ls "$directory/" | grep "$PROST_DB_SQLFILES" | grep -v "^$skipfile$" |
    while read file; do
      fdate="$(echo "$file" | sed -e "s/^.*\([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\)T\([0-9][0-9]-[0-9][0-9]-[0-9][0-9]\).*$/\1 \2/g")";
      size="$(stat -c %s "$directory/$file")";
      echo -n "$fdate";
      echo -ne "\t"; echo -n "$size";
      echo -ne "\t"; echo "$file";
      let counter=$counter+1;
    done |
    sort |
    cat -n;
}

prost_adddependency "tail";
prost_adddependency "grep";
prost_adddependency "sed";
# get the selected file from a list of dump files sorted by date in the filename
#
# 1: directory
# 2: selected number
function prost_db_getdumpfileselection() {
  directory="$1"; shift;
  selected="$1"; shift;

  # file to skip
  skipfile="";

  while [ "$1" != "" ]; do
    if [ "$1" == "-skip" ]; then
      shift;
      skipfile="$1";
    fi;

    shift;
  done;

  if ! test -d "$directory"; then
    prost_printmsg "directory $directory does not exist" "error";
    return 1;
  fi;

  ls "$directory/" | grep "$PROST_DB_SQLFILES" | grep -v "^$skipfile$" |
    while read file; do
      fdate="$(echo "$file" | sed -e "s/^.*\([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\)T\([0-9][0-9]-[0-9][0-9]-[0-9][0-9]\).*$/\1 \2/g")";
      echo -n "$fdate";
      echo "|$file";
    done |
    sort |
    cut -d\| -f2- |
    tail -n+$selected | head -n1;
}

prost_adddependency "tail";
prost_adddependency "grep";
# get the selected file from a list of dump files sorted by date in the filename
#
# 1: directory
function prost_db_countdumpfiles() {
  directory="$1"; shift;

  # file to skip
  skipfile="";

  while [ "$1" != "" ]; do
    if [ "$1" == "-skip" ]; then
      shift;
      skipfile="$1";
    fi;

    shift;
  done;

  if ! test -d "$directory"; then
    prost_printmsg "directory $directory does not exist" "error";
    return 0;
  fi;

  counter=$(ls "$directory/" | grep "$PROST_DB_SQLFILES" | grep -v "^$skipfile$" | wc -l);

  return $counter;
}

prost_adddependency "date";
prost_adddependency "mysql";
prost_adddependency "grep";
prost_adddependency "mysqldump";
prost_adddependency "gzip";
# dump a database
# optionally dump some tables wihout content
#
# 1: database name
# 2-n: parameter
#
# parameters:
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
#   -nodatafile  file that includes all tables for which only the structure has to be dumped
function prost_db_dumptables() {
  # database user to use for connecting
  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  dumpdir="$PROST_DEFAULT_DUMPDIR";
  dumpfile="$(date "+%Y-%m-%dT%H-%M-%S")-mysqldump.mysql.gz";
  PROST_RETURN_DUMPFILE="$dumpfile";

  database="$1"; shift;
  nodatafile="";

  while [ "$1" != "" ]; do
    if [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    elif [ "$1" == "-nodatafile" ]; then
      shift; nodatafile="$1";
    fi;

    shift;
  done;

  # check if nodata file exists if set
  if [ "$nodatafile" != "" ]; then
    if ! test -f "$nodatafile"; then
      prost_printmsg "nodata file not found" "error";
      return 1;
    fi;
  fi;

  # check if dump directory does exist
  if ! test -d "$dumpdir"; then
    prost_printmsg "dump file directory not found" "error";
    return 1;
  fi;

  prost_printmsg "dumping tables into $dumpdir/$dumpfile";

  # get all tables from the database
  if [ "$mypwd" != "" ]; then
    mysql -u"$myuser" -p"$mypwd" "$database" --execute="show tables" | grep -v ^Tables_in_ |
      while read table; do
        nodata=0;

        if [ "$nodatafile" != "" ]; then
          # check if table has to be dumped without data
          if ! grep "^$table$" "$nodatafile" >/dev/null; then
            # complete dump
            mysqldump --skip-lock-tables -u"$myuser" -p"$mypwd" "$database" "$table";
          else
            # dump without data
            mysqldump --skip-lock-tables -d -u"$myuser" -p"$mypwd" "$database" "$table";
          fi;
        else
          # complete dump
          mysqldump --skip-lock-tables -u"$myuser" -p"$mypwd" "$database" "$table";
        fi;
      done | gzip >"$dumpdir/$dumpfile";
  else
    mysql -u"$myuser" "$database" --execute="show tables" | grep -v ^Tables_in_ |
      while read table; do
        nodata=0;

        if [ "$nodatafile" != "" ]; then
          # check if table has to be dumped without data
          if ! grep "^$table$" "$nodatafile" >/dev/null; then
            # complete dump
            mysqldump --skip-lock-tables -u"$myuser" "$database" "$table";
          else
            # dump without data
            mysqldump --skip-lock-tables -d -u"$myuser" "$database" "$table";
          fi;
        else
          # complete dump
          mysqldump --skip-lock-tables -u"$myuser" "$database" "$table";
        fi;
      done | gzip >"$dumpdir/$dumpfile";
  fi;

  return $?;
}

prost_adddependency "grep";
prost_adddependency "mysql";
prost_adddependency "sort";
prost_adddependency "cat";
prost_adddependency "zcat";
prost_adddependency "bzcat";
prost_adddependency "sed";
prost_adddependency "cut";
prost_adddependency "mysqldump";
prost_adddependency "head";
prost_adddependency "tail";
prost_adddependency "stty";
prost_adddependency "diff";
# database diff - compares a database dump file to a mysql database
# does compare the tables (added, removed)
# does compare entries specific tables
# only "INSERT INTO" and "REPLACE INTO" is supported
#
# 1: database
# 2: dump file
# 3-n: tables to compare entries
# n+1: parameter
#
# parameters:
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
function prost_db_diff() {
  # database user to use for connecting
  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  database="$1"; shift;
  dumpfile="$1"; shift;

  difftables="";

  # get the table names to compare values
  while [ "$1" != "" ]; do
    # break out of the loop if we hit the first parameter
    if echo "$1" | grep "^-" >/dev/null; then
      break;
    fi;

    prefix="";
    if [ "$difftables" != "" ]; then
      prefix="|";
    fi;

    # append the table name, use a separator (prefix) if there is already a name in the list
    difftables="$difftables$prefix$1";

    shift;
  done;

  # get the parameters
  while [ "$1" != "" ]; do
    if [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    fi;

    shift;
  done;

  # check if the dump file exists
  if ! test -f "$dumpfile"; then
    prost_printmsg "dump file $dumpfile does not exist" "error";
    return 1;
  fi;

  # get the tables out of the database
  if [ "$mypwd" != "" ]; then
    mysql -u"$myuser" -p"$mypwd" "$database" --execute="show tables" | grep -v ^Tables_in_ | sort >"$PROST_SCRIPT_PATH/tmp/dblist";
  else
    mysql -u"$myuser" "$database" --execute="show tables" | grep -v ^Tables_in_ | sort >"$PROST_SCRIPT_PATH/tmp/dblist";
  fi;

  # get the tables out of the dump file

  # check if this is a compressed file
  compressed=0;
  catcmd="cat";

  if echo "$dumpfile" | grep -i "\.tar\.gz$" >/dev/null; then
    prost_printmsg "file type tar.gz not supported" "error";
    return 1;
  elif echo "$dumpfile" | grep -i "\.tar\.bz$" >/dev/null; then
    prost_printmsg "file type tar.bz not supported" "error";
    return 1;
  elif echo "$dumpfile" | grep -i "\.tar\.bz2$" >/dev/null; then
    prost_printmsg "file type tar.bz2 not supported" "error";
    return 1;
  elif echo "$dumpfile" | grep -i "\.gz$" >/dev/null; then
    compressed=1;
    catcmd="zcat";
  elif echo "$dumpfile" | grep -i "\.gz$" >/dev/null; then
    compressed=1;
    catcmd="zcat";
  elif echo "$dumpfile" | grep -i "\.bz$" >/dev/null; then
    compressed=1;
    catcmd="bzcat";
  elif echo "$dumpfile" | grep -i "\.bz2$" >/dev/null; then
    compressed=1;
    catcmd="bzcat";
  fi;

  "$catcmd" "$dumpfile" | grep -iP "^[ \t]*CREATE TABLE[ \t]" | sed -e "s/^[ \t]*CREATE TABLE[ \t][ \t]*\`*//gi" -e "s/\`* .*$//g" | sort >"$PROST_SCRIPT_PATH/tmp/dumplist";

  # now compare the two files
  prost_separator;
  prost_printmsg "comparing table lists";
  prost_separator;

  # look for tables in the database which cannot be found in the dump file
  cat "$PROST_SCRIPT_PATH/tmp/dblist" |
    while read table; do
      if ! grep "^$table$" "$PROST_SCRIPT_PATH/tmp/dumplist" >/dev/null; then
        prost_printmsg "$table not in dumpfile" "color red";
      fi;
    done;

  # look for tables in the dump file which cannot be found in the database
  cat "$PROST_SCRIPT_PATH/tmp/dumplist" |
    while read table; do
      if ! grep "^$table$" "$PROST_SCRIPT_PATH/tmp/dblist" >/dev/null; then
        prost_printmsg "$table added in dumpfile" "color green";
      fi;
    done;

  if [ "$difftables" != "" ]; then
    # now compare the special tables
    prost_separator;
    prost_printmsg "comparing table entries for $difftables";
    prost_subseparator;

    tmpdifftables="$difftables";
    while [ "$tmpdifftables" != "" ]; do
      table="$(echo "$tmpdifftables" | cut -d"|" -f1)";

      prost_printmsg "comparing table $table";

      # remove choice or make empty if this was the last
      if echo "$tmpdifftables" | grep "|" >/dev/null; then
        tmpdifftables="$(echo "$tmpdifftables" | cut -d"|" -f2-)";
      else
        tmpdifftables="";
      fi;

      # get the inserts from db table
      if [ "$mypwd" != "" ]; then
        mysqldump --skip-lock-tables --skip-quote-names --skip-extended-insert --compact -u"$myuser" -p"$mypwd" "$database" "$table" |
          grep -iP "^[ \t]*INSERT[ \t][ \t]*INTO|^[ \t]*REPLACE[ \t][ \t]*INTO" |
          head -n10000 |
          sed -e "s/^.*[ \t]VALUES[ \t]([ \t]*//gi" -e "s/[ \t]*);*$//g" -e "s/\\\\\"/\"/g" |
          sed -e "s/^[ \t]*['\"]//g" -e "s/['\"][ \t]*$//g" -e "s/^\([^'\"]*\)['\"][ \t]*,[ \t]*['\"]/\1 \t /g" |
          sort >"$PROST_SCRIPT_PATH/tmp/dbtable";
      else
        mysqldump --skip-lock-tables --skip-quote-names --skip-extended-insert --compact -u"$myuser" "$database" "$table" |
          grep -iP "^[ \t]*INSERT[ \t][ \t]*INTO|^[ \t]*REPLACE[ \t][ \t]*INTO" |
          head -n10000 |
          sed -e "s/^.*[ \t]VALUES[ \t]([ \t]*//gi" -e "s/[ \t]*);*$//g" -e "s/\\\\\"/\"/g" |
          sed -e "s/^[ \t]*['\"]//g" -e "s/['\"][ \t]*$//g" -e "s/^\([^'\"]*\)['\"][ \t]*,[ \t]*['\"]/\1 \t /g" |
          sort >"$PROST_SCRIPT_PATH/tmp/dbtable";
      fi;

      # get the inserts from dump

      # flag if the right table has already started
      started=0;

      # walk through the dump file
      "$catcmd" "$dumpfile" |
        sed -e "s/),[ \t]*(/)\nINSERT INTO \`$table\` VALUES (/g" |
        grep -iP -A10000 "^[ \t]*CREATE[ \t][ \t]*TABLE[ \t]\`*$table\`*[ \t]" |
        tail -n+2 |
        sed "/^[ \t]*CREATE[ \t][ \t]*TABLE[ \t]/,$ d" |
        grep -iP "^[ \t]*INSERT[ \t][ \t]*INTO|^[ \t]*REPLACE[ \t][ \t]*INTO" |
        sed -e "s/^.*[ \t]VALUES[ \t]([ \t]*//gi" -e "s/[ \t]*);*$//g" -e "s/\\\\\"/\"/g" |
        sed -e "s/^[ \t]*['\"]//g" -e "s/['\"][ \t]*$//g" -e "s/^\([^'\"]*\)['\"][ \t]*,[ \t]*['\"]/\1 \t /g" |
        sort >"$PROST_SCRIPT_PATH/tmp/dumptable";

      # now compare the two files
      # count how many differences there are
      counter=0;

      # get the screen width
      screenwidth="$(stty size | head -n1 | cut -d' ' -f2)";

      # set default if not a number
      if ! echo "$screenwidth" | grep "^[0-9][0-9]*$" >/dev/null; then
        screenwidth="100";
      elif [ $screenwidth -ge 108 ]; then
        # leave border for maximum of 8 spaces as tab
        let screenwidth=$screenwidth-8;
      else
        # minimum width
        screenwidth="100";
      fi;

      diff "$PROST_SCRIPT_PATH/tmp/dbtable" "$PROST_SCRIPT_PATH/tmp/dumptable" |
        cut -b1-$screenwidth |

        while read line; do
          if echo "$line" | grep "^[0-9,][0-9,]*[a-z][0-9,][0-9,]*$" >/dev/null; then
            prost_subseparator;
          elif echo "$line" | grep "^<" >/dev/null; then
            let counter=$counter+1;
            prost_script_setcolor "red";
            echo -E "$line";
            prost_script_resetcolor;
          elif echo "$line" | grep "^>" >/dev/null; then
            let counter=$counter+1;
            prost_script_setcolor "green";
            echo -E "$line";
            prost_script_resetcolor;
          elif echo "$line" | grep "^---$" >/dev/null; then
            prost_printmsg "--- vs ---" "color cyan";
          else
            echo -E "$line";
          fi;
        done;
    done;

    prost_subseparator;
  fi;
}

# check if a database exists
#
# 1: database name
# 2-n: parameter
#
# parameters:
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
function prost_db_checkdatabase() {
  # database user to use for connecting
  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  database="$1"; shift;

  while [ "$1" != "" ]; do
    if [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    fi;

    shift;
  done;

  returnvalue=1;

  mysql -u"$myuser" -p"$mypwd" --execute="show databases like \"$database\"" | grep -v "^Database " >/dev/null;

  if [ "$?" -gt 0 ]; then
    returnvalue=0;
  fi;

  return $returnvalue;
}

# drop a database
#
# 1: database name
# 2-n: parameter
#
# parameters:
#   -myuser [mysql username to connect with]
#   -mypwd [mysql user password to connect with]
function prost_db_dropdatabase() {
  # database user to use for connecting
  myuser="$PROST_DEFAULT_MYUSER";
  mypwd="$PROST_DEFAULT_MYPWD";

  database="$1"; shift;

  while [ "$1" != "" ]; do
    if [ "$1" == "-myuser" ]; then
      shift;
      myuser="$1";
    elif [ "$1" == "-mypwd" ]; then
      shift;
      mypwd="$1";
    fi;

    shift;
  done;

  mysql -u"$myuser" -p"$mypwd" --execute="drop database \`$database\`";

  return $?;
}
