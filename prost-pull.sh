#!/bin/bash

# prost project initialization script
# v0.1 ezi

# include the basic functions
PROST_SCRIPT_PATH="$(dirname "$0")";
. "$PROST_SCRIPT_PATH/include/_prost-base.sh";

# run init to get the required information
prost_init "$@";

if [ $PROST_INIT_NODEPENDENCIES -eq 0 ]; then
  # run some dependency checks
  prost_printmsg "running dependency checks";
  prost_script_checkdependencies -quiet;

  if [ "$?" -gt 0 ]; then
    prost_printmsg "at least one dependency check failed, aborting" "error";
    exit 1;
  fi;
fi;

# get the current project name
prost_fs_getcurrentproject;

# check if project could be found
if [ "$?" -gt 0 ]; then
  prost_printmsg "current project could not be determined, aborting" "error";
  exit 1;
fi;

PROST_PROJECT_PATH="$PROST_RETURN_PROJECTPATH";
PROST_PROJECT_NAME="$PROST_RETURN_PROJECTNAME";

prost_printmsg "Current project path: $PROST_PROJECT_PATH";
prost_printmsg "Current project name: $PROST_PROJECT_NAME";

# change to the project path to make sure subpaths are right
prost_printmsg "changing into main project directory";
cd "$PROST_PROJECT_PATH";

# check if cd was successfull
if [ "$?" -gt 0 ]; then
  prost_printmsg "could not change into main project directory, aborting" "error";
  exit 1;
fi;

# check git branch
prost_printmsg "checking git branch";
prost_git_currentbranch;

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not determine current git branch, aborting" "error";
  exit 1;
fi;

PROST_GIT_BRANCH="$PROST_RETURN_GITBRANCH";

if [ "$PROST_GIT_BRANCH" == "" ]; then
  prost_printmsg "could not determine current git branch, aborting" "error";
  exit 1;
fi;

# fetching from origin
prost_printmsg "fetching from development server";
prost_git_fetch "$PROST_INIT_ORIGIN" "$PROST_GIT_BRANCH";

if [ "$?" -gt 0 ]; then
  prost_printmsg "fetch was not successfull, aborting" "error";
  exit 1;
fi;

# pulling from origin
prost_printmsg "pulling from development server";
prost_git_pull "$PROST_INIT_ORIGIN" "$PROST_GIT_BRANCH";

if [ "$?" -gt 0 ]; then
  prost_printmsg "pull was not successfull, aborting" "error";
  exit 1;
fi;

# only import if said to do
if [ "$PROST_INIT_IMPORTDB" -eq 1 ]; then
  # check if the database already exists
  prost_printmsg "checking if database exists";
  prost_db_checkdatabase "$PROST_PROJECT_NAME" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";
  dbexists="$?";

  dropdb=0;
  createdb=1;

  if [ "$dbexists" -eq 1 ]; then
    dropdb=1;

    prost_script_yesno "database already exists. Drop existing database? ";

    # 0 is false in this case, exit script
    if [ "$?" -eq 0 ]; then
      dropdb=0;
      createdb=0;
    fi;
  fi;

  # do the dump the manual way
  prost_printmsg "dumping database tables via mysqldump";
  prost_db_dumptables "$PROST_PROJECT_NAME" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD" -nodatafile "$PROST_SCRIPT_PATH/templates/dump_nodata";

  dumpfile="$PROST_RETURN_DUMPFILE";

  if [ $dropdb -eq 1 ]; then
    prost_printmsg "dropping database";
    prost_db_dropdatabase "$PROST_PROJECT_NAME" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";

    # check if database operations have been successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not drop database, aborting" "error";
      exit 1;
    fi;
  fi;

  if [ $createdb -eq 1 ]; then
    # create database
    prost_printmsg "creating database";
    prost_db_create "$PROST_PROJECT_NAME" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";

    # check if database operations have been successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not create database, aborting" "error";
      exit 1;
    fi;
  fi;

  # import a database dump
  prost_printmsg "importing database backup";
  prost_db_import "$PROST_PROJECT_NAME" "dumps" -select -ignore -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD" -skip "$dumpfile";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "backup could not be imported, aborting" "error";
    exit 1;
  fi;

  # prepending to the site name
  # get the current site name
  prost_printmsg "getting current site name";
  prost_drupal_vget "drupal" "site_name";

  # check if we got the site name
  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not get current site name" "warning";
  else
    sitename="$PROST_RETURN_VARIABLE";

    # remove leading test system marker
    sitename="$(echo "$sitename" | sed -e "s/^.*+++[ \t]*//g")";

    # get the users name
    prost_printmsg "getting the users name";
    prost_git_vget "." "user.name";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not get user name" "warning";
    else
      username="$PROST_RETURN_VARIABLE";
      newsitename="+++ Testsystem - $username +++ $sitename";

      prost_printmsg "setting site name to $newsitename";
      prost_drupal_vset "drupal" "site_name" "$newsitename";

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not set new site name" "warning";
      fi;
    fi;
  fi;

  # call drush cc all
  prost_printmsg "clearing database caches";
  prost_drupal_drushcmd "drupal" cc all;
fi;
