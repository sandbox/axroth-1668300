<?php

// VERSION 7
//
// in this file we set installation specific settings that won't make it's way into the git repisotiry
// for example test settings, the local database connection etc.
// this fetches the project specific settings
// PHP 5.4 has E_STRICT included into E_ALL which means that if drupal is not coded for strict...
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

if (file_exists('../drupal_sftp/local.settings.php')) {
  include('../drupal_sftp/local.settings.php');
}

// here the project specific settings are being overwritten by installation specific settings
$databases = array(
  'default' =>
  array(
    'default' =>
    array(
      'database' => '[PROJECTNAME]',
      'username' => '[MYSQLUSER]',
      'password' => '[MYSQLPWD]',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => false,
    ),
  ),
);

//$base_url = '/[PROJECTNAME]';  // NO trailing slash!
// enables testing d7 site over vpn
if (isset($_SERVER['HTTP_X_FORWARDED_SERVER']) && $_SERVER['HTTP_X_FORWARDED_SERVER'] != '') {
  $base_url = 'http://' . $_SERVER['HTTP_X_FORWARDED_SERVER'] . '[BASEURLNODOMAIN]';
}
else {
  $base_path = '[BASEURL]';
  $base_url = $base_path;
}

$update_free_access = TRUE;

$drupal_hash_salt = '[DRUPAL_HASH_SALT]';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

// initialize if not set by local.settings.php
if (!is_array($conf)) {
  $conf = array();
}

$conf['file_public_path'] = 'sites/default/files';
$conf['file_temporary_path'] = 'sites/default/files/tmp';
