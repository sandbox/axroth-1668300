<?php

/**
 * @file
 * Drush plugin for sending a ticket to redmine
 * 
 */

/**
 * Implements hook_drush_command().
 */
function feedback_drush_command() {
  $items = array();

  $items['feedback'] = array(
    'description' => 'Open a ticket in redmine',
    'examples' => array(
      'drush feedback "text and more"' => 'Open a redmine ticket with message text',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('red'),
    'arguments' => array(
      'message' => 'Message text to be send',
    ),
  );

  return $items;
}

function drush_feedback($message = '') {

  // todo: allow options for ticket type, priority, sender ...
  if (module_exists('feedback')) {
    if (!empty($message)) {
      _redmine_add_ticket($message, 'Error-Report', 'Normal', '');
      drush_print(dt('Ticket was created'));
    }
    else {
      return drush_set_error('feedback', dt('No message text provided'));
    }
  }
  else {
    return drush_set_error('feedback', dt('Missing feedback-module'));
  }
}
