-- phpMyAdmin SQL Dump
-- version 3.3.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2012 at 04:03 AM
-- Server version: 5.1.62
-- PHP Version: 5.3.2-1ubuntu4.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Table structure for table `backup_migrate_profiles`
--

CREATE TABLE IF NOT EXISTS `backup_migrate_profiles` (
  `profile_id` varchar(32) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `append_timestamp` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `timestamp_format` varchar(14) NOT NULL,
  `filters` text NOT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `backup_migrate_profiles`
--

REPLACE INTO `backup_migrate_profiles` (`profile_id`, `name`, `filename`, `append_timestamp`, `timestamp_format`, `filters`) VALUES
('default', 'Standardeinstellungen', '[site-name]', 1, 'Y-m-d\\TH-i-s', 'a:10:{s:11:"compression";s:4:"gzip";s:21:"notify_success_enable";i:0;s:20:"notify_success_email";s:14:"roth@prost.de";s:21:"notify_failure_enable";i:0;s:20:"notify_failure_email";s:14:"roth@prost.de";s:18:"utils_site_offline";i:0;s:26:"utils_site_offline_message";s:160:"<p>Sehr geehrter Besucher,&nbsp;</p>\r\n<p>momentan werden Wartungsarbeiten auf dieser Webseite durchgef&uuml;hrt.<br />\r\n	Vielen Dank f&uuml;r Ihre Geduld.</p>\r\n";s:14:"exclude_tables";a:0:{}s:13:"nodata_tables";a:15:{s:5:"cache";s:5:"cache";s:11:"cache_block";s:11:"cache_block";s:12:"cache_filter";s:12:"cache_filter";s:10:"cache_form";s:10:"cache_form";s:10:"cache_menu";s:10:"cache_menu";s:10:"cache_page";s:10:"cache_page";s:12:"cache_update";s:12:"cache_update";s:11:"cache_views";s:11:"cache_views";s:13:"devel_queries";s:13:"devel_queries";s:11:"devel_times";s:11:"devel_times";s:14:"search_dataset";s:14:"search_dataset";s:12:"search_index";s:12:"search_index";s:12:"search_total";s:12:"search_total";s:8:"sessions";s:8:"sessions";s:8:"watchdog";s:8:"watchdog";}s:17:"utils_lock_tables";i:0;}');
