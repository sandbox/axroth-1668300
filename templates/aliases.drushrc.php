<?php

$sshUser = "";

if (file_exists(getcwd() . "/sites/all/drush/sshuser.php")) {
  include(getcwd() . "/sites/all/drush/sshuser.php");
}

$aliases['live'] = array(
  'root' => '/vol1/www/clients/[LIVECLIENT]/[LIVESITE]/docroot',
  'uri' => '[LIVESITE].[LIVEHOST]',
  'remote-host' => '[LIVEHOST]',
  'remote-user' => $sshUser,
  'command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
    ),
  ),
);

// these settings come from prost-install.sh
// always use them
$aliases['local'] = array(
  'root' => '[PROJECTPATH]',
  'uri' => 'localhost/[PROJECTNAME]',
);
