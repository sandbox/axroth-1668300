<?php

/**
 * Include settings.php from private folder
 * This is for the live server
 */
if (file_exists(getcwd() . '/../../private/settings.php')) {
  include(getcwd() . '/../../private/settings.php');
}

/**
 * Include installation specific settings.php from drupal_sftp folder
 */
elseif (file_exists(getcwd() . '/../drupal_sftp/settings.php')) {
  include(getcwd() . '/../drupal_sftp/settings.php');
}
/**
 * Include project specific settings.php from drupal_sftp folder
 */
elseif (file_exists(getcwd() . '/../drupal_sftp/local.settings.php')) {
  include(getcwd() . '/../drupal_sftp/local.settings.php');
}
