<?php

function mychmod($path, $dirmode, $filemode, $recursive = false) {
  global $errors;

  if (is_dir($path)) {
    $cmode = $dirmode;
  }
  else {
    $cmode = $filemode;
  }

  print 'setting permission for "' . $path . '"<br>' . "\n";
  $errors += chmod($path, $cmode) ? 0 : 1;

  if ($recursive && is_dir($path)) {
    // open this directory
    $dirhandle = opendir($path);

    // get each entry
    while ($entryName = readdir($dirhandle)) {
      if ($entryName != "." && $entryName != "..") {
        mychmod($path . "/" . $entryName, $dirmode, $filemode, $recursive);
      }
    }

    // close directory
    closedir($dirhandle);
  }
}

$errors = 0;

$rootdir = '';

if (isset($_SERVER['DOCUMENT_ROOT']) && isset($_SERVER['SCRIPT_FILENAME'])) {
  $docroot = $_SERVER['DOCUMENT_ROOT'];
  $docrootlen = strlen($_SERVER['DOCUMENT_ROOT']);
  $scriptfile = $_SERVER['SCRIPT_FILENAME'];

  if (substr($scriptfile, 0, $docrootlen + 1) == $docroot . '/') {
    $subdir = substr($scriptfile, $docrootlen + 1);

    if (substr_count($subdir, '/') > 0) {
      $subdir = preg_replace('/\/.*$/', '', $subdir);
      $rootdir = $docroot . '/' . $subdir;
    }
  }
}

if ($rootdir != '') {
  mychmod($rootdir, 0775, 0664, true);
}
else {
  print "DocumentRoot not found, aborting\n";
}

if ($errors > 0) {
  print "<br>\n";
  print $errors . " entries could not be set\n";
}
