-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 11. Jun 2014 um 16:09
-- Server Version: 5.5.37-0ubuntu0.14.04.1
-- PHP-Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `backup_migrate_destinations`
--

CREATE TABLE IF NOT EXISTS `backup_migrate_destinations` (
  `destination_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary ID field for the table. Not used for anything except internal lookups.',
  `machine_name` varchar(255) NOT NULL DEFAULT '0' COMMENT 'The primary identifier for a destination.',
  `name` varchar(255) NOT NULL COMMENT 'The name of the destination.',
  `subtype` varchar(32) NOT NULL COMMENT 'The type of the destination.',
  `location` text NOT NULL COMMENT 'The the location string of the destination.',
  `settings` text NOT NULL COMMENT 'Other settings for the destination.',
  PRIMARY KEY (`destination_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `backup_migrate_destinations`
--

REPLACE INTO `backup_migrate_destinations` (`destination_id`, `machine_name`, `name`, `subtype`, `location`, `settings`) VALUES
(1, 'manual', 'Zielverzeichnis für manuelle Sicherungen', 'file_manual', '../dumps', 'a:2:{s:5:"chmod";s:4:"0644";s:5:"chgrp";s:0:"";}'),
(2, 'live-manual', 'Zielverzeichnis auf dem Live Server für manuelle Sicherungen', 'file_manual', '../../private/backup_migrate/manual', 'a:2:{s:5:"chmod";s:4:"0644";s:5:"chgrp";s:0:"";}');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
