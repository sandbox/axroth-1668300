<?php

// VERSION 6
//
// in this file we set installation specific settings that won't make it's way into the git repisotiry
// for example test settings, the local database connection etc.
// this fetches the project specific settings
// PHP 5.4 has E_STRICT included into E_ALL which means that if drupal is not coded for strict...
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

if (file_exists('../drupal_sftp/local.settings.php')) {
  include('../drupal_sftp/local.settings.php');
}

// here the project specific settings are being overwritten by installation specific settings
$db_url = 'mysqli://[MYSQLUSER]:[MYSQLPWD]@localhost/[PROJECTNAME]';
//$base_url = '/[PROJECTNAME]';  // NO trailing slash!
$base_url = '[BASEURL]';  // NO trailing slash!
$update_free_access = TRUE;

ini_set('arg_separator.output', '&amp;');
ini_set('magic_quotes_runtime', 0);
ini_set('magic_quotes_sybase', 0);
ini_set('session.cache_expire', 200000);
ini_set('session.cache_limiter', 'none');
ini_set('session.cookie_lifetime', 0);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.save_handler', 'user');
ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
ini_set('session.use_trans_sid', 0);
ini_set('url_rewriter.tags', '');
ini_set('display_errors', 1);

// initialize if not set by local.settings.php
if (!is_array($conf)) {
  $conf = array();
}

$conf['file_directory_path'] = 'sites/default/files';
$conf['file_directory_temp'] = 'sites/default/files/tmp';
