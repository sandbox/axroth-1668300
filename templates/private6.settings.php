<?php

// this file is indended for the live server
// here the project specific settings are being overwritten by installation specific settings
$db_url = 'mysqli://[MYSQLUSER]:[MYSQLPWD]@localhost/[PROJECTNAME]';
$base_url = 'http://[PROJECTONLINEURL]';  // NO trailing slash!
$update_free_access = FALSE;

ini_set('arg_separator.output', '&amp;');
ini_set('magic_quotes_runtime', 0);
ini_set('magic_quotes_sybase', 0);
ini_set('session.cache_expire', 200000);
ini_set('session.cache_limiter', 'none');
ini_set('session.cookie_lifetime', 2000000);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.save_handler', 'user');
ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
ini_set('session.use_trans_sid', 0);
ini_set('url_rewriter.tags', '');

// initialize if not set by local.settings.php
if (!is_array($conf)) {
  $conf = array();
}

$conf['file_directory_path'] = 'sites/default/files';
$conf['file_directory_temp'] = 'sites/default/files/tmp';
