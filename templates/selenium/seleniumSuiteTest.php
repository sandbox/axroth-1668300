<?php

class SeleneseTest extends PHPUnit_Extensions_SeleniumTestCase {

  protected function setUp() {
    //$this->setHost("localhost");
    //$this->setPort(4444);

    $this->setBrowser("*chrome");
    $this->setBrowserUrl("file://");
    $this->shareSession(true);
  }

  public function provider() {
    $seleneses = array();

    $recursive = false;
    if (getenv('PROST_INIT_RECURSIVE') == 1) {
      // this is called from the prost scripts with recursion
      $recursive = true;
    }
    if (getenv('NB_EXEC_EXTEXECUTION_PROCESS_UUID') != "") {
      // this is called from within netbeans, use recursion
      $recursive = true;
    }

    // find all suites in this directory with or withour recursion
    $suiteFiles = $this->_find_all_suites(getcwd(), $recursive);

    foreach ($suiteFiles as $suiteEntry) {
      $suiteDir = $suiteEntry['dir'];
      $suiteFile = $suiteEntry['file'];

      if (file_exists($suiteDir . "/" . $suiteFile)) {
        // files that will be executed
        $testFiles = array();

        // load and parse the suite file and prepare the xpath
        $suitePage = new DOMDocument();
        @$suitePage->loadHTMLFile($suiteDir . "/" . $suiteFile);
        $suiteXpath = new DOMXPath($suitePage);

        // find all links in the table 'suiteTable'
        $suiteElements = $suiteXpath->query("//table[@id='suiteTable']//a");

        // fill the suite files with the href's of the links we found
        foreach ($suiteElements as $suiteElement) {
          $testFiles[] = $suiteDir . "/" . $suiteElement->getAttribute("href");
        }

        // walk over the suite files
        foreach ($testFiles as $testFile) {
          if (file_exists($testFile)) {
            // TODO: maybe signalise which files we are currently running
            // TODO: not via print or echo
            // base url for this test
            $testTitle = "";
            $testBrowserUrl = "";

            // load and parse the test file and prepare the xpath
            $testPage = new DOMDocument();
            @$testPage->loadHTMLFile($testFile);
            $testXpath = new DOMXPath($testPage);

            // find all link elements containing the title of this test
            $testElements = $testXpath->query("//title");

            // get the first element we found
            foreach ($testElements as $testElement) {
              $testTitle = $testElement->nodeValue;
              break;
            }

            // find all link elements containing the base url of this test
            $testElements = $testXpath->query("//link[@rel='selenium.base']");

            // get the first element we found
            foreach ($testElements as $testElement) {
              $testBrowserUrl = $testElement->getAttribute("href");
              break;
            }

            // appent a trailing slash at the end
            if ($testBrowserUrl != "" && substr($testBrowserUrl, -1) != "/") {
              $testBrowserUrl .= "/";
            }

            // find all elements that do an open command
            // these must be modified to fit the test's base url
            $testElements = $testXpath->query("//td[.='open']");

            foreach ($testElements as $testElement) {
              // the actual node is a sibling, get the parent node first
              $testParentNode = $testElement->parentNode;

              // and then walk through all childs
              foreach ($testParentNode->childNodes as $testChildNode) {
                // get only the td elements
                if (get_class($testChildNode) == "DOMElement" && $testChildNode->tagName == "td") {
                  // ignore the command itself
                  if ($testChildNode->nodeValue != "open") {
                    // set the base for this open command
                    $testBase = $testBrowserUrl;
                    // this is the element we want to check
                    $testLink = $testChildNode->nodeValue;

                    // check if this link already has a base
                    if (!preg_match("/^[a-z]{1,}:\/\//i", $testLink)) {
                      // remove ./ and use ../ to update our open command base
                      while (substr($testLink, 0, 3) == "../" || $testLink == ".." || substr($testLink, 0, 2) == "./" || $testLink == ".") {
                        if (substr($testLink, 0, 2) == "..") {
                          // this link (still) wants to go one directory up
                          $testLink = substr($testLink, 3);
                          $testBase = preg_replace("/(^[a-z]{1,}:\/\/[^\/]*)\/[^\/]{1,}\/{0,1}$/i", "$1/", $testBase);
                        }
                        elseif (substr($testLink, 0, 1) == ".") {
                          // this link (still) uses ./ (or .) to direct to the current directory
                          $testLink = substr($testLink, 2);
                        }
                      }

                      if (substr($testLink, 0, 1) == "/") {
                        // this is an absolute url, use only the base of the base url for this command
                        $testBase = preg_replace("/(^[a-z]{1,}:\/\/[^\/]*).*$/i", "$1", $testBase);
                        $testLink = $testBase . $testLink;
                      }
                      else {
                        // this is a relative url, just append to the base url for this command
                        $testLink = $testBase . $testLink;
                      }
                    }

                    // now set the new value
                    $testChildNode->nodeValue = $testLink;

                    // we only need the link element which is the first after the command element
                    break;
                  }
                }
              }
            }

            // save the new dom into a temporary file
            $testNewDir = sys_get_temp_dir();
            $testNewFile = tempnam($testNewDir, 'seleniumtest_');
            $testPage->saveHTMLFile($testNewFile);

            $seleneseKey = "#" . count($seleneses) . " " . $testTitle;
            $seleneses[$seleneseKey] = array($testNewDir, $testNewFile);
          }
        }
      }
    }

    return $seleneses;
  }

  /**
   * @dataProvider provider
   */
  public function test_cycle($seleneseDir, $seleneseFile) {
    $this->runSelenese($seleneseFile);

    // remove all evidence, but only if this is a temporary file
    if ($seleneseDir == sys_get_temp_dir()) {
      unlink($seleneseFile);
    }
  }

  function _find_all_suites($dir, $recursive = false) {
    $result = array();

    // get all entries of the current directory
    $root = scandir($dir);

    foreach ($root as $value) {
      // ignore . and ..
      if ($value == '.' || $value == '..') {
        continue;
      }

      if (is_file($dir . "/" . $value)) {
        // this is a file?
        // only register suite.html or suite__%.html
        if ($value == "suite.html" || preg_match("/^suite__.*\.html$/", $value)) {
          $result[] = array("dir" => $dir, "file" => $value);
        }
      }
      elseif (is_dir($dir . "/" . $value)) {
        // this is a directory
        // only use it on recursive search
        if ($recursive) {
          // is this a directory?
          // get the results of this subdirectory and merge
          $subresult = $this->_find_all_suites($dir . "/" . $value, $recursive);

          $result = array_merge($result, $subresult);
        }
      }
    }

    return $result;
  }

}
