; ----------------
; Generated makefile from http://drushmake.me
; Permanent URL: http://drushmake.me/file.php?token=7549cb9f189e
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.
  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 6.x
  
; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.
  
api = 2
  
; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.
  
; Drupal 6.x core:
projects[drupal][version] = 6

  
  
; Modules
; --------
projects[adminrole][version] = 1.3
projects[adminrole][type] = "module"
projects[adminrole][subdir] = "contrib"
projects[admin_menu][version] = 1.8
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"
projects[devel][version] = 1.27
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"
projects[features][version] = 1.2
projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[imageapi][version] = 1.10
projects[imageapi][type] = "module"
projects[imageapi][subdir] = "contrib"
projects[imagecache][version] = 2.0-rc1
projects[imagecache][type] = "module"
projects[imagecache][subdir] = "contrib"
projects[backup_migrate][version] = 2.6
projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "contrib"
projects[pathauto][version] = 1.6
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"
projects[panels][version] = 3.10
projects[panels][type] = "module"
projects[panels][subdir] = "contrib"
projects[jquery_ui][version] = 1.5
projects[jquery_ui][type] = "module"
projects[jquery_ui][subdir] = "contrib"
projects[jquery_update][version] = 2.0-alpha1
projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"
projects[vertical_tabs][version] = 1.0-rc2
projects[vertical_tabs][type] = "module"
projects[vertical_tabs][subdir] = "contrib"
projects[views][version] = 2.16
projects[views][type] = "module"
projects[views][subdir] = "contrib"

  

; Themes
; --------
projects[zen][version] = 2.1
projects[zen][type] = "theme"

  
  
; Libraries
; ---------
; No libraries were included


