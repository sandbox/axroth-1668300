#!/bin/bash

(
  echo "<?php";
  tail -n1 backup_migrate_profiles_6.sql | sed -e "s/^.*, '\(a:\)/\1/g" -e "s/');$//g" -e "s/s:[0-9][0-9]*:\"[^\"]*Sehr geehrter Besucher[^\"]*\"/s:0:\"\"/g" -e "s/^/  \$input='/g" -e "s/$/';/g";
  echo "  \$output=unserialize(\$input);";
  echo "  echo implode(\"\n\",\$output['nodata_tables']);";
  echo "  echo \"\n\";";
  echo "?>";
) | php | sort;
