-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 11. Jun 2014 um 16:10
-- Server Version: 5.5.37-0ubuntu0.14.04.1
-- PHP-Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `backup_migrate_profiles`
--

CREATE TABLE IF NOT EXISTS `backup_migrate_profiles` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary ID field for the table. Not used for anything except internal lookups.',
  `machine_name` varchar(255) NOT NULL DEFAULT '0' COMMENT 'The primary identifier for a profile.',
  `name` varchar(255) NOT NULL COMMENT 'The name of the profile.',
  `filename` varchar(255) NOT NULL COMMENT 'The name of the profile.',
  `append_timestamp` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Append a timestamp to the filename.',
  `timestamp_format` varchar(14) NOT NULL COMMENT 'The format of the timestamp.',
  `filters` text NOT NULL COMMENT 'The filter settings for the profile.',
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `backup_migrate_profiles`
--

REPLACE INTO `backup_migrate_profiles` (`profile_id`, `machine_name`, `name`, `filename`, `append_timestamp`, `timestamp_format`, `filters`) VALUES
(1, '0', 'Standardeinstellungen', '[site:name]', 1, 'Y-m-d\\TH-i-s', 'a:9:{s:11:"compression";s:4:"gzip";s:21:"notify_success_enable";i:0;s:20:"notify_success_email";s:14:"roth@arocom.de";s:21:"notify_failure_enable";i:0;s:20:"notify_failure_email";s:14:"roth@arocom.de";s:18:"utils_site_offline";i:0;s:26:"utils_site_offline_message";s:99:"arocom GmbH is currently under maintenance. We should be back shortly. Thank you for your patience.";s:17:"utils_description";s:0:"";s:12:"destinations";a:1:{s:2:"db";a:4:{s:14:"exclude_tables";a:0:{}s:13:"nodata_tables";a:23:{s:5:"cache";s:5:"cache";s:16:"cache_admin_menu";s:16:"cache_admin_menu";s:11:"cache_block";s:11:"cache_block";s:15:"cache_bootstrap";s:15:"cache_bootstrap";s:11:"cache_field";s:11:"cache_field";s:12:"cache_filter";s:12:"cache_filter";s:10:"cache_form";s:10:"cache_form";s:11:"cache_image";s:11:"cache_image";s:10:"cache_menu";s:10:"cache_menu";s:10:"cache_page";s:10:"cache_page";s:12:"cache_panels";s:12:"cache_panels";s:10:"cache_path";s:10:"cache_path";s:11:"cache_token";s:11:"cache_token";s:14:"cache_variable";s:14:"cache_variable";s:11:"cache_views";s:11:"cache_views";s:16:"cache_views_data";s:16:"cache_views_data";s:16:"ctools_css_cache";s:16:"ctools_css_cache";s:19:"ctools_object_cache";s:19:"ctools_object_cache";s:14:"search_dataset";s:14:"search_dataset";s:12:"search_index";s:12:"search_index";s:12:"search_total";s:12:"search_total";s:8:"sessions";s:8:"sessions";s:8:"watchdog";s:8:"watchdog";}s:17:"utils_lock_tables";i:1;s:13:"use_mysqldump";i:0;}}}');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
