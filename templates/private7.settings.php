<?php

// this file is indended for the live server
// here the project specific settings are being overwritten by installation specific settings
$databases = array(
  'default' =>
  array(
    'default' =>
    array(
      'database' => '[PROJECTNAME]',
      'username' => '[MYSQLUSER]',
      'password' => '[MYSQLPWD]',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => false,
    ),
  ),
);

$base_url = 'http://[PROJECTONLINEURL]';  // NO trailing slash!
$update_free_access = FALSE;

$drupal_hash_salt = '[DRUPAL_HASH_SALT]';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);
