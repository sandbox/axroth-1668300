#!/bin/bash

# prost project initialization script
# v0.1 ezi

# include the basic functions
PROST_SCRIPT_PATH="$(dirname "$0")";
. "$PROST_SCRIPT_PATH/include/_prost-base.sh";

# run init to get the required information
prost_init "$@";

if [ $PROST_INIT_NODEPENDENCIES -eq 0 ]; then
  # run some dependency checks
  prost_printmsg "running dependency checks";
  prost_script_checkdependencies -quiet;

  if [ "$?" -gt 0 ]; then
    prost_printmsg "at least one dependency check failed, aborting" "error";
    exit 1;
  fi;
fi;

# get the current project name
prost_fs_getcurrentproject;

# check if project could be found
if [ "$?" -gt 0 ]; then
  prost_printmsg "current project could not be determined, aborting" "error";
  exit 1;
fi;

PROST_PROJECT_PATH="$PROST_RETURN_PROJECTPATH";
PROST_PROJECT_NAME="$PROST_RETURN_PROJECTNAME";

prost_printmsg "Current project path: $PROST_PROJECT_PATH";
prost_printmsg "Current project name: $PROST_PROJECT_NAME";

# change to the project path to make sure subpaths are right
prost_printmsg "changing into main project directory";
cd "$PROST_PROJECT_PATH";

# check if cd was successfull
if [ "$?" -gt 0 ]; then
  prost_printmsg "could not change into main project directory, aborting" "error";
  exit 1;
fi;

# fix file permissions
prost_printmsg "fixing filesystem group for project directory";
if [ $PROST_INIT_SUDO -eq 0 ]; then
  prost_fs_chown -check -ignore -userrec "www-data" ".";
else
  prost_fs_chown -check -ignore -userrec "www-data" -sudo ".";
fi;

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not fix filesystem group" "error";
  exit 1;
fi;

# fix file permissions
prost_printmsg "fixing filesystem permissions";
if [ $PROST_INIT_SUDO -eq 0 ]; then
  prost_fs_chmod -check -ignore -dirmoderec 775 -filemoderec 664 "."
else
  prost_fs_chmod -check -ignore -dirmoderec 775 -filemoderec 664 -sudo "."
fi;

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not fix filesystem permissions" "error";
  exit 1;
fi;
