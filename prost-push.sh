#!/bin/bash

# prost project initialization script
# v0.1 ezi

# TODO: add check that this script's source branch is master

# include the basic functions
PROST_SCRIPT_PATH="$(dirname "$0")";
. "$PROST_SCRIPT_PATH/include/_prost-base.sh";

# run init to get the required information
prost_init "$@";

if [ $PROST_INIT_NODEPENDENCIES -eq 0 ]; then
  # run some dependency checks
  prost_printmsg "running dependency checks";
  prost_script_checkdependencies -quiet;

  if [ "$?" -gt 0 ]; then
    prost_printmsg "at least one dependency check failed, aborting" "error";
    exit 1;
  fi;
fi;

# get the current project name
prost_fs_getcurrentproject;

# check if project could be found
if [ "$?" -gt 0 ]; then
  prost_printmsg "current project could not be determined, aborting" "error";
  exit 1;
fi;

PROST_PROJECT_PATH="$PROST_RETURN_PROJECTPATH";
PROST_PROJECT_NAME="$PROST_RETURN_PROJECTNAME";

prost_printmsg "Current project path: $PROST_PROJECT_PATH";
prost_printmsg "Current project name: $PROST_PROJECT_NAME";

# change to the project path to make sure subpaths are right
prost_printmsg "changing into main project directory";
cd "$PROST_PROJECT_PATH";

# check if cd was successfull
if [ "$?" -gt 0 ]; then
  prost_printmsg "could not change into main project directory, aborting" "error";
  exit 1;
fi;

# check git branch
prost_printmsg "checking git branch";
prost_git_currentbranch;

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not determine current git branch, aborting" "error";
  exit 1;
fi;

PROST_GIT_BRANCH="$PROST_RETURN_GITBRANCH";

if [ "$PROST_GIT_BRANCH" == "" ]; then
  prost_printmsg "could not determine current git branch, aborting" "error";
  exit 1;
fi;

# only dump if said to do
if [ "$PROST_INIT_DUMPDB" -eq 1 ]; then
  hasdrupal=1;

  # check for drupal
  prost_printmsg "checking for working drupal";

  # TODO: do some better check
  if test -f "drupal/index.php"; then
    # import the default dump settings
    prost_printmsg "setting default database dump settings";

    # get the currently set value, if any
    installversion=6;

    if test -f "drupal_sftp/settings.php"; then
      if grep "\/\/ VERSION 7" "drupal_sftp/settings.php" >/dev/null; then
        installversion=7;
      fi;
    fi;

    #prost_drupal_drushcmd "drupal" sql-query --file="$PROST_SCRIPT_PATH/templates/backup_migrate_destinations.sql";

    # check if import was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not import default database dump destination, aborting" "error";
      exit 1;
    fi;

    #prost_drupal_drushcmd "drupal" sql-query --file="$PROST_SCRIPT_PATH/templates/backup_migrate_profiles_$installversion.sql";

    # check if import was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not import default database dump profile, aborting" "error";
      exit 1;
    fi;

    # do a drush db backup
    prost_printmsg "starting database backup";
    prost_drupal_drushcmd "drupal" bam-backup;

    if [ "$?" -gt 0 ]; then
      prost_printmsg "backup could not be created, aborting" "error";
      exit 1;
    fi;
  else
    # do the dump the manual way
    prost_printmsg "dumping database tables via mysqldump";
    prost_db_dumptables "$PROST_PROJECT_NAME" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD" -nodatafile "$PROST_SCRIPT_PATH/templates/dump_nodata";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "database dump could not be created, aborting" "error";
      exit 1;
    fi;
  fi;

  prost_printmsg "getting current tag number";
  prost_fs_getnewtagnumber "dumps" "tag-";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not get current tag, aborting" "error";
    exit 1;
  fi;

  newtagnum="$PROST_RETURN_NEWTAGNUM";

  prost_printmsg "appending new tag number to dumps";
  prost_git_appendtagtodumps "dumps" "$newtagnum";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "at least one dump file could not be renamed, aborting" "error";
    exit 1;
  fi;

  # add the files to git
  prost_printmsg "adding the dump file(s) to git";
  prost_git_add "dumps";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "dump file(s) could not be added, aborting" "error";
    exit 1;
  fi;

  # commit the files to git
  prost_printmsg "committing to git";
  prost_git_commit "added sql-dump(s) before pushing" "dumps";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "dump file(s) could not be committed, aborting" "error";
    exit 1;
  fi;

  prost_printmsg "adding new tag $newtagnum";
  prost_git_addtag "dbtag-$newtagnum";

  if [ "$?" -gt 0 ]; then
    prost_printmsg "could not add new tag number, aborting" "error";
    exit 1;
  fi;
fi;

# pushing to origin
prost_printmsg "pushing to development server";
prost_git_push "$PROST_INIT_ORIGIN" "$PROST_GIT_BRANCH";

if [ "$?" -gt 0 ]; then
  prost_printmsg "push was not successfull, aborting" "error";
  exit 1;
fi;
