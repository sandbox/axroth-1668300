#!/bin/bash

# prost project initialization script
# v0.1 aro
# v0.2 ezi

# include the basic functions
PROST_SCRIPT_PATH="$(dirname "$0")";
. "$PROST_SCRIPT_PATH/include/_prost-base.sh";

# run init to get the required information
prost_init "$@";

prost_printmsg "Project directory: $PROST_INIT_DIRECTORY";
prost_printmsg "Makefile: $PROST_INIT_MAKEFILE";

prost_separator;

# check if the directory already exists
if test -d "$PROST_INIT_DIRECTORY"; then
  prost_printmsg "project directory already exists, aborting" "error";
  exit 1;
fi;

prost_printmsg "installing empty Drupal project";
install_drupal -projectname "$PROST_INIT_DIRECTORY" -makefile "$PROST_INIT_MAKEFILE" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not install empty project, aborting" "error";
  exit 1;
fi;

prost_printmsg "remove existing files directory";

# setting files directory writable
if [ $PROST_INIT_SUDO -eq 0 ]; then
  prost_fs_chmod -check -ignore -moderec "u+w" "$PROST_INIT_DIRECTORY/sites/default"
else
  prost_fs_chmod -check -ignore -moderec "u+w" -sudo "$PROST_INIT_DIRECTORY/sites/default"
fi;

if [ "$?" -gt 0 ]; then
  prost_printmsg "could set files directory writable" "warning";
fi;

# files will be added later as symbolic link
prost_fs_rmdirs -check -ignore -rec "$PROST_INIT_DIRECTORY/sites/default/files";

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not remove files directory, aborting" "error";
  exit 1;
fi;

# initialize git
prost_printmsg "initializing git";
prost_git_init "$PROST_INIT_DIRECTORY";

# check if git was successfully initialized
if [ "$?" -gt 0 ]; then
  prost_printmsg "initializing git failed, aborting" "error";
  exit 1;
fi;

# make sure we are in the develop branch
prost_printmsg "switching to develop branch, create it if missing";
prost_git_checkout "develop" -createonmissing -directory "$PROST_INIT_DIRECTORY";

# check if there was an error switching/creating the branch
if [ "$?" -gt 0 ]; then
  prost_printmsg "could not switch/create the branch, aborting" "error";
  exit 1;
fi;

# enabling some modules
prost_printmsg "enabling some modules";
prost_drupal_drushcmd "$PROST_INIT_DIRECTORY" en devel admin_menu pathauto views views_bulk_operations diff hacked features strongarm admin_menu_toolbar libraries aurora magic ds ds_extras rabbit_hole admin_views fpa views_ui ds_ui rh_node transliteration globalredirect jquery_update;

# check if en backup_migrate failed
if [ "$?" -gt 0 ]; then
  prost_printmsg "at least one module could not been enabled" "warning";
fi;

# enabling some modules
prost_printmsg "disabling some modules";
prost_drupal_drushcmd "$PROST_INIT_DIRECTORY" dis toolbar rdf overlay color comment help search shortcut dashboard;

# check if en backup_migrate failed
if [ "$?" -gt 0 ]; then
  prost_printmsg "at least one module could not been disabled" "warning";
fi;

# enabling some modules
prost_printmsg "getting drush status";
prost_drupal_drushcmd "$PROST_INIT_DIRECTORY" status;

# check if en backup_migrate failed
if [ "$?" -gt 0 ]; then
  prost_printmsg "could not get drush status, aborting" "error";
  exit 1;
fi;

version="6";

basemakefile="$(basename "$PROST_INIT_MAKEFILE")";
if echo "$basemakefile" | grep "^d7\." >/dev/null; then
  version="7";
fi;

if [ "$PROST_INIT_VERSION" != "" ]; then
  version="$PROST_INIT_VERSION";
fi;

"$PROST_SCRIPT_PATH/prost-install.sh" -noupdate -nobranchcheck -projectname "$PROST_INIT_DIRECTORY" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD" -baseurl "localhost/$PROST_INIT_DIRECTORY" -version "$version" -nodeployment -xstage checks -xstage clone -xstage database -xstage import;
