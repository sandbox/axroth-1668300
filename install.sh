#!/bin/bash

# prost project initialization script
# v0.1 ezi

# include the basic functions
PROST_SCRIPT_PATH="$(dirname "$0")";
. "$PROST_SCRIPT_PATH/include/_prost-base.sh";

# this script's dependencies
prost_adddependency "grep";
prost_adddependency "id";
prost_adddependency "basename";
prost_adddependency "pwd";

# run init to get the required information
prost_init "$@";

if [ $PROST_INIT_NODEPENDENCIES -eq 0 ]; then
  # run some dependency checks
  prost_printmsg "running dependency checks";
  prost_script_checkdependencies;

  if [ "$?" -gt 0 ]; then
    prost_printmsg "at least one dependency check failed, aborting" "error";
    exit 1;
  fi;
fi;

# if this is a dry run (only do checks) exit here
if [ "$PROST_INIT_DRYRUN" -eq 1 ]; then
  exit 0;
fi;

# do the checks

# find out who we are
uid="$(id -u)";

# we should not be run as root
if [ "$uid" == "0" ]; then
  prost_printmsg "this script should not be run as user root, aborting" "error";
  exit 1;
fi;

# find out what shell we are using, currently bash and zsh are supported
PROST_INIT_SHELLNAME="$(basename "$SHELL")";

if [ "$PROST_INIT_SHELLNAME" != "bash" ]; then
  if [ "$PROST_INIT_SHELLNAME" != "zsh" ]; then
    prost_printmsg "your shell \"$PROST_INIT_SHELLNAME\" is not supported, aborting" "error";
    exit 1;
  fi;
fi;

# check if we can determine the user's home directory
if [ "$HOME" == "" ]; then
  prost_printmsg "could not detect your home directory, aborting" "error";
  exit 1;
fi;

prost_subseparator;

# now do the installation things

prost_printmsg "creating target directory";
prost_fs_mkdirs -check "$PROST_INIT_DIRECTORY";

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not create target directory, aborting" "error";
  exit 1;
fi;

prost_printmsg "copying files into target directory";
prost_fs_copy "$PROST_SCRIPT_PATH" "$PROST_INIT_DIRECTORY";

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not copy files into target directory, aborting" "error";
  exit 1;
fi;

prost_printmsg "creating temp directory";
prost_fs_mkdirs -check -ignore "$PROST_INIT_DIRECTORY/tmp";

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not create temp directory, aborting" "error";
  exit 1;
fi;

prost_printmsg "creating log directory";
prost_fs_mkdirs -check -ignore "$PROST_INIT_DIRECTORY/log";

if [ "$?" -gt 0 ]; then
  prost_printmsg "could not create log directory, aborting" "error";
  exit 1;
fi;

# check if target directory is absolute, if not prepend current path
targetdir="$PROST_INIT_DIRECTORY";

if ! echo "$PROST_INIT_DIRECTORY" | grep "^\/" >/dev/null; then
  targetdir="$(pwd)/$targetdir";
fi;

# check if we can replace the home directory by the home variable
quotedhome="$(echo "$HOME" | sed -e "s/\//\\\\\//g")";

if [ "$quotedhome" != "" ]; then
  targetdir="$(echo "$targetdir" | sed -e "s/^$quotedhome/\$HOME/g")";
  quotedtargetdir="$(echo "$targetdir" | sed -e "s/\\$/\\\\$/g" -e "s/\//\\\\\//g")";

  # only alter the path variable if not already present
  setpath=0;
  setmanpath=0;

  if [ "$PROST_INIT_SHELLNAME" == "bash" ]; then
    if ! grep -P "^[ \t]*export[ \t][ \t]*PATH=.*$quotedtargetdir[:; \t]|^[ \t]*PATH=.*$quotedtargetdir[:; \t]" "$HOME/.bashrc" >/dev/null; then
      setpath=1;
    fi;

    if ! grep -P "^[ \t]*export[ \t][ \t]*MANPATH=.*$quotedtargetdir\/man[:; \t]|^[ \t]*MANPATH=.*$quotedtargetdir\/man[:; \t]" "$HOME/.bashrc" >/dev/null; then
      setmanpath=1;
    fi;
  elif [ "$PROST_INIT_SHELLNAME" == "zsh" ]; then
    if ! grep -P "^[ \t]*export[ \t][ \t]*PATH=.*$quotedtargetdir[:; \t]|^[ \t]*PATH=.*$quotedtargetdir[:; \t]" "$HOME/.zshrc" >/dev/null; then
      setpath=1;
    fi;

    if ! grep -P "^[ \t]*export[ \t][ \t]*MANPATH=.*$quotedtargetdir\/man[:; \t]|^[ \t]*MANPATH=.*$quotedtargetdir\/man[:; \t]" "$HOME/.zshrc" >/dev/null; then
      setmanpath=1;
    fi;
  fi;

  if [ "$setpath" -eq 1 ]; then
    prost_printmsg "putting target directory into PATH variable";

    if [ "$PROST_INIT_SHELLNAME" == "bash" ]; then
      (echo; echo -n "export PATH=\"$targetdir:\$PATH\";") >>"$HOME/.bashrc";
    elif [ "$PROST_INIT_SHELLNAME" == "zsh" ]; then
      (echo; echo -n "export PATH=\"$targetdir:\$PATH\";") >>"$HOME/.zshrc";
    fi;
  fi;

  if [ "$setmanpath" -eq 1 ]; then
    prost_printmsg "putting target directory into MANPATH variable";

    if [ "$PROST_INIT_SHELLNAME" == "bash" ]; then
      (echo; echo -n "export MANPATH=\"$targetdir/man:\$MANPATH\";") >>"$HOME/.bashrc";
    elif [ "$PROST_INIT_SHELLNAME" == "zsh" ]; then
      (echo; echo -n "export MANPATH=\"$targetdir/man:\$MANPATH\";") >>"$HOME/.zshrc";
    fi;
  fi;
fi;

# check if we have to set some aliases
for aliasname in "prost-install" "prost-newproject" "prost-push" "prost-pull" "prost-importdb" "prost-rollout" "prost-fixpermissions" "prost-update" "prost-cmd"; do
  shortname="$aliasname";

  if [ "$aliasname" != "prost-install" ]; then
    shortname="$(echo "$aliasname" | cut -d"-" -f2)";
  fi;

  setalias=0;

  if [ "$PROST_INIT_SHELLNAME" == "bash" ]; then
    if ! grep -P "^[ \t]*alias[ \t]['\"]*$shortname['\"]*=['\"]*$aliasname\.sh['\"]*[; \t]" "$HOME/.bashrc" >/dev/null; then
      setalias=1;
    fi;
  elif [ "$PROST_INIT_SHELLNAME" == "zsh" ]; then
    if ! grep -P "^[ \t]*alias[ \t]['\"]*$shortname['\"]*=['\"]*$aliasname\.sh['\"]*[; \t]" "$HOME/.zshrc" >/dev/null; then
      setalias=1;
    fi;
  fi;

  if [ "$setalias" -eq 1 ]; then
    prost_printmsg "setting alias for $aliasname.sh";

    if [ "$PROST_INIT_SHELLNAME" == "bash" ]; then
      (echo; echo -n "alias \"$shortname\"=\"$aliasname.sh\";") >>"$HOME/.bashrc";
    elif [ "$PROST_INIT_SHELLNAME" == "zsh" ]; then
      (echo; echo -n "alias \"$shortname\"=\"$aliasname.sh\";") >>"$HOME/.zshrc";
    fi;
  fi;
done;

echo;
prost_printmsg "all done, please restart your shell or reload your configuration file by typing";
if [ "$PROST_INIT_SHELLNAME" == "bash" ]; then
  echo ". \$HOME/.bashrc";
elif [ "$PROST_INIT_SHELLNAME" == "zsh" ]; then
  echo ". \$HOME/.zshrc";
fi;
