#!/bin/sh

PROST_SCRIPT_PATH="$(dirname "$0")";

today="$(date "+%d.%m.%Y")";

(
  echo ".TH prost 1 \"$today\" \"\" \"prost deployment scripts";
  echo;

  for aliasname in "prost-install" "prost-newproject" "prost-push" "prost-pull" "prost-importdb" "prost-rollout" "prost-fixpermissions" "prost-update" "prost-cmd"; do
    shortname="$aliasname";

    if [ "$aliasname" != "prost-install" ]; then
      shortname="$(echo "$aliasname" | cut -d"-" -f2)";
    fi;

    echo ".SH NAME";
    echo ".B $aliasname.sh";
    "$PROST_SCRIPT_PATH/$aliasname.sh" -h | sed "/^$/q";

    echo ".SH SYNOPSIS";
    echo ".B $aliasname.sh (default alias: $shortname)";

    "$PROST_SCRIPT_PATH/$aliasname.sh" -h | grep -A1 "^  ";
    echo;
  done;

  echo ".SH Sponsored by";
  echo "arocom GmbH www.arocom.de";
  echo "Christopher Schmidhofer cjs-design.de";
) | gzip >"$PROST_SCRIPT_PATH/man/en/man1/prost.1.gz";
