#!/bin/bash

# prost project initialization script
# v0.1 ezi

# include the basic functions
PROST_SCRIPT_PATH="$(dirname "$0")";
. "$PROST_SCRIPT_PATH/include/_prost-base.sh";

# run init to get the required information
prost_init "$@";
