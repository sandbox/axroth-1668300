#!/bin/bash

# prost project initialization script
# v0.1 ezi

# include the basic functions
PROST_SCRIPT_PATH="$(dirname "$0")";
. "$PROST_SCRIPT_PATH/include/_prost-base.sh";

# thish script's dependencies
prost_adddependency "find";
prost_adddependency "grep";
prost_adddependency "sed";

# add some stages for the usage/stage list and for numbering
prost_addstage "checks";
prost_addstage "clone";
prost_addstage "directories";
prost_addstage "templates";
prost_addstage "database";
prost_addstage "import";
prost_addstage "drupal";

# run init to get the required information
prost_init "$@";

if [ $PROST_INIT_NODEPENDENCIES -eq 0 ]; then
  # run some dependency checks
  prost_printmsg "running dependency checks";
  prost_script_checkdependencies -quiet;

  if [ "$?" -gt 0 ]; then
    prost_printmsg "at least one dependency check failed, aborting" "error";
    exit 1;
  fi;
fi;

# get the stage number we will be starting from

foundstage=1;
startstage=0;
i=0;

if [ "$PROST_INIT_STAGE" != "" ]; then
  foundstage=0;

  tmpscriptstages="$PROST_SCRIPTSTAGES";
  while [ "$tmpscriptstages" != "" ]; do
    stagename="$(echo "$tmpscriptstages" | cut -d"|" -f1)";

    # remove stage or make empty if this was the last
    if echo "$tmpscriptstages" | grep "|" >/dev/null; then
      tmpscriptstages="$(echo "$tmpscriptstages" | cut -d"|" -f2-)";
    else
      tmpscriptstages="";
    fi;

    if [ "$PROST_INIT_STAGE" == "$stagename" ]; then
      foundstage=1;
      startstage=$i;
      break;
    fi;

    let i=$i+1;
  done;
fi;

if [ "$foundstage" -eq 0 ]; then
  prost_printmsg "invalid stage" "error";
  prost_stages;
  exit 1;
fi;

# setting default init stage name if not set
if [ "$PROST_INIT_STAGE" == "" ]; then
  PROST_INIT_STAGE="checks";
fi;

if [ "$PROST_INIT_XSTAGE" != "" ]; then
  tmpscriptstages="$PROST_SCRIPTSTAGES";
  while [ "$tmpscriptstages" != "" ]; do
    stagename="$(echo "$tmpscriptstages" | cut -d"|" -f1)";

    # remove stage or make empty if this was the last
    if echo "$tmpscriptstages" | grep "|" >/dev/null; then
      tmpscriptstages="$(echo "$tmpscriptstages" | cut -d"|" -f2-)";
    else
      tmpscriptstages="";
    fi;

    if echo "$PROST_INIT_XSTAGE" | grep "^$stagename|\||$stagename$\||$stagename|\|^$stagename$" >/dev/null; then
      let skipstage$i=1;
    fi;

    let i=$i+1;
  done;
fi;

# print some information on what will be done

prost_printmsg "Origin name: $PROST_INIT_ORIGIN";
if [ "$PROST_INIT_URL" != "" ]; then
  prost_printmsg "Project url: $PROST_INIT_URL";
fi;
prost_printmsg "Project directory: $PROST_INIT_DIRECTORY";
prost_printmsg "Starting at stage: $PROST_INIT_STAGE";

prost_separator;

# get the current project name
prost_fs_getcurrentproject;

if [ "$?" -eq 0 ]; then
  prost_printmsg "we are already in a project directory, aborting" "error";
  exit 1;
fi;

# stage 0: checks
if [ "$startstage" -le 0 ]; then
  if [ "$skipstage0" != "1" ]; then
    prost_printmsg "entering stage 0: checks";

    # check if the directory already exists
    if test -d "$PROST_INIT_DIRECTORY"; then
      prost_printmsg "project directory already exists, aborting" "error";
      exit 1;
    fi;

    if [ "$PROST_INIT_STOP" -eq 1 ]; then
      exit 0;
    fi;

    prost_separator;
  fi;
fi;

# stage 1: clone
if [ "$startstage" -le 1 ]; then
  if [ "$skipstage1" != "1" ]; then
    prost_printmsg "entering stage 1: clone";

    # run a git clone
    prost_printmsg "cloning project";
    prost_git_clone "$PROST_INIT_ORIGIN" "$PROST_INIT_URL" "$PROST_INIT_DIRECTORY";

    # check if cloning was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "cloning failed, aborting" "error";
      exit 1;
    else
      # cloning was successfull, log this installation
      prost_fs_loginstallation "$PROST_INIT_DIRECTORY" "$PROST_INIT_URL";
    fi;

    # check if cloning really was successfull (directory exists)
    if ! test -d "$PROST_INIT_DIRECTORY"; then
      prost_printmsg "cloned directory not found, aborting" "error";
      exit 1;
    fi;

    # check if cloned directory is empty
    if ! ls "$PROST_INIT_DIRECTORY" | grep "." >/dev/null; then
      prost_printmsg "cloned directory is empty, aborting" "error";
      exit 1;
    fi;

    # TODO: do this in a function the "proper" way
    cd "$PROST_INIT_DIRECTORY";
    if ! git branch | grep " master$" >/dev/null; then
      git branch -t "master";
    fi;
    cd "$OLDPWD";

    # make sure we are in the develop branch
    prost_printmsg "switching to develop branch, create it if missing";
    prost_git_checkout "develop" -createonmissing -directory "$PROST_INIT_DIRECTORY";

    # check if there was an error switching/creating the branch
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not switch/create the branch, aborting" "error";
      exit 1;
    fi;

    if [ "$PROST_INIT_GITLIVEURL" != "" ]; then
      # add live origin if supplied
      prost_printmsg "setting live remote origin";
      prost_git_addremote "$PROST_INIT_DIRECTORY" "$PROST_INIT_LIVEORIGIN" "$PROST_INIT_GITLIVEURL";

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not set live remote origin, aborting" "error";
        exit 1;
      fi;
    fi;

    if [ "$PROST_INIT_STOP" -eq 1 ]; then
      exit 0;
    fi;

    prost_separator;
  fi;
fi;

# stage 2: directories
if [ "$startstage" -le 2 ]; then
  if [ "$skipstage2" != "1" ]; then
    prost_printmsg "entering stage 2: directories";

    # creating initial directories
    prost_printmsg "creating base directories";

    # flag wether to move the existing files into the drupal directory
    movefiles=0;
    if ! test -d "$PROST_INIT_DIRECTORY/drupal"; then
      movefiles=1;
    fi;

    prost_fs_mkdirs -check -ignore "$PROST_INIT_DIRECTORY/drupal";

    # check if there was an error creating the directories which has to be catched
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not create the drupal directory, aborting" "error";
      exit 1;
    fi;

    if [ "$movefiles" -eq 1 ]; then
      prost_printmsg "moving files/directories into drupal directory";

      cd "$PROST_INIT_DIRECTORY";
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not change into installation directory, aborting" "error";
        exit 1;
      fi;

      prost_fs_move * .* drupal -skip drupal -skip . -skip .. -skip .git -skip nbproject -skip .gitignore;

      if [ "$?" -gt 0 ]; then
        prost_printmsg "at least one file/directory could not be moved, aborting" "error";
        exit 1;
      fi;

      cd "$OLDPWD";
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not change back into old directory, aborting" "error";
        exit 1;
      fi;
    fi;

    prost_fs_mkdirs -check -ignore -moderec "u+w" "$PROST_INIT_DIRECTORY/drupal/sites/default" -check -ignore "$PROST_INIT_DIRECTORY/landingpage" -check -ignore -moderec "a+w" "$PROST_INIT_DIRECTORY/drupal_sftp/files" -check -ignore -mode "a+w" "$PROST_INIT_DIRECTORY/drupal_sftp/files/languages" -check -ignore -mode "a+w" "$PROST_INIT_DIRECTORY/drupal_sftp/files/tmp" -check -ignore -mode "a+w" "$PROST_INIT_DIRECTORY/drupal_sftp/files/js" -check -ignore -mode "a+w" "$PROST_INIT_DIRECTORY/drupal_sftp/files/css" -check -ignore "$PROST_INIT_DIRECTORY/dumps" -check -ignore "$PROST_INIT_DIRECTORY/tests/library";

    # check if there was an error creating the directories which has to be catched
    if [ "$?" -gt 0 ]; then
      prost_printmsg "at least one mandatory directory could not be created, aborting" "error";
      exit 1;
    fi;

    if [ "$PROST_INIT_STOP" -eq 1 ]; then
      exit 0;
    fi;

    prost_separator;
  fi;
fi;

# stage 3: templates
if [ "$startstage" -le 3 ]; then
  if [ "$skipstage3" != "1" ]; then
    prost_printmsg "entering stage 3: templates";

    # get some information from the user

    # get the currently set value, if any
    defaultbase="";
    baseurl="";

    if [ "$PROST_INIT_BASEURL" != "" ]; then
      baseurl="$PROST_INIT_BASEURL";
    fi;

    if [ "$baseurl" == "" ]; then
      if test -f "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php"; then
        defaultbase="$(grep -P "^[ \t]*[\$]base_url[ \t]*=[ \t]*['\"][^'\"]*['\"]" "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php" | head -n1 | sed -e "s/^[ \t]*\$base_url[ \t]*=[ \t]*['\"]\([^'\"]*\)['\"].*$/\1/g")";
      fi;

      # get the base url
      prost_script_input "please enter your test system base url (including host, like localhost/fictive): " -default "$defaultbase";
      baseurl="$PROST_RETURN_INPUT";
    fi;

    if [ "$baseurl" != "" ]; then
      if ! echo "$baseurl" | grep -i "^[a-z][a-z]*:\/\/" >/dev/null; then
        baseurl="http://$baseurl";
      fi;
    fi;

    # drupal does not want a trailing slash
    drupalbaseurl="$baseurl";

    # add trailing slash for tests etc
    if ! echo "$baseurl" | grep "\/$" >/dev/null; then
      baseurl="$baseurl/";
    fi;

    # remove trailing slash for drupal if there was any
    if echo "$drupalbaseurl" | grep "\/$" >/dev/null; then
      drupalbaseurl="$(echo "$baseurl" | sed -e "s/\/$//g")";
    fi;

    drupalbaseurlnodomain="$drupalbaseurl";
    # get the folder-only base for proxy base url
    if echo "$drupalbaseurlnodomain" | grep -i "^[a-z][a-z]*:\/\/" >/dev/null; then
      drupalbaseurlnodomain="/$(echo "$drupalbaseurlnodomain" | cut -d\/ -f4-)";
    fi;

    # quote the urls for replacement
    quotedbaseurl="$(echo "$baseurl" | sed -e "s/\//\\\\\//g")";
    quoteddrupalbaseurl="$(echo "$drupalbaseurl" | sed -e "s/\//\\\\\//g")";
    quoteddrupalbaseurlnodomain="$(echo "$drupalbaseurlnodomain" | sed -e "s/\//\\\\\//g")";

    # do we use drupalconcept for deployment?
    drupalconcept=0;

    if [ "$PROST_INIT_NODEPLOYMENT" -eq 0 ]; then
      prost_script_yesno "are you using drupalconcept for deployment? ";
      drupalconcept=$?;
    fi;

    # get the currently set value, if any
    defaultsshuser="";
    sshuser="";

    if [ $drupalconcept -eq 1 ]; then
      if test -f "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/sshuser.php"; then
        defaultsshuser="$(grep -P "^[ \t]*[\$]sshUser[ \t]*=[ \t]*['\"][^'\"]*['\"]" "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/sshuser.php" | head -n1 | sed -e "s/^[ \t]*[\$]sshUser[ \t]*=[ \t]*['\"]\([^'\"]*\)['\"].*$/\1/g")";
      fi;

      # get the ssh username
      prost_script_input "please enter your deployment ssh username (like 'c[0-9]+_[a-z]+'): " -default "$defaultsshuser";
      sshuser="$PROST_RETURN_INPUT";
    fi;

    # get the currently set value, if any
    defaultlivehost="";
    livehost="";

    if [ $drupalconcept -eq 1 ]; then
      if [ "$PROST_INIT_GITLIVEURL" != "" ]; then
        if echo "$PROST_INIT_GITLIVEURL" | grep "@" >/dev/null; then
          defaultlivehost="$(echo "$PROST_INIT_GITLIVEURL" | cut -d"@" -f2-)";

          if echo "$defaultlivehost" | grep "\/" >/dev/null; then
            defaultlivehost="$(echo "$defaultlivehost" | cut -d\/ -f1)";
          fi;
        fi;
      fi;

      if [ "$defaultlivehost" == "" ]; then
        if test -f "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php"; then
          defaultlivehost="$(grep -P "^[ \t]*['\"]remote-host['\"][ \t]*=>[ \t]*['\"][^'\"]*['\"]" "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php" | head -n1 | sed -e "s/^[ \t]*['\"]remote-host['\"][ \t]*=>[ \t]*['\"]\([^'\"]*\)['\"].*$/\1/g")";
        fi;
      fi;

      # get the ssh username
      prost_script_input "please enter your deployment server (like 'power[0-9]+'): " -default "$defaultlivehost";
      livehost="$PROST_RETURN_INPUT";

      if ! echo "$livehost" | grep "\.$PROST_DEFAULT_LIVEHOSTDOMAIN$" >/dev/null; then
        livehost="$livehost.$PROST_DEFAULT_LIVEHOSTDOMAIN";
      fi;
    fi;

    # get the currently set value, if any
    defaultlivesite="";
    livesite="";

    if [ $drupalconcept -eq 1 ]; then
      if [ "$PROST_INIT_GITLIVEURL" != "" ]; then
        if echo "$PROST_INIT_GITLIVEURL" | grep ":" >/dev/null; then
          defaultlivesite="$(echo "$PROST_INIT_GITLIVEURL" | sed -e "s/^[a-z][a-z]*:\/\///gi" | cut -d":" -f1)";
        fi;
      fi;

      if [ "$defaultlivesite" == "" ]; then
        if test -f "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php"; then
          defaultlivesite="$(grep -P "^[ \t]*['\"]uri['\"][ \t]*=>[ \t]*['\"][^'\"]*['\"]" "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php" | head -n1 | sed -e "s/^[ \t]*['\"]uri['\"][ \t]*=>[ \t]*['\"]\([^'\"]*\)['\"].*$/\1/g" | cut -d"." -f1)";
        fi;
      fi;

      # get the ssh username
      prost_script_input "please enter your deployment site (like 'site[0-9]+'): " -default "$defaultlivesite";
      livesite="$PROST_RETURN_INPUT";
    fi;

    # get the currently set value, if any
    defaultliveclient="";
    liveclient="";

    if [ $drupalconcept -eq 1 ]; then
      # get the currently set value, if any
      if test -f "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php"; then
        if [ "$livesite" != "" ]; then
          quotedlivesite="$(echo "$livesite" | sed -e "s/\//\\\\\//g")";

          defaultliveclient="$(grep -P "^[ \t]*['\"]root['\"][ \t]*=>[ \t]*['\"][^'\"]*['\"]" "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php" | head -n1 | sed -e "s/^[ \t]*['\"]root['\"][ \t]*=>[ \t]*['\"]\([^'\"]*\)['\"].*$/\1/g")";
          defaultliveclient="$(echo "$defaultliveclient" | sed -e "s/\/$quotedlivesite\/.*$//g" -e "s/^.*\///g")";
        fi;
      fi;

      # get the live server client id
      prost_script_input "please enter your deployment site client id (like 'client[0-9]+'): " -default "$defaultliveclient";
      liveclient="$PROST_RETURN_INPUT";
    fi;

    appenddrupalroot=1;

    if [ $drupalconcept -eq 1 ]; then
      prost_script_yesno "has the project already been deployed with the prost subdirectory/tree structure? " -default "no";
      appenddrupalroot=$?;
    fi;

    # get the currently set value, if any
    defaultversion=6;
    installversion=6;

    if [ "$PROST_INIT_VERSION" != "" ]; then
      installversion="$PROST_INIT_VERSION";
    fi;

    if [ "$installversion" == "" ]; then
      if test -f "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php"; then
        if grep "\/\/ VERSION 7" "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php" >/dev/null; then
          defaultversion=7;
        fi;
      fi;

      # get the designated drupal version
      prost_script_multichoice "please select if you want to create a drupal 6 or 7 installation: " "6|7" -default $defaultversion;
      installversion="$PROST_RETURN_RESPONSE";
    fi;

    # information complete

    # copy some template files

    # .gitignore
    prost_printmsg "creating .gitignore file";
    prost_fs_copytemplate gitignore "$PROST_INIT_DIRECTORY" .gitignore -ignore;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg ".gitignore could not successfully be created, aborting" "error";
      exit 1;
    fi;

    # copy the selenium test script
    # TODO: consider using prost_fs_copytemplate or replace prost_fs_copytemplate by prost_fs_copy
    prost_printmsg "copying the phpunit test script";
    prost_fs_copy "$PROST_SCRIPT_PATH/templates/selenium/seleniumSuiteTest.php" "$PROST_INIT_DIRECTORY/tests";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not copy phpunit test script, aborting" "error";
      exit 1;
    fi;

    # copy the selenium test library
    prost_printmsg "copying the selenium test library";
    prost_fs_copy "$PROST_SCRIPT_PATH/templates/selenium/library" "$PROST_INIT_DIRECTORY/tests";

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not copy selenium test library, aborting" "error";
      exit 1;
    fi;

    # installation.settings.php
    prost_printmsg "creating installation specific settings.php file";
    prost_fs_copytemplate "installation$installversion.settings.php" "$PROST_INIT_DIRECTORY/drupal_sftp" "settings.php" -ignore;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "settings.php could not successfully be created, aborting" "error";
      exit 1;
    fi;

    # modify installation.settings.php (database)
    prost_printmsg "modifying installation specific settings.php file (database)";
    if [ "$PROST_INIT_CREATEUSER" != "" ]; then
      prost_fs_replace "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php" -ignore "\[MYSQLUSER\]" "$PROST_INIT_CREATEUSER" "\[MYSQLPWD\]" "$PROST_INIT_CREATEPWD" "\[PROJECTNAME\]" "$PROST_INIT_DIRECTORY";
    else
      prost_fs_replace "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php" -ignore "\[MYSQLUSER\]" "$PROST_INIT_MYUSER" "\[MYSQLPWD\]" "$PROST_INIT_MYPWD" "\[PROJECTNAME\]" "$PROST_INIT_DIRECTORY";
    fi;

    # check if replace was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "settings.php could not successfully be modified, aborting" "error";
      exit 1;
    fi;

    # modify installation.settings.php (base_url)
    prost_printmsg "modifying installation specific settings.php file (base url)";
    prost_fs_replace "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php" -ignore "\[BASEURL\]" "$quoteddrupalbaseurl";
    prost_fs_replace "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php" -ignore "\[BASEURLNODOMAIN\]" "$quoteddrupalbaseurlnodomain";

    # check if replace was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "settings.php could not successfully be modified, aborting" "error";
      exit 1;
    fi;

    # modify the drupal 7 bootstrap to allow drupal in a subdirectory
    if [ "$installversion" == "7" ]; then
      prost_printmsg "applying prost Drupal 7 patch";
      prost_fs_applypatch "$PROST_INIT_DIRECTORY/drupal" "request-path-bootstrap-757506-55_0.patch" -prefixskip 1 -ignore;

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not apply Drupal 7 patch, aborting" "error";
        exit 1;
      fi;
    fi;

    # modify selenium tests (base_url)
    prost_printmsg "modifying selenium tests (base url)";
    errors=0;

    find "$PROST_INIT_DIRECTORY/tests" -type f -name "test__*.html" |
      while read file; do
        prost_printmsg "modifying selenium test \"$file\"";
        prost_fs_replace "$file" -ignore -caseins "\([ \t]rel[ \t]*=[ \t]*['\"]selenium\.base['\"].*[ \t]href[ \t]*=[ \t]*['\"]\)[^'\"]*\(['\"]\)" "\1$quotedbaseurl\2";

        if [ "$?" -gt 0 ]; then
          prost_printmsg "could not modify test file \"$file\"" "warning";
          errors=1;
        fi;
      done;

    if [ "$errors" -gt 0 ]; then
      prost_printmsg "at least one selenium test could not be modified" "warning";
    fi;

    # settings.php
    prost_printmsg "creating settings.php file";
    prost_fs_copytemplate settings.php "$PROST_INIT_DIRECTORY/drupal/sites/default" -ignore;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "settings.php could not successfully be created, aborting" "error";
      exit 1;
    fi;

    # fake settings.php for the online server link

    # creating sites/default directory
    prost_printmsg "creating fake sites/default directory if it does not exist";
    prost_fs_mkdirs -check -ignore "$PROST_INIT_DIRECTORY/sites/default";

    # check if there was an error creating the directory
    if [ "$?" -gt 0 ]; then
      prost_printmsg "fake directory sites/default could not be created, aborting" "error";
      exit 1;
    fi;

    # settings.php
    prost_printmsg "creating fake settings.php file";
    prost_fs_copytemplate fake.settings.php "$PROST_INIT_DIRECTORY/drupal/sites/default" "settings.php" -skip;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "fake settings.php could not successfully be created, aborting" "error";
      exit 1;
    fi;

    # main htaccess file
    prost_printmsg "creating htaccess file for internal redirects into the drupal directory";
    prost_fs_copytemplate htaccess "$PROST_INIT_DIRECTORY" .htaccess -ignore;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "htaccess file could not successfully be created, aborting" "error";
      exit 1;
    fi;

    # modify htaccess file
    prost_printmsg "modifying htaccess file";
    prost_fs_replace "$PROST_INIT_DIRECTORY/.htaccess" "\[PROJECTNAME\]" "$PROST_INIT_DIRECTORY";

    # check if replace was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "htaccess file could not successfully be modified, aborting" "error";
      exit 1;
    fi;

    # drupal htaccess file
    # check if the file already contains the redirect
    prost_printmsg "removing existing dublicate content redirects if there are any";
    prost_fs_removeblockbypattern "$PROST_INIT_DIRECTORY/drupal/.htaccess" "^[ \t]*RewriteCond[ \t][ \t]*%{THE_REQUEST}[ \t][ \t]*[^ \t]*\/drupal" -ignore;

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not remove existing dublicate content redirects, aborting";
      exit 1;
    fi;

    # file does not contain redirect in it's first 10 lines, prepending template
    prost_printmsg "prepending duplicate content redirects";
    prost_fs_prependtemplate drupal-duplicatecontent-htaccess "$PROST_INIT_DIRECTORY/drupal/.htaccess" -ignore;

    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not modify drupal htaccess file, aborting" "error";
      exit 1;
    fi;

    # replace some variables in htaccess file
    prost_printmsg "modifying drupal htaccess file (variables)";
    prost_fs_replace "$PROST_INIT_DIRECTORY/drupal/.htaccess" -ignore "\[PROJECTNAME\]" "$PROST_INIT_DIRECTORY";

    # check if replace was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "drupal htaccess file could not successfully be modified, aborting" "error";
      exit 1;
    fi;

    # creating drush directory
    prost_printmsg "creating drush settings directory if it does not exist";
    prost_fs_mkdirs -check -ignore "$PROST_INIT_DIRECTORY/drupal/sites/all/drush";

    # check if mkdir was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "drush settings directory could not be created, aborting" "error";
      exit 1;
    fi;

    # drupal alias file
    prost_printmsg "creating drupal alias file";
    prost_fs_copytemplate aliases.drushrc.php "$PROST_INIT_DIRECTORY/drupal/sites/all/drush" -ignore;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "drupal alias file could not successfully be created, aborting" "error";
      exit 1;
    fi;

    # drupal alias ssh user
    prost_printmsg "creating user specific drupal alias ssh user file";
    prost_fs_copytemplate sshuser.php "$PROST_INIT_DIRECTORY/drupal/sites/all/drush" -ignore;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "ssh user file could not be created, aborting" "error";
      exit 1;
    fi;

    # modify drupal alias file
    prost_printmsg "modifying drupal alias file";
    prost_fs_replace "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php" "\[PROJECTNAME\]" "$PROST_INIT_DIRECTORY" "\[PROJECTPATH\]" "$(pwd | sed -e "s/\//\\\\\//g")\/$PROST_INIT_DIRECTORY" "\[LIVEHOST\]" "$livehost" "\[LIVESITE\]" "$livesite" "\[LIVECLIENT\]" "$liveclient";

    # check if replace was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "drupal alias file could not successfully be modified, aborting" "error";
      exit 1;
    fi;

    if [ "$appenddrupalroot" -gt 0 ]; then
      # update aliases' root if nescessary
      prost_printmsg "updating live drush site alias root";
      prost_drupal_appenddrupalroot "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/aliases.drushrc.php";

      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not update site alias, aborting" "error";
        exit 1;
      fi;
    fi;

    # modify drupal ssh user file
    prost_printmsg "modifying drupal alias ssh user file";
    prost_fs_replace "$PROST_INIT_DIRECTORY/drupal/sites/all/drush/sshuser.php" "\[SSHUSER\]" "$sshuser";

    # check if replace was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "drupal alias ssh user file could not successfully be modified, aborting" "error";
      exit 1;
    fi;

    # drupal alias ssh user
    prost_printmsg "creating permission fix script";
    prost_fs_copytemplate fixpermissions.php "$PROST_INIT_DIRECTORY/drupal/sites/all/drush" -ignore;

    # check if copy was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "permission fix script could not be created, aborting" "error";
      exit 1;
    fi;

    # create symlink for files
    prost_printmsg "creating symlink for files in drupal";
    prost_fs_symlink "../../../sites/default/files" "$PROST_INIT_DIRECTORY/drupal/sites/default/files" -ignore;

    # check if symlink creation was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not create symlink, aborting" "error";
      exit 1;
    fi;

    # create symlink for files
    prost_printmsg "creating symlink for files in fake directory";
    prost_fs_symlink "../../drupal_sftp/files" "$PROST_INIT_DIRECTORY/sites/default/files" -ignore;

    # check if symlink creation was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not create symlink, aborting" "error";
      exit 1;
    fi;

    if [ "$PROST_INIT_STOP" -eq 1 ]; then
      exit 0;
    fi;

    prost_separator;
  fi;
fi;

# stage 4: database
if [ "$startstage" -le 4 ]; then
  if [ "$skipstage4" != "1" ]; then
    prost_printmsg "entering stage 4: database";

    # check if the database already exists
    prost_printmsg "checking if database exists";
    prost_db_checkdatabase "$PROST_INIT_DIRECTORY" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";
    dbexists="$?";

    dropdb=0;
    createdb=1;

    if [ "$dbexists" -eq 1 ]; then
      dropdb=1;

      prost_script_yesno "database already exists. Drop existing database? ";

      # 0 is false in this case, exit script
      if [ "$?" -eq 0 ]; then
        dropdb=0;
        createdb=0;
      fi;
    fi;

    if [ $dropdb -eq 1 ]; then
      prost_printmsg "dropping database";
      prost_db_dropdatabase "$PROST_INIT_DIRECTORY" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";

      # check if database operations have been successfull
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not drop database, aborting" "error";
        exit 1;
      fi;
    fi;

    if [ $createdb -eq 1 ]; then
      # create database
      prost_printmsg "creating database";
      prost_db_create "$PROST_INIT_DIRECTORY" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD" -createuser "$PROST_INIT_CREATEUSER" -createpwd "$PROST_INIT_CREATEPWD";

      # check if database operations have been successfull
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not create database, aborting" "error";
        exit 1;
      fi;
    fi;

    if [ "$PROST_INIT_STOP" -eq 1 ]; then
      exit 0;
    fi;

    prost_separator;
  fi;
fi;

# stage 5: import
if [ "$startstage" -le 5 ]; then
  if [ "$skipstage5" != "1" ]; then
    prost_printmsg "entering stage 5: import";

    # import the latest database dump, if there is any
    prost_printmsg "importing database dump";
    if ls "$PROST_INIT_DIRECTORY/$PROST_DEFAULT_DUMPDIR" | grep "$PROST_DB_SQLFILES" >/dev/null; then
      prost_db_import "$PROST_INIT_DIRECTORY" "$PROST_INIT_DIRECTORY/$PROST_DEFAULT_DUMPDIR" -select -ignore -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";
    else
      prost_db_import "$PROST_INIT_DIRECTORY" "$PROST_INIT_DIRECTORY/drupal" -select -ignore -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";
    fi;

    # check if the database has been successfully imported
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not import database, aborting" "error";
      exit 1;
    fi;

    # import a user 0
    prost_printmsg "creating user 0";
    prost_drupal_createuser0 "$PROST_INIT_DIRECTORY" -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";

    # check if user 0 has been imported
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not import user 0" "warning";
    fi;

    if [ "$PROST_INIT_STOP" -eq 1 ]; then
      exit 0;
    fi;

    prost_separator;
  fi;
fi;

# stage 6: drupal
if [ "$startstage" -le 6 ]; then
  if [ "$skipstage6" != "1" ]; then
    prost_printmsg "entering stage 6: drupal";

    if [ "$installversion" == "" ]; then
      # get the currently set value, if any
      defaultversion=6;
      installversion=6;

      if [ "$PROST_INIT_VERSION" != "" ]; then
        installversion="$PROST_INIT_VERSION";
      fi;

      if [ "$installversion" == "" ]; then
        if test -f "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php"; then
          if grep "\/\/ VERSION 7" "$PROST_INIT_DIRECTORY/drupal_sftp/settings.php" >/dev/null; then
            defaultversion=7;
          fi;
        fi;

        # get the designated drupal version
        prost_script_multichoice "please select if you want to create a drupal 6 or 7 installation: " "6|7" -default $defaultversion;
        installversion="$PROST_RETURN_RESPONSE";
      fi;
    fi;

    # rudimentary check for drupal
    # TODO: do some better check
    prost_printmsg "checking for working drupal";

    if ! test -f "$PROST_INIT_DIRECTORY/drupal/index.php"; then
      prost_printmsg "project does not seem to have a working drupal" "warning";
    fi;

    if test -f "$PROST_INIT_DIRECTORY/drupal/index.php"; then
      # set file directories
      prost_printmsg "setting version specific variables";
      prost_drupal_vset "$PROST_INIT_DIRECTORY/drupal" -majversion 6 "file_directory_path" "sites/default/files" -majversion 6 "file_directory_temp" "sites/default/files/tmp" -majversion 7 "file_public_path" "sites/default/files" -majversion 7 "file_temporary_path" "sites/default/files/tmp";

      # check if some settings failed
      if [ "$?" -gt 0 ]; then
        prost_printmsg "at least one variable could not be set, aborting" "error";
        exit 1;
      fi;

      # prepending to the site name
      # get the current site name
      prost_printmsg "getting current site name";
      prost_drupal_vget "$PROST_INIT_DIRECTORY/drupal" "site_name";

      # check if we got the site name
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not get current site name" "warning";
      else
        sitename="$PROST_RETURN_VARIABLE";

        # remove leading test system marker
        sitename="$(echo "$sitename" | sed -e "s/^.*+++[ \t]*//g")";

        # get the users name
        prost_printmsg "getting the users name";
        prost_git_vget "$PROST_INIT_DIRECTORY" "user.name";

        if [ "$?" -gt 0 ]; then
          prost_printmsg "could not get user name" "warning";
        else
          username="$PROST_RETURN_VARIABLE";
          newsitename="+++ Testsystem - $username +++ $sitename";

          prost_printmsg "setting site name to $newsitename";
          prost_drupal_vset "$PROST_INIT_DIRECTORY/drupal" "site_name" "$newsitename";

          if [ "$?" -gt 0 ]; then
            prost_printmsg "could not set new site name" "warning";
          fi;
        fi;
      fi;

      # check if bam module is installed
      prost_printmsg "checking for installed bam module";
      prost_drupal_drushcmd "$PROST_INIT_DIRECTORY/drupal" drupal-directory backup_migrate;

      if [ "$?" -gt 0 ]; then
        # bam is not installed
        prost_printmsg "bam module is not installed, downloading";
        prost_drupal_drushcmd "$PROST_INIT_DIRECTORY/drupal" dl backup_migrate;

        if [ "$?" -gt 0 ]; then
          prost_printmsg "could not download bam module" "warning";
        fi;
      fi;

      # enabling bam
      prost_printmsg "enabling bam module";
      prost_drupal_drushcmd "$PROST_INIT_DIRECTORY/drupal" en backup_migrate;

      # check if en backup_migrate failed
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not enable bam module" "warning";
      fi;
    fi;

    # import the default dump settings
    prost_printmsg "setting default database dump settings";

    prost_db_import "$PROST_INIT_DIRECTORY" "$PROST_SCRIPT_PATH/templates/backup_migrate_destinations.sql" -ignore -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";

    # check if import was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not import default database dump destination" "warning";
    fi;

    prost_db_import "$PROST_INIT_DIRECTORY" "$PROST_SCRIPT_PATH/templates/backup_migrate_profiles_$installversion.sql" -ignore -myuser "$PROST_INIT_MYUSER" -mypwd "$PROST_INIT_MYPWD";

    # check if import was successfull
    if [ "$?" -gt 0 ]; then
      prost_printmsg "could not import default database dump profile" "warning";
    fi;

    if test -f "$PROST_INIT_DIRECTORY/drupal/index.php"; then
      # disabling varnish
      prost_printmsg "disabling varnish module";
      prost_drupal_drushcmd "$PROST_INIT_DIRECTORY/drupal" dis varnish;

      # check if dis varnish failed
      if [ "$?" -gt 0 ]; then
        prost_printmsg "could not disable varnish module" "warning";
      fi;

      # call drush updb
      prost_printmsg "updating database";
      prost_drupal_drushcmd "$PROST_INIT_DIRECTORY/drupal" updb

      # check if updb failed
      if [ "$?" -gt 0 ]; then
        prost_printmsg "updating database failed, aborting" "warning";
      fi;

      # call drush cc all
      prost_printmsg "clearing database caches";
      prost_drupal_drushcmd "$PROST_INIT_DIRECTORY/drupal" cc all;

      # check if cc failed
      if [ "$?" -gt 0 ]; then
        prost_printmsg "clearing database caches failed, aborting" "warning";
      fi;

      # set drupal admin pwd
      prost_printmsg "setting drupal admin password";
      prost_drupal_setuserpwd "$PROST_INIT_DIRECTORY/drupal" "$PROST_INIT_DRUPALADMINUSER" "$PROST_INIT_DRUPALADMINPWD" -fallback;

      # check if setting pwd failed
      if [ "$?" -gt 0 ]; then
        prost_printmsg "setting drupal admin password failed" "warning";
      fi;
    fi;

    if [ "$PROST_INIT_STOP" -eq 1 ]; then
      exit 0;
    fi;

    prost_separator;
  fi;
fi;

prost_printmsg "default email addresses for deployment messages can be set in $HOME/.prost/config as messagesfrommail={email} and messagestomail={email}" "color green";

# all done, ex(c)iting
prost_printmsg "all done";
